<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Vanderbilt - Commercial</label>
    <logo>Company_Logo/Vanderbilt_Homes_Logo.png</logo>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Development__c</tab>
    <tab>Plot__c</tab>
    <tab>Sub_Contractors__c</tab>
    <tab>Suppliers__c</tab>
    <tab>Contra_Charges__c</tab>
    <tab>Group_Deal__c</tab>
    <tab>Site__c</tab>
    <tab>Lesson_Learned__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>
