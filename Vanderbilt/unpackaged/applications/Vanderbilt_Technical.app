<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Vanderbilt - Technical</label>
    <logo>Company_Logo/Vanderbilt_Homes_Logo.png</logo>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>Site__c</tab>
    <tab>Development__c</tab>
    <tab>Plot__c</tab>
    <tab>Technical_Approval__c</tab>
    <tab>Consultant__c</tab>
    <tab>Architects_Engineers__c</tab>
    <tab>Planning_Conditions__c</tab>
    <tab>Lesson_Learned__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
</CustomApplication>
