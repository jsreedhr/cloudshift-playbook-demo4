<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>SpecifiedUser</dashboardType>
    <description>Strategy Dashboard</description>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Land summary</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Sites exchanged (volume) YTD</metricLabel>
            <report>Strategy_dashboard_reports/Site_exchanges_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Site__c.Expected_GDV__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Sites exchanged (exp. GDV) YTD</metricLabel>
            <report>Strategy_dashboard_reports/Site_exchanges_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <column>Site__c.Expected_GDV__c</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Sites completed (exp. GDV) YTD</metricLabel>
            <report>Strategy_dashboard_reports/Site_completions_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>Exchange/completion date in calendar year</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Sites completed (volume) YTD</metricLabel>
            <report>Strategy_dashboard_reports/Site_completions_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Excludes registered and completed sites</footer>
            <groupingColumn>Site__c.Stage__c</groupingColumn>
            <legendPosition>Right</legendPosition>
            <report>Strategy_dashboard_reports/Land_site_pipeline</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>New sites pipeline (number)</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All registered sites</footer>
            <groupingColumn>Site__c.Introduction_Date__c</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Weekly_Site_intros_trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Sites introduced</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>H_S_Inspection__c.Inspection_Score__c</column>
            </chartSummary>
            <componentType>Column</componentType>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Note - some sites may have had two inspections in this time. (A=8, B=4, C=2, D=1)</footer>
            <groupingColumn>CUST_NAME</groupingColumn>
            <header>Construction - H&amp;S</header>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/H_S_scores_last_8_weeks</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>H&amp;S inspection score (last 8 weeks)</title>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <header>Sales summary</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Reservations (volume) YTD</metricLabel>
            <report>Strategy_dashboard_reports/New_reservations_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Reservations (value) YTD</metricLabel>
            <report>Strategy_dashboard_reports/New_reservations_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Completions (volume) YTD</metricLabel>
            <report>Strategy_dashboard_reports/Company_completions_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Metric</componentType>
            <displayUnits>Auto</displayUnits>
            <footer>Reservation/completion date in calendar year</footer>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <metricLabel>Completions (value) YTD</metricLabel>
            <report>Strategy_dashboard_reports/Company_completions_YTD</report>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Forecast completion date in calendar year</footer>
            <legendPosition>Right</legendPosition>
            <report>Strategy_dashboard_reports/Revenue_pipeline_YTD</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Forecast revenue (current year)</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Column</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>New leads registered in week</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Weekly_new_leads_created</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>New leads</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>LineGrouped</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Visits and re-visits recorded in week</footer>
            <groupingColumn>DUE_DATE</groupingColumn>
            <groupingColumn>SUBJECT</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Weekly_visitor_trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Visitors</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>For exchanged reservations in week</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Weekly_reservation_to_exchange_time</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Average reservation - exchange (days)</title>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Includes some &apos;duplicate&apos; cases</footer>
            <header>Customer Care summary</header>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Weekly_cases_opened_trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Cases opened</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All open issues</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Open_issues_by_dev</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Open issues by development</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Donut</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>All open issues</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Open_issues_by_category</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <sortBy>RowValueDescending</sortBy>
            <title>Open issues by category</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>For cases closed in week</footer>
            <legendPosition>Bottom</legendPosition>
            <report>Strategy_dashboard_reports/Weekly_avg_case_resolution_time</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Average case resolution time (days)</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <aggregate>Average</aggregate>
                <axisBinding>y</axisBinding>
                <column>Cases_Snapshot__c.Age_Hours__c</column>
            </chartSummary>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Cases_Snapshot__c.Execution_Time__c</groupingColumn>
            <legendPosition>Bottom</legendPosition>
            <report>Customer_Care_Reports/Open_cases_monthly_trend</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Average age of open cases (days)</title>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>james.geake@vanderbilthomes.co.uk</runningUser>
    <textColor>#000000</textColor>
    <title>Company overview - weekly</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
