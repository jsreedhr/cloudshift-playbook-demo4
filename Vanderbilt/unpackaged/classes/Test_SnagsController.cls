@isTest
/**
*  @Class Name:    Test_SnagsController 
*  @Description:   This is a test class for the SnagsController
*  @Company: CloudShift
*  CreatedDate: 30/07/2017
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  David Kurtanjek     30/07/2017                  Original Version
*  Nitha T S           31/07/2017                  Last Modified
*
*/
public class Test_SnagsController {
    
    private static testMethod void testSnagsController(){
        
        List<Plot__c> plotList = new List<Plot__c>();
        
        //Create Account
        Account objAccount=TestData.createBusinessAccount();
        objAccount.Name = 'newTestAcc';
        objAccount.Type = 'Consultant';
        objAccount.Active_Region__c = 'London';
        insert objAccount;
        
        Account objAccount2=TestData.createBusinessAccount();
        objAccount2.Name = 'new';
        objAccount2.Type = 'Consultant';
        objAccount2.Active_Region__c = 'London';
        insert objAccount2;
        
        //createContactUnderAccount
        Contact objContact=TestData.createContact(objAccount);
        objContact.LastName = 'Mr.Scott';
        objContact.AccountId = objAccount.Id;
        objContact.Contact_Priority__c = 'high';
        insert objContact;
        
        //createDevelopment
        Development__c objdev=TestData.createDevelopment(objContact);
        objdev.Name = 'testDev';
        insert objdev;        
        
        //Create Plot
        Plot__c objPlot=TestData.createPlot(objAccount,objdev);
        objPlot.Name = 'testPlot';
        objPlot.Development__c = objdev.Id;
        plotList.add(objPlot);
        insert plotList;
        
        //Create snag
        Snag__c objSnag=TestData.createSnag(objPlot);
        objSnag.Category__c = 'Carpentry';
        objSnag.Plot_Property__c = objPlot.id;
        objSnag.Status__c = 'Raised';
        objSnag.Location__c = 'Kitchen';
        objSnag.Description__c = 'test data description';
        objSnag.Date_Completed__c = system.today();
        objSnag.CreatedDate = DateTime.newInstance(2017, 06, 17, 14, 30, 0);
        insert objSnag;
        
        PageReference pageRef = Page.Snag;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', objPlot.id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(objPlot);
        SnagsController classSC = new SnagsController(sc);
        classSC.saveWorkinHand();
        classSC.createRecords();
        classSC.addRow();
        classSC.saveWorkinHand();
        classSC.selectAll();
        classSC.deleteSelectedRows();
        classSC.rowNo ='1';
        classSC.delRow();
        classSC.saveWorkinHand();
    }
}