@isTest
private class SubStructure_Test {
    
static testMethod void SubStructure (){
   
    UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
 Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;

        insert u1;
     system.debug('start my-------->' +u1);
     
     User u2 = new User();
   

     u2.ProfileId = profileId.id ;
     u2.LastName = 'last';
     u2.Email = 'puser001@amamama.com';
     u2.Username = 'puser001@amamama.com' + System.currentTimeMillis();
     u2.CompanyName = 'TEST';
     u2.Title = 'title';
     u2.Alias = 'alias';
     u2.TimeZoneSidKey = 'America/Los_Angeles';
     u2.EmailEncodingKey = 'UTF-8';
     u2.LanguageLocaleKey = 'en_US';
     u2.LocaleSidKey = 'en_US';
     u2.UserRoleId = r2.Id;
      insert u2;
      system.debug('start my-------->' +u2);
    
      System.RunAs(u1)
     {
        Test.startTest();
         testfunc(u1.id, u2.id);
         Test.stopTest();
     }
    
   
}
    @future
    public static void testfunc(Id userid, Id u2id)
    {
          Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
    dev.Sales_Manager__c = userid;
    dev.Number_of_Plots__c = 1;
    dev.Active__c = TRUE;
           
    insert dev;
        
    Plot__c plot = new Plot__c();
    plot.Name = '1 Court Road';
    plot.Plot_Number__c = '1';
    plot.Development__c = dev.id;
    plot.Number_of_Beds__c = 3;
    plot.Open_Market_vs_Affordable__c = 'Affordable';
    plot.Size_Sq_Foot__c = 57;
    plot.House_Type__c = 'House';
    insert plot;
    
    Development__c sc1 = [SELECT id, Contracts_Manager__c, Site_Manager__c FROM  Development__c where id=:dev.id ];
        system.debug('start my-------->' +sc1);
        
    Plot__c lst_plots = new Plot__c ();
    lst_plots.Name = '1 Aldridge Place';
    lst_plots.Plot_Number__c = '2';
    lst_plots.Development__c = dev.id;
    lst_plots.Sales_Status__c = 'Not Released';
    lst_plots.Build_Status__c = 'Foundations';
    lst_plots.Communal_Plot__c = TRUE;
    lst_plots.Number_of_Beds__c = 3;
    lst_plots.House_Type__c = 'House';
    lst_plots.Open_Market_vs_Affordable__c = 'Affordable';
    lst_plots.Size_Sq_Foot__c = 57;
    Insert lst_plots;
  
    Site_Start__c ssc = new Site_Start__c ();
    ssc.Name = 'Site Start: Battersea';
    ssc.Development__c = dev.id;
    ssc.Status__c = 'Completed';
    system.debug('propIdset');
    ssc.Sub_Structure_Created__c = true;
    ssc.Initial_Notice_accepted__c = TRUE;
    ssc.Site_Start_Authorisation_meeting_held__c = TRUE;
    ssc.X4see_notified_inspection_plan_agreed__c =TRUE;
    ssc.Pre_Development_meeting_held__c =TRUE;
    ssc.Design_freeze_has_been_reached__c =TRUE;
    ssc.Technical_packages_ready_for_procurement__c =TRUE;
    ssc.Material_SC_orders_in_line_w_programme__c =TRUE;
    ssc.Site_prepared_correctly__c =TRUE;
    ssc.Site_fully_available_for_groundworker__c =TRUE;
    ssc.Pre_commencement_notifications_made__c =TRUE;
    ssc.Pre_commencement_conditions_addressed__c =TRUE;
    ssc.Pre_Development_meeting_held__c = TRUE;
    ssc.Local_authority_advised_of_start__c =TRUE;
    ssc.Neighbours_advised_of_intended_start__c =TRUE;
    ssc.Schools_Police_notified__c =TRUE;
    ssc.Construction_Management_Plan_complete__c =TRUE;
    ssc.Site_Traffic_Management_Plan_complete__c =TRUE;
    ssc.Sales_plan_completed_issued_to_site__c =TRUE;
    ssc.Sales_Strategy_set_for_the_development__c =TRUE;
    ssc.Site_fully_available_for_groundworker__c = TRUE;
    ssc.Site_prepared_correctly__c = TRUE;
    ssc.Site_Start_Authorisation_meeting_held__c = TRUE;
    ssc.Areas_of_concern_highlighted_to_team__c =TRUE;
    ssc.Site_Traffic_Management_Plan_complete__c = TRUE;
    ssc.F10_expiry_date__c = System.today();
    ssc.Sub_Structure_Created__c = False;
    insert ssc;  
    
    Sub_Structure__c sub = new Sub_Structure__c ();
    sub.Name = 'ALB Test';
    sub.Plot_Property__c = lst_plots.id;
    sub.Groundworks_externals_complete__c = TRUE;
    sub.Brickwork_to_standard_of_superstructure__c = TRUE;
    sub.Setting_out_and_level_check_completed__c = TRUE;
    sub.Driveways_roads_completed_to_base_course__c = TRUE;
    sub.Clean_safe_level_access_to_plot__c = TRUE;
    sub.Drainage_connected_open_ends_capped__c = TRUE;
    sub.Scaffold_patio_base_in_to_correct_level__c = TRUE;
    sub.Scaffold_patio_dressed_w_type_1_crush__c = TRUE;
    sub.Oversite_clean_Excess_materials_stored__c = TRUE;
   sub.Plot_service_ducts_installed_considered__c = TRUE;
    sub.Plot_programme_on_site_understood__c = TRUE;
    sub.Areas_of_concern_highlighted_to_team__c = TRUE;
    sub.Building_control_sign_off_complete__c = TRUE;
    sub.Technical_pack_in_place_to_complete_roof__c = TRUE;
    sub.Materials_in_place_to_completion_of_roof__c = TRUE;
    sub.SC_in_place_to_completion_of_roof__c = TRUE;
    sub.Robust_details_applicable_SCs_notified__c = TRUE;
    sub.Photos_Added__c = TRUE;
    sub.Mains_services_installed_to_site__c = TRUE;
    sub.Status__c = 'Completed';
    sub.Plot_Detail_Objects_Created__c = FALSE;
    sub.Date_Completed__c = System.today();
    
    Insert sub;
    
        
    Update sub;
   
    }
}