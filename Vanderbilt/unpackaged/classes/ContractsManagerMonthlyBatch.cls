global class ContractsManagerMonthlyBatch implements Database.Batchable<sObject>,schedulable {
    
    global void execute(SchedulableContext sc) {
        database.executebatch(this);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {                      
        return Database.getQueryLocator([
            SELECT Id, Name, Site_Manager__c, Contracts_Manager__c
            FROM Development__c 
            WHERE Active__c = TRUE
            AND Enable_Auto_Created_Inspections__c = TRUE
        ]);                     
    }

    global void execute(Database.BatchableContext BC, List<Development__c> scope) 
    {        
        List<Inspection__c> ins = New List<Inspection__c>();       
        Id monthlyInspectId = Schema.SObjectType.Inspection__c.RecordTypeInfosByName.get('Contracts Manager Monthly Inspection').RecordTypeId;
        
        Date nextMonthMonday = Date.today().addDays(28).toStartOfWeek();
        for(Development__c develop : scope) 
        {                                       
			Inspection__c insp = new Inspection__c(
                Development__c = develop.id,
                RecordTypeId = monthlyInspectId,
                Report_Date__c = nextMonthMonday,
                Site_Manager__c = develop.Site_Manager__c,
                Contracts_Manager__c = develop.Contracts_Manager__c,
                OwnerId = develop.Contracts_Manager__c,
                Created_From_Batch__c = True
            );            
			ins.add(insp);                                                              
        }
        insert ins;
    }

    global void finish(Database.BatchableContext BC) 
    {
      
    }
    
}