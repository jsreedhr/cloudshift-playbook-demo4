public without sharing class SiteTriggerFunctions {
    
    public static void CreateDevelopment(List <Site__c> lst_sites){
        
        List <Development__c> lst_devstocreate = new List <Development__c>();
        
        for(Site__c s: lst_sites){
            if(s.Stage__c == Label.Development_Creation_Stage && s.Development_Created__c == FALSE){
                Development__c d = new Development__c();
                d.Name = s.Name;
                d.Site__c = s.id;
                d.OwnerID = Label.Peter_McKeown;
                d.Marketing_Name__c = 'TBC';
                d.Active__c = TRUE;
                d.COINS_Development_Code__c = s.COINS_Development_Code__c;
                d.City__c = s.City__c;
                d.Created_By_Process__c = TRUE;
                d.Post_Code__c = s.Post_Code__c;
                d.Solicitor_Development_Setup__c = s.Vanderbilt_s_Solicitor__c;
                d.State_Province__c = s.County__c;
                d.Street__c = s.Street__c;
                lst_devstocreate.add(d);
                s.Development_Created__c = TRUE;
            }
        }
        insert lst_devstocreate;
    }
}