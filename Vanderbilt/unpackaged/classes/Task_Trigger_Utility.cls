/**
 *  @Class Name:    Task_Trigger_Utility 
 *  @Description:   This is a utility class for Task Trigger
 *  @Company: dQuotient
 *  CreatedDate: 30/09/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran				30/09/2016                  Orginal Version 
 * */
 
public with sharing class Task_Trigger_Utility{

 
    /**
     *  Method Name:    SendInitialCaseResponse 
     *  Description:    Method to Modify Initial Response Field On Case Related to the task
     *  Param:  Trigger.oldMap, Trigger.newMap
     *  Return: None
     */
    public static void SendInitialCaseResponse( map<id,Task> mapNewTask){
		Set<Id> CaseIdSet=new Set<Id>();
		List<Case> CaseList=new List<Case>();
        for(Task objTaskNew: mapNewTask.values()){
           if(objTaskNew.WhatId!=null&&string.isNotBlank(objTaskNew.subject)){
           if(String.valueOf(objTaskNew.WhatId).substring(0,3)=='500' && objTaskNew.subject.contains('Has Been Allocated to One of Our Agents')){
			   CaseIdSet.add(objTaskNew.WhatId);
		   }
        }
        }
		CaseList=[select id,Initial_Response_Email__c,OwnerId  from Case where Initial_Response_Email__c=false  AND id in:CaseIdSet];
		List<Case> updatedCase=new List<Case>();
		system.debug('CaseList+++'+CaseList);
    		if(CaseList!=null && CaseList.size()>0){
    		    for(Case C :CaseList){
    		        
        			    C.Initial_Response_Email__c=true;
        			    updatedCase.add(C);
    		        
    		    }
    		    try{
    		        update updatedCase;
    		      }
    		   catch(Exception e){
    		       
    		   }    
            }
    }
    /**
     *  Method Name:    SendInitialCaseResponseUpdate 
     *  Description:    Method to Modify Initial Response Field On Case Related to the task
     *  Param:  Trigger.oldMap, Trigger.newMap
     *  Return: None
     */
    public static void SendInitialCaseResponseUpdate(map<id,Task> mapOldTask, map<id,Task> mapNewTask){
		Set<Id> CaseIdSet=new Set<Id>();
	    List<Case> CaseList=new List<Case>();
		List<Case> updatedCase=new List<Case>();
		
        for(Task objTaskNew: mapNewTask.values()){
           if(objTaskNew.WhatId!=null && String.valueOf(objTaskNew.WhatId).substring(0,3)=='500'){
               if(mapOldTask.containsKey(objTaskNew.id) &&mapOldTask.get(objTaskNew.id)!=null&&!mapOldTask.get(objTaskNew.id).subject.contains('Has Been Allocated to One of Our Agents') && objTaskNew.subject.contains('Has Been Allocated to One of Our Agents')){
    			   CaseIdSet.add(objTaskNew.WhatId);
               }
    		   /*if(mapOldTask.get(objTaskNew.id).subject.contains('Has Been Allocated to One of Our Agents') && !objTaskNew.subject.contains('Has Been Allocated to One of Our Agents')){
    		       CaseIdSettoRed.add(objTaskNew.WhatId);
    		   }*/
    		   
    	    	   
        }
        if(!CaseIdSet.isEmpty()){
		    CaseList=[select id,Initial_Response_Email__c,OwnerId  from Case where Initial_Response_Email__c=false  AND id in:CaseIdSet];
        }
        /*if(CaseIdSet.isEmpty()){
		    CaseListtoRed=[select id,Initial_Response_Email__c,OwnerId  from Case where Initial_Response_Email__c=true  AND id in:CaseIdSettoRed];
        }*/
		system.debug('CaseList+++'+CaseList);
	    if(CaseList!=null && CaseList.size()>0){
    		    for(Case C :CaseList){
    		         
        			    C.Initial_Response_Email__c=true;
        			    updatedCase.add(C);
    		        
    		    }
    		    
    		}
            /*if(CaseListtoRed!=null && CaseList.size()>0){
    		    for(Case C :CaseListtoRed){
    		         if(String.ValueOf(C.OwnerId).substring(0,3)=='005'){
        			    C.Initial_Response_Email__c=false;
        			    updatedCasetoRed.add(C);
    		         }
    		    }
    		   
            }*/
    
        
    }
    if(updatedCase!=null &&updatedCase.size()>0){
        try{
            update updatedCase;
            system.debug('updatedCase+++'+updatedCase);
        }
        catch(DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
  }
    /*if(updatedCasetoRed!=null &&updatedCasetoRed.size()>0){
        try{
        update updatedCasetoRed;
        }
        catch(DmlException e){
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }*/
}
}