/**
 *  @Class Name:    ReservationLetterControllerExt 
 *  @Description:   This is a controller for ReservationLetterPage
 *  @Company: dQuotient
 *  CreatedDate: 23/11/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Pankaj Nayak        23/11/2016                  Orginal Version
 *
 */
public with sharing class ReservationLetterControllerExt{
    
    private Opportunity objOpportunity;
    public string oppId;
    public list<Opportunity> lstOpportunity = new list<Opportunity>();
    public String strCaseid;
    public String reservationLetterid;
    public Opportunity objreservationLetter{get;set;}
    public boolean isDisabled {get; set;}
    public List<Opportunity> ReservationLetterList{get;set;}
    public String pageMessage{get;set;}
    public boolean isSuccess {get; set;}
    public boolean isError {get; set;}
    public List<Issue__c>lstOfIssues{get;set;}
    public List<Case>lstOfCases{get;set;}
    
    
    /**
     *  Method Name: JOBSheetControllerExt 
     *  Description: This is the constructor for JOBSheetControllerExt class
     *  Param: ApexPages.StandardController
     *  Return: None
    */
    public ReservationLetterControllerExt(ApexPages.StandardController stdController){
        
        
        reservationLetterid=Apexpages.currentPage().getParameters().get('id');
     
        reservationLetterid=String.escapeSingleQuotes(reservationLetterid);
        if(stdController != null){
            
            objreservationLetter= (Opportunity)stdController.getRecord();
            reservationLetterList=new List<Opportunity>();
            reservationLetterList=[select id,Development_Marketing_Name__c,Plot_Number__c,Primary_Purchaser_Title_Surname__c,Primary_Purchaser_First_Name__c from opportunity where id=:reservationLetterid];
          
        }   
     
    
    
} 
    public Pagereference attachPdf()
    {
        string sheetid;
         Attachment att = new Attachment();
         att.name='';
         att.name+='Res Letter-';
         if(reservationLetterList[0].Development_Marketing_Name__c!=null){
             att.name+=reservationLetterList[0].Development_Marketing_Name__c;
              att.name+='-';
         } 
         if(reservationLetterList[0].Plot_Number__c!=null){
             att.name+=reservationLetterList[0].Plot_Number__c;
             att.name+='-';
         }
         if(ReservationLetterList[0].Primary_Purchaser_Title_Surname__c !=null){
             att.name+=reservationLetterList[0].Primary_Purchaser_Title_Surname__c ;
             att.name+='-';
         }
         if(reservationLetterList[0].Primary_Purchaser_First_Name__c !=null){
             att.name+=reservationLetterList[0].Primary_Purchaser_First_Name__c;
             att.name+='-';
         }
             att.name+=system.now().day();
             att.name+='-';
             att.name+=system.now().month();
             att.name+='-';
             att.name+=system.now().year();
             att.name+=' ';
             att.name+=system.now().hour();
             att.name+=':';
             att.name+=system.now().minute();
             
              att.name+='.pdf';
         PageReference invoicePage = Page.reservation_Letter;
         invoicePage .getParameters().put('id',reservationLetterid);
       //  system.debug('jobsheetid+++'+jobsheetid);
         // return page content as blob type
         // Alt: att.body = invoicePage .getContentAsPDF();
         if(Test.isRunningTest()){
            att.body=Blob.valueOf('UNIT.TEST');
         }
         else
            att.body = invoicePage.getContent();
         // Associate with project's record Id
         att.parentid = reservationLetterid;
         insert att;
         Pagereference pg=new Pagereference('/'+reservationLetterid) ;
        return pg;
    }
   
}