@isTest
/**
*  @Class Name:    Test_AccountTriggerFunctions 
*  @Description:   This is a test class for the AccountTriggerFunctions
*  @Company: CloudShift
*  CreatedDate: 08/12/2017
*
*  Modification Log
*  -----------------------------------------------------------
*  Developer           Modification Date           Comments
*  -----------------------------------------------------------
*  David Kurtanjek     08/12/2017                  Original Version
*
*/
public without sharing class Test_AccountTriggerFunctions {

    private static testMethod void MaterialsTest(){
        
    Account a = TestData.createBusinessAccount();
    a.Materials__c = 'Carpentry; Appliance';
    insert a;       
    a.Materials__c = 'Cleaning; Curtain Walling';
    Update a;
    }
}