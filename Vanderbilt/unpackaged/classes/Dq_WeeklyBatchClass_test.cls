@isTest
private class Dq_WeeklyBatchClass_test {
	public static testMethod void WeeklyReport() {
        
        User u1 = new User();
        Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
        u1.ProfileId = profileId.id;
        u1.LastName = 'last';
        u1.Email = 'puser000@amamama.com';
        u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
        u1.CompanyName = 'TEST';
        u1.Title = 'title';
        u1.Alias = 'alias';
        u1.TimeZoneSidKey = 'America/Los_Angeles';
        u1.EmailEncodingKey = 'UTF-8';
        u1.LanguageLocaleKey = 'en_US';
        u1.LocaleSidKey = 'en_US';

        insert u1;

        Development__c dev = new Development__c ();
        dev.Marketing_Name__c = 'Court Road';
        dev.Name = 'Abingdon';
        dev.COINS_Development_Code__c = '0009';
        dev.Street__c = '87 North Court Road';
        dev.City__c = 'Abingdon';
        dev.Country__c = 'United Kingdom';
        dev.Post_Code__c = 'OX14 1NN';
        dev.Contracts_Manager__c = u1.Id;
        dev.Site_Manager__c = u1.Id;
        dev.Sales_Manager__c = u1.Id; 
        dev.Active__c = TRUE;
        dev.Enable_Auto_Created_Inspections__c = TRUE;       
         
        insert dev;


        String recordstr=System.Label.Inspection_Weekly_Site_Manager_RT_Id ;

        Inspection__c inspect = new Inspection__c();        
        inspect.Development__c = dev.id;
        inspect.Name = 'Old Trafford Manager Monthly Inspection';
        inspect.RecordTypeId = recordstr;
        inspect.Report_Date__c = System.Date.Today().AddDays(7);
        //inspect.Site_Manager__c = user.id;
       
        
        insert inspect;

        Test.startTest();
          schduleBatchWeeklySiteManager obj = new  schduleBatchWeeklySiteManager();
          String cron = '0 0 23 * * ?';
          System.schedule('Test Schedule', cron, obj);    
        Test.stopTest();
   
    }
}