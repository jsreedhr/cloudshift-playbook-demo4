/**
 *  Name: TriggerHandler_Test
 *  Copyright © 2017 Salesforce
 *  Purpose: Test Class of CustomLogging.
 *
 *  History:
 *  Version         Author              Date               Detail
 *  1.0             Jeevraj Rao       14/11/2017           creation
 */
 
@isTest
private class CustomLogging_Test {
    
    static testMethod void testDivideByZeroException(){
        CustomLogController customLogController = new CustomLogController();
        customLogController.divideByZeroException();
        
        Integer numberOfCustomLogs= [Select count() From Custom_Log__c];
        Custom_Log__c customLog= [Select Class__c, Priority__c, Method__c, Message__c From Custom_Log__c LIMIT 1];
        CustomLogging.save();
        
        System.assertEquals('CustomLogController', customLog.Class__c);
        System.assertEquals('divideByZeroException', customLog.Method__c);
        System.assertEquals(CustomLogging.WARNING, customLog.Priority__c);
    }
    
    static testMethod void testNullPointerException(){
        CustomLogController customLogController = new CustomLogController();
        customLogController.nullPointerException();
        
        Integer numberOfCustomLogs= [Select count() From Custom_Log__c];
        Custom_Log__c customLog= [Select Class__c, Priority__c, Method__c, Message__c From Custom_Log__c LIMIT 1];
        CustomLogging.save();
        
        System.assertEquals('CustomLogController', customLog.Class__c);
        System.assertEquals('nullPointerException', customLog.Method__c);
        System.assertEquals(CustomLogging.ERROR, customLog.Priority__c);
    }
}