/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Performance_RatingTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Performance_RatingTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Performance_Rating__c());
    }
}