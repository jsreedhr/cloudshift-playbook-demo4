/**
 *  @Class Name:    ReservationagreementControllerExt 
 *  @Description:   This is a controller for ReservationagreementPage
 *  @Company: dQuotient
 *  CreatedDate: 30/11/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Pankaj Nayak        30/11/2016                  Orginal Version
 *
 */
public with sharing class ReservationAgreementControllerExt{
    
    private Opportunity objOpportunity;
    public String strCaseid;
    public String ReservationAgreementid;
    public Opportunity objReservationAgreement{get;set;}
    public boolean isDisabled {get; set;}
    public List<Opportunity> ReservationAgreementList{get;set;}
    public String pageMessage{get;set;}
    public boolean isSuccess {get; set;}
    public boolean isError {get; set;}
    public List<Issue__c>lstOfIssues{get;set;}
    public List<Case>lstOfCases{get;set;}
    
    
    /**
     *  Method Name: JOBSheetControllerExt 
     *  Description: This is the constructor for JOBSheetControllerExt class
     *  Param: ApexPages.StandardController
     *  Return: None
    */
    public ReservationAgreementControllerExt(ApexPages.StandardController stdController){
        
        lstOfIssues=new List<Issue__c>();
        lstOfCases=new List<Case>();
        ReservationAgreementid=Apexpages.currentPage().getParameters().get('id');
      //  system.debug('jobsheetid+++'+jobsheetid);
        ReservationAgreementid=String.escapeSingleQuotes(ReservationAgreementid);
        if(stdController != null){
            
            objReservationAgreement= (Opportunity)stdController.getRecord();
            ReservationAgreementList=new List<Opportunity>();
            ReservationAgreementList=[select id,Development_Marketing_Name__c,Plot_Number__c,Primary_Purchaser_Title_Surname__c,Primary_Purchaser_First_Name__c from opportunity where id=:ReservationAgreementid];
           // if(reservationLetterList!=null &&reservationLetterList.size()>0){                
             //   strCaseid=jobSheetList[0].Case__c;
              //  system.debug('strCaseid+++'+strCaseid);
           // }
        }   
    
    
    
} 
    public Pagereference attachPdf()
    {
        string sheetid;
         Attachment att = new Attachment();
         att.name='';
         att.name+='Res Agr-';
         if(ReservationAgreementList[0].Development_Marketing_Name__c!=null){
             att.name+=ReservationAgreementList[0].Development_Marketing_Name__c;
              att.name+='-';
         } 
         if(ReservationAgreementList[0].Plot_Number__c!=null){
             att.name+=ReservationAgreementList[0].Plot_Number__c;
             att.name+='-';
         }
         if(ReservationAgreementList[0].Primary_Purchaser_Title_Surname__c !=null){
             att.name+=ReservationAgreementList[0].Primary_Purchaser_Title_Surname__c ;
             att.name+='-';
         }
         if(ReservationAgreementList[0].Primary_Purchaser_First_Name__c !=null){
             att.name+=ReservationAgreementList[0].Primary_Purchaser_First_Name__c;
             att.name+='-';
         }
             att.name+=system.now().day();
             att.name+='-';
             att.name+=system.now().month();
             att.name+='-';
             att.name+=system.now().year();
             att.name+=' ';
             att.name+=system.now().hour();
             att.name+=':';
             att.name+=system.now().minute();
             
              att.name+='.pdf';
         PageReference invoicePage = Page.Reservation_Agreement;
         invoicePage.getParameters().put('id',ReservationAgreementid);
       
         if(Test.isRunningTest()){
            att.body=Blob.valueOf('UNIT.TEST');
         }
         else
            att.body = invoicePage.getContent();
         // Associate with project's record Id
         att.parentid = ReservationAgreementid;
         insert att;
         Pagereference pg=new Pagereference('/'+ReservationAgreementid) ;
        return pg;
    }
   
}