public without sharing class SiteStartTriggerFunctions{
    
    //Method to create substructures against plots when a Site Start has been completed for a development
    public static void createSubStructuresAgainstPlots(List <Site_Start__c> lst_ss){
        
        Set <ID> set_DevIds = new Set <ID>();
        List <Site_Start__c> lst_sstoupdate = new List <Site_Start__c>();
        List <Sub_Structure__c> lst_subs = new List <Sub_Structure__c>();
        
        //Check to see if there is at least one plot related to the development
        for(Site_Start__c s: lst_ss){
            If(s.Status__c == 'Completed'){
                IF(s.Development__c != Null){
                    IF(s.Number_of_Plots__c < 1 && s.Construction_Active__c == TRUE){
                        s.addError('Please ensure at least one plot has been created for the development.');
                    }
                }
            }
        }
        
        //Check to see if the Contracts Manager and the Site Manager has been recorded on the Development
        for(Site_Start__c s: lst_ss){
            If(s.Status__c == 'Completed'){
                IF(s.Development__c != Null){
                    IF(s.Contracts_Manager_ID__c == NULL || s.Site_Manager_ID__c == NULL || s.Sales_Manager_ID__c == NULL){
                        s.addError('Please ensure the Contracts Manager, Site Manager & Sales Manager have all been set on the development before attempting completion of a Site Start record.');
                    }
                }
            }
        }              
        
        for(Site_Start__c s: lst_ss){
            If(s.Status__c == 'Completed'){
                IF(s.Development__c != Null){
                    IF(s.Contracts_Manager_ID__c != NULL && s.Site_Manager_ID__c != NULL && s.Sub_Structure_Created__c == False && s.Data_Loaded__c == FALSE){
                        set_DevIds.add(s.Development__c);
                        lst_sstoupdate.add(s);
                    }
                }
            }            
        }
        system.debug('bi'+set_DevIds);
        IF(!set_DevIds.isEmpty()){
            
            List <Development__c> lst_devs = new List <Development__c>([SELECT ID, Site_Manager__c, Contracts_Manager__c FROM Development__c WHERE ID in: set_DevIds]);
            
            List <Inspection__c> lst_insps = new List <Inspection__c>();
            
            //Create the first weekly inspection record
            for(Development__c d: lst_devs){
                Inspection__c WI = new Inspection__c();
                    //Workflow will overwrite with the standard naming convention
                    WI.Name = 'X';
                    WI.Status__c = 'Not Started';
                    WI.RecordTypeID = System.Label.Inspection_Weekly_Site_Manager_RT_Id;
                    WI.Development__c = d.id;
                    WI.Contracts_Manager__c = d.Contracts_Manager__c;
                    WI.Site_Manager__c = d.Site_Manager__c;
                    WI.OwnerId = d.Site_Manager__c;
                    
                    date dateT=Date.valueOf(system.today());
                    integer addDays=0;
                    integer n=math.abs(math.MOD(dateT.daysBetween(date.newinstance(1985,7,1)),7));
                    system.debug('bi'+n);
                    if(n>0){
                        addDays=7-n;
                    }
                    system.debug(addDays+'hey'+dateT.addDays(addDays));
                    WI.Report_Date__c = dateT.addDays(addDays);
                    lst_insps.add(WI);
                
                //Create the first monthly inspection record
                Inspection__c MI = new Inspection__c();
                    //Workflow will overwrite with the standard naming convention
                    MI.Name = 'X';
                    MI.Status__c = 'Not Started';
                    MI.RecordTypeID = System.Label.Inspection_Monthly_Contracts_Manager_RT_Id;
                    MI.Development__c = d.id;
                    MI.Contracts_Manager__c = d.Contracts_Manager__c;
                    MI.Site_Manager__c = d.Site_Manager__c;
                    MI.OwnerId = d.Site_Manager__c;
                 
                    dateT=Date.valueOf(system.today()).toStartOfMonth();
                    dateT= dateT.addDays(32);
                    dateT= dateT.toStartOfMonth();
                    addDays=0;
                    n=math.abs(math.MOD(dateT.daysBetween(date.newinstance(1985,7,1)),7));
                    system.debug('bi'+n);
                    if(n>0){
                        addDays=7-n;
                    }
                    MI.Report_Date__c =  dateT.addDays(addDays);
                    lst_insps.add(MI);                
                    }            
            
            //Query all the plots that are linked to the development
            List <Plot__c> lst_plots = new List <Plot__c>([SELECT ID, Development__c, Sales_Status__c, Build_Status__c, Communal_Plot__c FROM Plot__c WHERE Development__c in: set_DevIds AND Sales_Status__c = 'Not Released' AND Build_Status__c = 'Foundations']);
            
            IF(!lst_plots.isEmpty()){
                
                for(Plot__c p: lst_plots){
                    
                    Sub_Structure__c s = new Sub_Structure__c();
                    //Workflow will overwrite with the standard naming convention
                    s.Name = 'X';
                    s.Plot_Property__c = p.id;
                    s.Status__c = 'Not Started';
                    
                    IF(p.Communal_Plot__c == TRUE){
                        s.RecordTypeId = System.Label.Sub_Structure_Communal_Plot_RT_Id;
                    }
                    IF (p.Communal_Plot__c == FALSE){
                        s.RecordTypeId = System.Label.Sub_Structure_Plot_RT_Id;
                    }
                    
                    lst_subs.add(s);
                }
                insert lst_subs;
                insert lst_insps;
                              
                for(Site_Start__c s: lst_sstoupdate){
                    s.Sub_Structure_Created__c = true;                     
                }              
            } 
        }              
    } 
}