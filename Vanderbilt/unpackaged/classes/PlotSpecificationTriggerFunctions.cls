public class PlotSpecificationTriggerFunctions {

        public static void getspecificationname(List <Plot_Specification__c> lst_plotspec){
        
        Set <ID> set_specids = new Set <ID>();
        string thestring;
        
        for(Plot_Specification__c obj_plotspec: lst_plotspec){
            IF(obj_plotspec.Specification__c != Null && obj_plotspec.Isclone__c==false){
                thestring = obj_plotspec.Specification__c;
                set_specids.add(thestring.right(15));                                                 
            }            
        }
        
        IF(!set_specids.isEmpty()){
            List <ContentDocument> lst_contentdocs = new List <ContentDocument>([SELECT ID, Title FROM ContentDocument WHERE ID in: set_specids]); 
            IF(!lst_contentdocs.isEmpty()){
                for(Plot_Specification__c obj_plotspec: lst_plotspec){
                    for(ID obj_spec: set_specids){
                        for(ContentDocument obj_contentdoc: lst_contentdocs){
                            IF(obj_spec == obj_contentdoc.id){
                                obj_plotspec.File_Name__c = obj_contentdoc.Title;                  
                            }
                        }  
                    }
                }
            }
        }
    }   
    
}