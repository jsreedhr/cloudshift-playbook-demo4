/**
*  Name: DeveloperTriggerFunctions_Test
*  Copyright © 2017 Salesforce
*  Purpose: This test class will test the DeveloperTriggerFunctions class
*
*  History:
*  Version          Author              Date                Detail
*  1.0              Jeevraj Rao         15/11/2017          created testCreateTechnicalApprovals.
*  1.1              Jeevraj Rao         15/11/2017          added testCreateConsultants.
**/

@isTest
private class DeveloperTriggerFunctions_Test {
    
    // Static variable
    private static User runningUser;
    private static final String STREET                                      = 'MLC Centre, 2/108 King St';
    private static final String CITY                                        = 'Sydney';
    private static final String COUNTY                                      = 'NSW';
    private static final String POSTCODE                                    = 'NSW 2000';
    private static final String COUNTRY                                     = 'Australia';
    
    
    @testSetup static void setup() {
        // Create User
        Id profileId = TestData.getProfileId(TestData.USER_PROFILE_ADMIN);
        runningUser = TestData.createUser(profileId);
        
        Account Acc = (Account) TestData.createSobject(TestData.ACCOUNT_OBJECT, false);
        insert Acc;
                
        Local_Authority__c localAuthority = new Local_Authority__c(Name = 'Test Local Authority');
        insert localAuthority;
        
        
        Site__c sites = new Site__c(
            Name                                          = 'Test Guru',
            Planning_Status__c                            = 'STPP',
            Purchase_Basis__c                             = 'Unconditional',
            Stage__c                                      = 'Registered',
            Land_Manager__c                               = runningUser.Id,
            Sourced_By__c                                 = runningUser.Id,
            Introduction_Date__c                          = System.today(),
            Street__c                                     = STREET,
            City__c                                       = CITY,
            County__c                                     = COUNTY,
            Post_Code__c                                  = POSTCODE,
            Actual_Exchange_Date__c                       = Date.newInstance(2017, 10, 30),
            Vendor_s_Solicitor__c                         = Acc.id,
            Vanderbilt_s_Solicitor__c                     = Acc.id,
            Local_Authority__c                            = localAuthority.iD,
            Land_Value__c                                 = 9000,
            Land_Value_Details__c                         = 'QWERTY',
            Expected_GDV__c                               = 99000,
            Gross_Margin__c                               = 7.00,
            No_of_Acres__c                                = 8.00,
            Payment_Profile__c                            = 'Payment Profile',
            No_of_Units_Open_Market_Houses__c             = 8,
            Offer_Deadline_Date__c                        = Date.newInstance(2017, 10, 30),
            Target_Exchange_Date__c                       = Date.newInstance(2017, 10, 30),
            Target_Completion_Date__c                     = Date.newInstance(2017, 10, 31),
            Black_Book_Issue_Date__c                      = Date.newInstance(2017, 10, 30),
            Long_Stop_Date__c                             = Date.newInstance(2017, 10, 30),
            Application_type_1st_application__c           = 'Outline',
            Application_description_1st_application__c    = 'Application description',
            Target_Submission_Date_1st_application__c     = Date.newInstance(2017, 10, 30),
            Development_Created__c                        = FALSE
        );
        insert sites;
        
        Consultant__c consultant = new Consultant__c(Type__c = 'Asbestos', Site__c = sites.ID);
        insert consultant;
            
        Offer__c offer = new Offer__c(Site__c = sites.ID, Stage__c = 'Accepted', Offer_Date__c = Date.newInstance(2017, 10, 30), Amount__c = 1000, Offer_Terms__c = 'Test Offer Terms');
        insert offer;
    }
    
   
    /**
    * @desc     Test Method to check Creation of Consultants Record if picklist value is changed to Exchanged.
    * @author   Jeevraj Rao (jeevraj@cloudshiftgroup.com)
    * @since    15/11/2017
    * @param    None
    */
    
	private static testMethod void testCreateConsultants() {
	    Site__c objSite = [Select ID, Stage__c from Site__c WHERE Name = 'Test Guru' LIMIT 1];
        
        Test.StartTest();
        
        objSite.Stage__c = 'Exchanged';
        objSite.Pre_Plan_1_Approved_Tick__c = true;
        update objSite;
        
        Test.StopTest();
    
        //assertions
        System.assertNotEquals(NULL, [Select id from Development__c WHERE Site__c =:objSite.id LIMIT 1]);
    
	}
    
    /**
    * @desc     Test Method to check Creation of Technical Approval Record if checkbox is set as true in DevelopmentObject.
    * @author   Jeevraj Rao (jeevraj@cloudshiftgroup.com)
    * @since    15/11/2017
    * @param    None
    */
    
	private static testMethod void testCreateTechnicalApprovals() {
        Test.StartTest();

        Development__c development = (Development__c) TestData.createSobject(TestData.DEVELOPMENT_OBJECT, false, new Map<String, Object> {
                'List_of_Plot_House_Type_s__c'      => 'A, B',
                'Plot_Number_with_House_Type__c'    => 'A - 1, B - 2, B - 3',
                'Created_by_Process__c'             => true,
                'Create_Technical_Approvals__c'     => false
        });
        insert development;
        
        development.Create_Technical_Approvals__c = true;
        update development;

        Test.StopTest();

        //assertions
        System.assertNotEquals(NULL, [Select id from Technical_Approval__c WHERE Development__c =:development.id LIMIT 1]);
	}

}