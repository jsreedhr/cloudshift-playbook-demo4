/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_Technical_ChecklistTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_Technical_ChecklistTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Technical_Checklist__c());
    }
}