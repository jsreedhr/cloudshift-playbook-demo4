global class schduleBatchWeeklySiteManager implements Schedulable {

   global void execute(SchedulableContext ctx) {
      DQ_WeeklyBatchClassBatchJob p = new DQ_WeeklyBatchClassBatchJob();
        database.executeBatch(p);
   }   
}