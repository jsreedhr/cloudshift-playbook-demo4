@isTest
/**
 *  @Class Name:    Test_Issue_Trigger_Utility 
 *  @Description:   This is a test class for Case_Email_Trigger_Utility
 *  @Company: dQuotient
 *  CreatedDate: 19/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran George              19/10/2016                  Orginal Version
 *  Nitha T S                 01/08/2017                  Last Modified
 */
private class Test_Issue_Trigger_Utility {

	/**
     *  Method Name:    positiveTestCase 
     *  Description:    Method to Test Issue_Trigger_Utility 
     *  Param:          None
     *  Return:         None
     */
	private static testMethod void positiveTestCase() {
	    //createBusinessAccount
	    Account objAccount=TestData.createBusinessAccount();
	    insert objAccount;
	    Account objAccount2=TestData.createBusinessAccount();
	    objAccount2.Name='new';
	    insert objAccount2;
	    //createContactUnderAccount
	    Contact objContact=TestData.createContact(objAccount);
	    insert objContact;
	   
	    Development__c dev = new Development__c ();
        dev.Marketing_Name__c = 'Court Road';
        dev.Name = 'Abingdon';
        dev.COINS_Development_Code__c = '0009';
        dev.Street__c = '87 North Court Road';
        dev.City__c = 'Abingdon';
        dev.Country__c = 'United Kingdom';
        dev.Post_Code__c = 'OX14 1NN';
        dev.Number_of_Plots__c = 1;
        dev.Active__c = TRUE;
        insert dev;
        
	    //Create Plot
        Plot__c objPlot=TestData.createPlotNoDev(objAccount,objAccount2);
        objPlot.Name = '1 Aldridge Place';
        obJPlot.House_Type__c = 'House';
        objPlot.Plot_Number__c = '2';
        objPlot.Development__c = dev.id;
        objPlot.Sales_Status__c = 'Not Released';
        objPlot.Build_Status__c = 'Foundations';
        objPlot.Communal_Plot__c = TRUE;
        objPlot.Number_of_Beds__c = 3;
        objPlot.Open_Market_vs_Affordable__c = 'Affordable';
        objPlot.Size_Sq_Foot__c = 57;
        insert objPlot;
        //Create Case
        Case objCase=TestData.createCaseNoPlot(objAccount,objContact);
        insert objCase;
        //Check the trigger logic
        //system.assert(objCase.Plot__c!=null);
        system.assert(objPlot!=null);
        //create issue
        Issue__c objIssue=TestData.createIssues(objAccount,objCase,objContact);
        insert objIssue;
        system.assert(objIssue!=null);
        //Trigger Logic
        system.assertEquals(objCase.Contractors_List__c, objIssue.Contractor__r.Name);
        update objIssue;
        delete objIssue;
        
	}

}