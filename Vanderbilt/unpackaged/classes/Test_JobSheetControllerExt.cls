@isTest
/**
 *  @Class Name:    Test_JobSheetControllerExt 
 *  @Description:   This is a test class for JOBSheetControllerExt
 *  @Company: dQuotient
 *  CreatedDate: 18/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran George              18/10/2016                  Orginal Version
 *  Nitha T S                 01/08/2017                  Last Modified
 */
private class Test_JobSheetControllerExt {

    private static testMethod void jobSheetTest() {
        //createBusinessAccount
        Account objAccount=TestData.createBusinessAccount();
        insert objAccount;
         //createContactUnderAccount
        Contact objContact=TestData.createContact(objAccount);
        insert objContact;
       //createDevelopment
        Development__c objDev=TestData.createDevelopment(objContact);
        insert objDev;
        //Create Plot
        Plot__c objPlot=TestData.createPlot(objAccount,objDev);
        insert objPlot;
        //Create Case
        Case objCase=TestData.createCase(objPlot,objContact);
        objCase.Additional_Contact__c = objContact.id;
        objCase.ContactId = objContact.id;
        insert objCase;
        //Create Issue
        Issue__c objIssue=TestData.createIssues(objAccount,objCase,objContact);
        insert objIssue;
        //Create JobSheet
        Job_Sheet__c objJobSheet=TestData.createJobSheet(objAccount,objCase,objContact);
        insert objJobSheet;
        Apexpages.currentPage().getParameters().put('id',objJobSheet.id);
        ApexPages.StandardController stdcontroller = new ApexPages.StandardController(objJobSheet);
        JOBSheetControllerExt ctrlr=new JOBSheetControllerExt(stdcontroller);
        ctrlr.attachPdf();
        ctrlr.GenerateAndSend();
    }

}