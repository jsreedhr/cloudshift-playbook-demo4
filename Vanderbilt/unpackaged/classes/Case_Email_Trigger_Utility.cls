/**
 *  @Class Name:    Case_Email_Trigger_Utility 
 *  @Description:   This is a utility class for CaseEmail Trigger
 *  @Company: dQuotient
 *  CreatedDate: 18/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran				18/10/2016                  Orginal Version 
 * */
 
public with sharing class Case_Email_Trigger_Utility{

    
 
    /**
     *  Method Name:    LinkPlotToCase 
     *  Description:    Method to Link Plot to Case
     *  Param:          Trigger.newMap
     *  Return:         None
     */
    public static void LinkPlotToCase(Case[] NewCase){
		List<Case>newCaseList=new List<Case>();
        Set<Plot__c> SetTemp = new Set<Plot__c>();
        Set<Id>SetCaseIds=new Set<Id>();
        Set<Id>SetContactId=new Set<Id>();
        List<Contact>lstofContact=new List<Contact>();
		//List<Plot__c> ListTemp=new List<Plot__c>();
        Set<Id> SetAccountIds=new Set<Id>();
		List<Plot__c>lstOfPlot=new List<Plot__c>();
	    Map<Id,Set<Plot__c>> MapAccountPlot=new Map<Id,Set<Plot__c>>();
		for(Case caseObj:NewCase){
			if(String.isNotBlank(caseObj.Origin)&&caseObj.Origin=='Email' &&String.isNotBlank(caseObj.ContactId)!=null){
			    SetAccountIds.add(caseObj.AccountId);
			    SetContactId.add(caseObj.ContactId);
			    SetCaseIds.add(caseObj.id);
			   /* if(String.isNotBlank(caseObj.Additional_Contact__c))
			       SetContactId.add(caseObj.Additional_Contact__c); */
			}
		}
		if(SetCaseIds!=null &&SetCaseIds.size()>0){
			newCaseList=[select id,Plot__c,Origin,AccountId,ContactId from Case where id in :SetCaseIds];
		}
		
		
		if(SetContactId!=null &&SetContactId.size()>0){
		   system.debug('SetContactId++++'+SetContactId);
		   lstofContact=[select id,AccountId from Contact where id in:SetContactId limit 10000]; 
		}
		if(lstofContact!=null &&lstofContact.size()>0){
    		for(Contact cont:lstofContact){
    		   
    		    SetAccountIds.add(cont.AccountId);
    		}
		}	
		if(SetAccountIds!=null && SetAccountIds.size()>0){
    		system.debug('SetAccountIds+++'+SetAccountIds);
    		lstOfPlot=[select id,Sold_To__c,Incumbent__c,Warranty_End__c,Completion_Date__c from Plot__c where (Sold_To__c in:SetAccountIds OR Incumbent__c in:SetAccountIds) AND Completion_Date__c!=null AND  Warranty_End__c>=:system.today() limit 10000];
		    system.debug('lstOfPlot+++'+lstOfPlot);
		    
		}
		Set<Plot__c>SetOfmapPlot=new Set<Plot__c>();
		if(lstOfPlot!=null &&lstOfPlot.size()>0){
		    system.debug('lstOfPlot+++'+lstOfPlot);
    		for(Plot__c objPlot:lstOfPlot){
    			if(objPlot.Incumbent__c!=null){
    				if (MapAccountPlot.containskey(objPlot.Incumbent__c)&&MapAccountPlot.get(objPlot.Incumbent__c)!=null){
    					MapAccountPlot.get(objPlot.Incumbent__c).add(objPlot);
    				}
    				else{
    					SetOfmapPlot.clear();
    					SetOfmapPlot.add(objPlot);
    					MapAccountPlot.put(objPlot.Incumbent__c,SetOfmapPlot);
    				}
    			}
    			if(objPlot.Sold_To__c!=null){
    				if(MapAccountPlot.containskey(objPlot.Sold_To__c)&&MapAccountPlot.get(objPlot.Sold_To__c)!=null){
    					MapAccountPlot.get(objPlot.Sold_To__c).add(objPlot);
    				}
    				else{
    					SetOfmapPlot.clear();
    					SetOfmapPlot.add(objPlot);
    					MapAccountPlot.put(objPlot.Sold_To__c,SetOfmapPlot);
    				}
    			}
    		}
    		system.debug('MapAccountPlot++++'+MapAccountPlot);
      }
        List<Case>lstnewCase=new List<Case>();
		if(newCaseList!=null &&newCaseList.size()>0){
		for(Case objCase:newCaseList)
			{
				if(String.isNotBlank(objCase.Origin)&&objCase.Origin=='Email'&&MapAccountPlot!=null &&MapAccountPlot.containskey(objCase.AccountId)&&MapAccountPlot.get(objCase.AccountId).size()==1){
				    SetTemp = MapAccountPlot.get(objCase.AccountId);
				    List<Plot__c> ListTemp = new List<Plot__c>();
				    ListTemp.addall(SetTemp);
					objCase.Plot__c=ListTemp[0].id;
					lstnewCase.add(objCase);
				}
			}
		}			
        if(lstnewCase!=null &&lstnewCase.size()>0){    
            try{
           
            update lstnewCase;
            }
            catch(Exception e){
                
            }
        }    
    }
    
    
}