@istest
public class TaskDevelopmentTriggerHandlerTest
{

    @istest
    public static void testMethod1()
    {
    	Account objAccount=TestData.createBusinessAccount();
        insert objAccount;
    
        Contact objContact=TestData.createContact(objAccount);
        insert objContact;
        
        Development__c objDev=TestData.createDevelopment(objContact);
        insert objDev;

    	Lead newLead = new Lead();
        newLead.salutation='Mr.';
        newLead.lastName='abcd';
        newLead.email='abcd@gmail.com';
        newLead.Primary_Development_Interest__c=objDev.id;
        insert newLead;

        Task taskObj = new Task();
        taskObj.Description='Test';
        taskObj.Subject='Test';
        taskObj.whoId=newLead.id;
        taskObj.Development__c=objDev.id;
        insert taskObj;
        taskObj.Subject='Test1';
        update taskObj;
    }
}