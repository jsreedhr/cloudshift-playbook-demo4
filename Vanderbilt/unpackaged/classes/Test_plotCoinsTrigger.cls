@isTest 

 /* 
 *  @Company: dQuotient
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S                 01/08/2017                  Last Modified
 */

private class Test_plotCoinsTrigger
{
	@isTest static void testTrigger()
	{
		Coins_Credentials__c csobj=new Coins_Credentials__c ();
		csobj.Coins_Username__c='test';
		csobj.Coins_Password__c='test';
		csobj.Coins_Endpoint_URL__c='1';
		csobj.Coins_Environment__c='test';
		csobj.name='Credentials';
		insert csobj;
		Account testAccount = new Account();
		testAccount.name='Test Account';
		insert testAccount;

		Development__c testDevelopment = new Development__c();
		testDevelopment.COINS_Development_Code__c='000';
		testDevelopment.Marketing_Name__c='Test';
		insert testDevelopment;

		Plot__c plotObj = new Plot__c();
        plotObj.House_Type__c = 'House';
		plotObj.Development__c=testDevelopment.id;
		plotObj.Plot_Number__c='000';
		plotObj.Reservation_Price__c=1;
		plotObj.Legal_Fees__c=1;
		plotObj.Incentives_Cash__c=1;
		plotObj.Extras_Standard_Rated__c=1;
		plotObj.Extras_Zero_Rated__c=1;
		plotObj.Incentives_Non_Cash__c=1;
		plotObj.Reservation_Deposit__c=1;
		plotObj.Legal_Disbursements__c=1;
		plotObj.Ground_Rent_Per_Annum__c=1;
		plotObj.Est_Service_Charge__c=1;
		plotObj.Release_Price_Approved__c=true;
		plotObj.Build_Status__c='CML';
		plotObj.Sales_Status__c='Exchanged';
		plotObj.COINS_Response_Message__c=NULL;
		plotObj.Coins_Response_Recieved_At__c=NULL;
		plotObj.Number_of_Beds__c = 3;
        plotObj.Open_Market_vs_Affordable__c = 'Affordable';
        plotObj.Size_Sq_Foot__c = 57;
		insert plotObj;
		plotObj.Completion_Date__c=System.now().date();
		plotObj.Sold_To__c=testAccount.id;
		plotObj.Incumbent__c=testAccount.id;
		plotObj.Sales_Status__c='Completed';
		update plotObj;

		Plot__c plotObjRetrieved = new Plot__c();
		plotObjRetrieved = [Select id,Sales_Status__c from Plot__c where id=:plotObj.id ]; 
		System.assertEquals(plotObjRetrieved.Sales_Status__c,'Completed');

		//Testing coinResultController
		coinResultController.doCallout(plotObj.id);
	}


}