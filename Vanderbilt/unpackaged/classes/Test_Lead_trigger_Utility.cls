@istest
public class Test_Lead_trigger_Utility{

        public testmethod static void method1(){
          UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;

        insert u1;
  System.RunAs(u1)
     {
        Test.startTest();
         allow(u1.id);
         Test.stopTest();
     }
   
}
    @future
    public static void allow(Id userid)
    {
    
    Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
    dev.Sales_Manager__c = userid; 
    dev.Active__c = TRUE;
     
    insert dev;
  
            Lead l = new Lead();
            l.LeadSource = 'Emails';
            l.Last_Name__c = 'x';
            l.Status = 'Qualified';
            l.Primary_Development_Interest__c = dev.id;
            l.Salutation__c = 'Dr.';
            l.Salutation = 'Mr.';
            l.Email = 'puser000@amamama.com';
            l.LastName = 'ab';
            l.First_Name__c = 'hv';
            l.Customer_Category__c = 'cash';
            l.Segment__c = 'Upsizer';
        	l.Opt_in_source_primary__c = 'Opted-in';
        	l.Opt_in_date_time_Primary__c = System.now();
        	l.Opt_in_source_primary__c = 'Word of Mouth';
        	l.Opt_in_expiry_date_primary__c = System.today()+365;
        	l.Opt_in_Status_Secondary__c = 'Opted-out';
            insert l;
        l.LastName = 'test';
        update l;
        
                   Account a = new Account();
                    a.name ='ABC';
                    a.type ='Other';
                    Insert a;
                    
                    contact c = new contact();
                    c.lastname='ABC';
                    c.accountId=a.id;
                    c.email='abc@gmail.com';
                    c.Salutation='Mr.';
                    insert c;
        
    Plot__c lst_plots = new Plot__c ();
    lst_plots.Name = '1 Aldridge Place';
    lst_plots.House_Type__c = 'House';
    lst_plots.Number_of_Beds__c = 2;
    lst_plots.Open_Market_vs_Affordable__c = 'Open Market';
    lst_plots.Planning_Sqft__c = 200;
    lst_plots.Plot_Number__c = '2';
    lst_plots.Development__c = dev.id;
    lst_plots.Sales_Status__c = 'Not Released';
    lst_plots.Build_Status__c = 'Foundations';
    lst_plots.Communal_Plot__c = TRUE;
    lst_plots.House_Type__c = 'House';
    lst_plots.Customer_Demo_Date__c = System.today();
     
    
    Insert lst_plots;
        
      
        
        Task t = New task();
        t.Subject = 'call';
        t.Status = 'open';
        t.Priority = 'Normal';
        t.Ownerid = Userid;
        t.WhoId = l.id;
        
        
        Case ca = new case();
        ca.AccountID = a.id;
        ca.ContactId = c.id;
        ca.Plot__c = lst_plots.id;
        ca.Status = 'On-Hold';
        ca.Priority = 'low';
        ca.Origin = 'Email';
        ca.Initial_Response_Email__c = False;
        
        insert ca;
        insert t;
        
        Update l;
        
        }
}