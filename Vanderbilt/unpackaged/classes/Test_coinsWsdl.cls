@isTest
private class Test_coinsWsdl {
    
static testMethod void coins(){
    
    UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;

        insert u1;
     
     
 
      System.RunAs(u1)
     {
        Test.startTest();
         testfunc(u1.id);
         Test.stopTest();
     }
    
   
}
    @future
    public static void testfunc(Id userid)
    {
        
        Account a = new Account();
                a.name ='ABC';
                a.type ='Other';
               
                Insert a;
            
                contact c = new contact();
                c.lastname='ABC';
                c.accountId=a.id;
                c.email='abc@gmail.com';
                c.Salutation='Mr.';
                insert c;
    
    Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
         dev.Sales_Manager__c = userid; 
    dev.Active__c = TRUE;
        
   
       
     
    insert dev;
    
    Development__c sc1 = [SELECT id, Contracts_Manager__c, Site_Manager__c FROM  Development__c where id=:dev.id ];
        system.debug('start my-------->' +sc1);
     
    Plot__c lst_plots = new Plot__c ();
    lst_plots.Name = '1 Aldridge Place';
    lst_plots.Plot_Number__c = '2';
    lst_plots.House_Type__c = 'House';
    lst_plots.Number_of_Beds__c = 2;
    lst_plots.Open_Market_vs_Affordable__c = 'Open Market';
    lst_plots.Planning_Sqft__c = 200;
    lst_plots.Development__c = dev.id;
    lst_plots.Sales_Status__c = 'Not Released';
    lst_plots.Build_Status__c = 'Foundations';
    lst_plots.Communal_Plot__c = TRUE;
    lst_plots.Customer_Demo_Date__c = System.today();
    lst_plots.Forecast_Completion_Quality_Audit_Date__c = System.today();
    lst_plots.Forecast_Completion_Date__c = System.today();
    lst_plots.Forecast_First_Fix_Completion_Date__c = System.today();
    lst_plots.Forecast_Pre_Decoration_Completion_Date__c = System.today();
    lst_plots.Forecast_Quality_Manager_Inspection_Date__c =System.today();
    lst_plots.Forecast_Sub_Structure_Completion_Date__c = system.today();
    lst_plots.Forecast_Super_Structure_Completion_Date__c = system.today();
    lst_plots.Construction_Comments__c = 'for test'; 
    lst_plots.Completion_Date__c = system.today();
    lst_plots.Reservation_Price__c = 300;
    lst_plots.Legal_Fees__c = 200;
    lst_plots.Incentives_Cash__c = 200;
    lst_plots.Extras_Standard_Rated__c = 100;
    lst_plots.Extras_Zero_Rated__c = 50;
    lst_plots.Incentives_Non_Cash__c = 200;
    lst_plots.Ground_Rent_Per_Annum__c = 300;
    lst_plots.Reservation_Deposit__c = 500;
    lst_plots.Legal_Disbursements__c = 600;
    lst_plots.Est_Service_Charge__c = 700;
    lst_plots.Incumbent__c = a.id ;
    lst_plots.Sold_To__c = a.id;
    
    Insert lst_plots;
    
    }
}