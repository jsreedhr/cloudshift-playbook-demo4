@istest
public class AddPrimaryContactToConvertedOppTest{
    public testmethod static void method1(){
        
        Development__c d = new Development__c();
        d.name ='Ascot';
        d.Marketing_Name__c ='Saddlers Mews';
        Insert d;
        
        Account a = new Account();
        a.name ='ABC';
        a.type ='Other';
        Insert a;
        
        contact c = new contact();
        c.lastname='ABC';
        c.accountId=a.id;
        c.email='abc@gmail.com';
        c.Salutation='Mr.';
        insert c;
        
        Opportunity o = new Opportunity();
        o.name = 'Test Opportunity';
        o.stagename = 'Reserved';
        o.closeDate = System.today()+20;
        insert o;
        
        list<Lead> lstLead = new list<Lead>();
        Lead le = new lead();
        le.lastName = 'TestData';
        le.First_Name__c = 'Mr.';
        le.Last_Name__c= 'kkkkkk'; 
        le.LeadSource = 'Emails';
        le.Customer_Category__c = 'NDP';
        le.Segment__c = 'First Time Buyer';
        le.Status = 'Qualified';
        le.salutation = 'MR.';
        le.Salutation__c = 'Mr.';
        le.email = 'abc@gmail.com';
        le.Primary_Development_Interest__c=d.id;
        le.Opt_in_Status_Primary__c = 'Opted-In';
        le.HasOptedOutOfEmail = FALSE;
        le.Opt_in_date_time_Primary__c = System.now();
        le.Opt_in_expiry_date_primary__c = System.today()+365;
        le.Opt_in_source_primary__c = 'Word of Mouth';
        le.Opt_in_Status_Secondary__c = 'Opted-Out';
        //le.Secondary_Contact__c = c.id;
        //le.Secondary_Contact__c = null;
        //le.isconverted = true;
        
        lstLead.add(le);
        insert lstLead;
        
        lstLead[0].email = 'TestClass@gmail.com';
        lstLead[0].First_Name__c = 'Mr.';
        update lstLead;
        
        Database.LeadConvert lc = new database.LeadConvert();
        lc.setLeadId(lstLead[0].id);
        lc.setDoNotCreateOpportunity(false);
        lc.setConvertedStatus('Qualified');
        lc.setOpportunityName('Test');
        LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        
        // Database.LeadConvertResult lcr = Database.convertLead(lc);
        //System.assert(lcr.isSuccess());
        Test.starttest();
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.assert(lcr.isSuccess());
        
        update lstLead;
        
        lead leadRetrieved =[Select id,lastname,Secondary_Contact__c,isconverted,convertedOpportunityId from Lead where id=:lstLead LIMIT 1];
        leadRetrieved.lastName = 'trunjeuyu';
        leadRetrieved.Secondary_Contact__c = null;
        System.debug('#********************************'+leadRetrieved.isConverted+leadRetrieved.convertedOpportunityId+leadRetrieved.Secondary_Contact__c+leadRetrieved.lastName);
        update leadRetrieved;
        Test.stoptest();
        
    } 
    
    
    
    
    
    
}