global class DailyLeadOwnerAssignmentBatch implements Database.Batchable<sObject>{
    
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        //String recordstr=System.Label.Inspection_Weekly_Site_Manager_RT_Id ;
        String query = 'select id, name, OwnerId, Primary_Development_Interest__c, Development_Sales_Consultant__c, Development_Sales_Consultant_Id__c, Number_of_Units_to_Reserve__c, isConverted FROM Lead WHERE isConverted = FALSE AND Primary_Development_Interest__c != Null AND Development_Sales_Consultant_Id__c != Null AND Primary_Development_Interest__r.Sales_Consultant__r.isActive = TRUE AND Number_of_Units_to_Reserve__c > 0';
        
        return Database.getQueryLocator(query);
        
    }
    
    global void execute(Database.BatchableContext BC, List<Lead> scope){
        
        List <Lead> leadstoupdate = new List <Lead>();
        
        System.debug(scope.size());
        
        for(Lead l : scope){
            
            if(l.OwnerId.getSObjectType() == User.SObjectType){
                l.ownerid = l.Development_Sales_Consultant_Id__c;
                leadstoupdate.add(l);
            }              
        }
        
        System.debug(leadstoupdate.size());
        
        Update leadstoupdate;
    }
        
        global void finish(Database.BatchableContext BC) {
            
        }
}