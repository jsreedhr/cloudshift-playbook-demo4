public with sharing class AddVisitController
{
    public String leadId{get;set;}
    public String option{get;set;}
    public PageReference addVisitController()
    {
        leadId=apexpages.currentpage().getparameters().get('leadId');
        option=apexpages.currentpage().getparameters().get('option');
        if(leadId!=NULL)
        {
        	if(option=='1')
        	{
        		Event newEvent = new Event();
        		newEvent.Subject='Visit';
        		newEvent.StartDateTime=roundToNearestHour(System.now());
        		newEvent.EndDateTime=newEvent.StartDateTime.addHours(1);
        		newEvent.whoId=leadId;
        		try
        		{
        		insert newEvent;
 				}catch(DmlException e)
 				{
 					System.debug('Insert Exception:'+e);
 				}
        	}
        	else if(option=='2')
        	{
        		Event newEvent = new Event();
        		newEvent.Subject='Re-Visit';
        		newEvent.StartDateTime=roundToNearestHour(System.now());
                newEvent.EndDateTime=newEvent.StartDateTime.addHours(1);
        		newEvent.whoId=leadId;
        		try
        		{
        		insert newEvent;
 				}catch(DmlException e)
 				{
 					System.debug('Insert Exception:'+e);
 				}
        	}
        	PageReference retURL = new PageReference('/'+leadId);
            retURL.setRedirect(true);
            return retURL;
        }
        else
        	return NULL;
    }

    public Datetime roundToNearestHour(Datetime dt)
    {
        if(dt.minute() > 30 )
            dt = dt.addMinutes(60 - dt.minute()).addSeconds(-1 * dt.second());  
        else
            dt = dt.addMinutes(30 - dt.minute()).addSeconds(-1 * dt.second());
        return dt;
    }


}