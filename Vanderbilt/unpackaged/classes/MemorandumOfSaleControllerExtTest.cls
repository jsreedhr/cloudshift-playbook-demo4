@isTest
/**
 *  @Class Name:    MemorandumOfSaleControllerExtTest
 *  @Description:   This is a test class for MemorandumOfSaleControllerExt
 *  @Company: dQuotient
 *  CreatedDate: 25/11/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Pankaj Nayak              25/11/2016                  Orginal Version
 *  Nitha T S                 01/08/2017                  Last Modified
 */
private class MemorandumOfSaleControllerExtTest{

    private static testMethod void reservationLetterTest() {
        //createBusinessAccount
     /*   Account objAccount=TestData.createBusinessAccount();
        insert objAccount;
         //createContactUnderAccount
        Contact objContact=TestData.createContact(objAccount);
        insert objContact;
       //createDevelopment
        Development__c objDev=TestData.createDevelopment(objContact);
        insert objDev;
        //Create Plot
        Plot__c objPlot=TestData.createPlot(objAccount,objDev);
        insert objPlot;
        //Create Case
        Case objCase=TestData.createCase(objPlot,objContact);
        insert objCase;
        //Create Issue
        Issue__c objIssue=TestData.createIssues(objAccount,objCase,objContact);
        insert objIssue;
        //Create JobSheet
        Job_Sheet__c objJobSheet=TestData.createJobSheet(objAccount,objCase,objContact);
        insert objJobSheet;
        Apexpages.currentPage().getParameters().put('id',objJobSheet.id);
        ApexPages.StandardController stdcontroller = new ApexPages.StandardController(objJobSheet);
        JOBSheetControllerExt ctrlr=new JOBSheetControllerExt(stdcontroller);
        ctrlr.attachPdf();
        ctrlr.GenerateAndSend(); */
        
            Account a = new Account();
            a.name ='ABC';
            a.type ='Other';
            Insert a;
            
            contact c = new contact();
            c.lastname='ABC';
            c.accountId=a.id;
            c.email='abc@gmail.com';
            c.Salutation='Mr.';
            insert c;
            
            Account objAccount=TestData.createBusinessAccount();
            insert objAccount;
            //createContactUnderAccount
            Contact objContact=TestData.createContact(objAccount);
            insert objContact;
           //createDevelopment
            Development__c objDev=TestData.createDevelopment(objContact);
            insert objDev;
            //Create Plot
            Plot__c objPlot=TestData.createPlot(objAccount,objDev);
            objPlot.Number_of_Beds__c =2;
            insert objPlot;
    
            list<opportunity> oppList = new list<Opportunity>();
            opportunity op = new opportunity();
            op.amount = 1000;
            op.Reservation_Fee__c=900; 
            op.AccountId=a.id;
            op.name='XYZ';
            op.StageName='Reserved';
            op.CloseDate= system.today();
            op.Anticipated_Exchange_Date__c = system.today() - 2;
            op.Reservation_Date__c = system.today() +2;
            op.Reservation_Deadline__c = system.today() +10;
            op.Primary_Purchaser__c = c.id;
        
            
            
            
            insert op;
            op.amount = 89898;
            update op;
            
            Apexpages.currentPage().getParameters().put('id',op.id);
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(op);
            MemorandumOfSaleControllerExt ctrlr=new MemorandumOfSaleControllerExt(stdcontroller);
            ctrlr.attachPdf();
            //ctrlr.GenerateAndSend();
    }

}