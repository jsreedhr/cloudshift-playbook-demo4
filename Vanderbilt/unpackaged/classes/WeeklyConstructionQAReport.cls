public class WeeklyConstructionQAReport {

public Map<Id,Development__c> dev {get; set;}
public Map<Id,Site_Start__c> ssc {get; set;}
public Map<Id,Site_Setup__c> ssu {get; set;}
public Map<Id,Site_Close_Down__c> scd {get; set;}
   

    public List<Id> DevList{get;set;}
    public Map<Id,String> DevsscDate {get; set;}
    public Map<Id,String> DevssuDate {get; set;}
    public Map<Id,String> DevscdDate {get; set;}
    public Map<Id,Plot__c> MapPlots {get; set;}
   
    public Map<Id,list<Plot__c>> MapDevAndPlot {get; set;}
    
    public Map<Id,String> MapPlotSubStrudtDate {get; set;}
    public Map<Id,String> MapPlotSuprstructDate {get; set;}
    public Map<Id,String> MapPlotFirstFixDate {get; set;}
    public Map<Id,String> MapPlotPreDecorationDate {get; set;}
    public Map<Id,String> MapPlotCompQuAuditDate {get; set;}
    public Map<Id,String> MapPlotQuaMangrInspDate {get; set;}
    public Map<Id,String> MapPlotQuaMangrInspDate1 {get; set;}
    public Map<Id,String> MapPlotCompletedDate {get; set;}
    public Map<Id,STRING> MapPlotStatus {get; set;}
    public Map<Id,STRING> MapPlotCustomer {get; set;}
    public Map<Id,  String> MapPlotComments {get; set;}
    public Map<Id,STRING> MapPlotNumber {get; set;}
    public Map<Id,STRING> devName {get; set;}
    
        public date dt {get;set;}
        public date weekStartDate1;
        public date weekEndDate1;
        public Integer Remain;
        public Integer Done;
    public Map<Id,String> MapPlotSubStrudtDateColor {get; set;}
    public Map<Id,String> MapPlotSuprstructDateColor {get; set;}
    public Map<Id,String> MapPlotFirstFixDateColor {get; set;}
    public Map<Id,String> MapPlotPreDecorationDateColor {get; set;}
    public Map<Id,String> MapPlotCompQuAuditDateColor {get; set;}
    public Map<Id,String> MapPlotQuaMangrInspDateColor {get; set;}
     public Map<Id,String> MapPlotQuaMangrInspDateColor1 {get; set;}
    public Map<Id,String> MapPlotCompletedDateColor {get; set;}
    public Map<Id,STRING> MapPlotCustomerColor {get; set;}
    
    
    
    
   
    
    public WeeklyConstructionQAReport()
    {
         string a = ApexPages.currentPage().getParameters().get('dt');
          
           
            
            if(a== null){
            
                dt = (System.today().toStartofWeek());
                
                dt = dt.addDays(-1);
              //  dt = dt.addDays(7);
                
                weekStartDate1 = dt.addDays(-6);
            }
            else{
                 // value in url 2016-12-04 where 04 is date 
                
                a = a.substring(0,a.indexof(' '));
             
                List<String> aList = a.split('-');  // this should give ind0-2016 ind1-12 ind2-04
                 dt = Date.newInstance(Integer.valueof(aList[0]),integer.valueOf(aList[1]),integer.valueOf(aList[2]));
                 
                    weekStartDate1 = dt;
            }
            
            
            
           
          
   
        
    MapPlots = new map<id,Plot__c>();
      dev =new Map<Id,Development__c>([select id,name from Development__c WHERE Active__c=TRUE ORDER BY name]);
        DevList  = new List<ID>();
        DevList.addAll(dev.keyset());
list<Plot__c> pp = [select id,Development__c,Plot_Number__c,Forecast_Completion_Quality_Audit_Date__c,Forecast_Quality_Manager_Inspection_Date__c,Forecast_Sub_Structure_Completion_Date__c,Forecast_Super_Structure_Completion_Date__c,Forecast_Pre_Decoration_Completion_Date__c,Customer_Demo_Date__c,Forecast_First_Fix_Completion_Date__c,Sales_Status__c,Forecast_Completion_Date__c,Construction_Comments__c,(select id,Date_Completed__c  from Sub_Structures__r LIMIT 1),(select id,Date_Completed__c  from Super_Structures__r LIMIT 1),(select id,Date_Completed__c  from First_Fixes__r LIMIT 1),(select id,Date_Completed__c from Pre_Decoration__r LIMIT 1),(select id,Date_Completed__c  from Completion_Quality_Audits__r LIMIT 1),(select id,Date_Completed__c,Status__c,First_Inspection_Date__c from Quality_Manager_Inspections__r LIMIT 1) from Plot__c where Development__c IN :dev.keyset() AND ( NOT (Sales_Status__c = 'Completed' AND Communal_Plot__c = FALSE ) )order by Forecast_Completion_Quality_Audit_Date__c desc];      system.debug('pp'+pp);
      if(pp!= null &&pp.size()>0)
      {
      for(Plot__c p: pp){
        MapPlots.put(p.id,p);  
      }
       system.debug('MapPlots---->'+MapPlots);
      }
    //   = new Map<Id,Plot__c>([]);
      ssc= new Map<Id,Site_Start__c>([select id,Date_Completed__c,Development__c from Site_Start__c where Development__c IN :dev.keyset()]);
      ssu= new Map<Id,Site_Setup__c>([select id,Date_Completed__c,Development__c from Site_Setup__c where Development__c IN :dev.keyset()]);  
      scd= new Map<Id,Site_Close_Down__c>([select id,Date_Completed__c,Development__c from Site_Close_Down__c where Development__c IN :dev.keyset()]);
      MapDevAndPlot=new Map<Id,list<Plot__c>>();
    MapPlotSubStrudtDate =new Map<Id,String>();
    MapPlotSuprstructDate=new Map<Id,String>();
     MapPlotFirstFixDate=new Map<Id,String>();
    MapPlotPreDecorationDate=new Map<Id,String>();
        
        MapPlotQuaMangrInspDate=new Map<Id,String>();
     MapPlotQuaMangrInspDate1=new Map<Id,String>();
        MapPlotCompQuAuditDate=new Map<Id,String>();
        
     MapPlotCompletedDate=new Map<Id,String>();
     MapPlotCustomer=new Map<Id,String>();
    MapPlotStatus=new Map<Id,STRING>();
    MapPlotComments=new Map<Id,String>();
     MapPlotNumber=new Map<Id,STRING>();
     devName=new Map<Id,STRING>();
        DevsscDate=new Map<ID,String>();
        DevssuDate=new Map<ID,String>();
        DevscdDate=new Map<ID,String>();
        // MapPlots=new Map<Id,Plot__c>();
        MapPlotSubStrudtDateColor =new Map<Id,String>();
    MapPlotSuprstructDateColor =new Map<Id,String>();
     MapPlotFirstFixDateColor =new Map<Id,String>();
    MapPlotPreDecorationDateColor =new Map<Id,String>();
     MapPlotCompQuAuditDateColor =new Map<Id,String>();
     MapPlotQuaMangrInspDateColor =new Map<Id,String>();
      MapPlotQuaMangrInspDateColor1 =new Map<Id,String>();
     MapPlotCompletedDateColor =new Map<Id,String>();
      MapPlotCustomerColor =new Map<Id,String>();
        
        
    
          
         for(Development__c C:dev.values()){
            if(!devName.containsKey(C.ID)){
         		devName.put(C.Id,C.name); 
                
            }
             
        } 
        
        for(Site_Start__c X:ssc.values()){
            if(!DevsscDate.containsKey(X.Development__c)){
                if(X.Date_Completed__c!=null){
                string dtt = DateTime.newInstance(X.Date_Completed__c.year(),X.Date_Completed__c.month(),X.Date_Completed__c.day()).format('dd-MM-YYYY');
         		DevsscDate.put(X.Development__c,dtt); 
               
                }
            }
        } 
        for(Site_Close_Down__c y:scd.values()){
            if(!DevscdDate.containsKey(y.Development__c)){
                if(y.Date_Completed__c!=null){
                    string dtt = DateTime.newInstance(y.Date_Completed__c.year(),y.Date_Completed__c.month(),y.Date_Completed__c.day()).format('dd-MM-YYYY');
         		DevscdDate.put(y.Development__c,dtt); 
               
                }else {
                DevscdDate.put(y.Development__c,''); 
            }
            }
            else {
                DevscdDate.put(y.Development__c,''); 
            }
        } 
        for(Site_Setup__c z:ssu.values()){
            if(!DevssuDate.containsKey(z.Development__c)){
                if(z.Date_Completed__c!=null){
                     string dtt = DateTime.newInstance(z.Date_Completed__c.year(),z.Date_Completed__c.month(),z.Date_Completed__c.day()).format('dd-MM-YYYY');
         		DevssuDate.put(z.Development__c,dtt);
                
            }
            
        } 
        }
        
        for(Development__c D:dev.values()){
            if(!DevsscDate.containsKey(D.ID)){
         		DevsscDate.put(D.Id,''); 
                 
            }
            
             if(!DevscdDate.containsKey(D.ID)){
         		DevscdDate.put(D.Id,''); 
                 
            }
            
             if(!DevssuDate.containsKey(D.ID)){
         		DevssuDate.put(D.Id,''); 
                 
            }
             
        }
        
      for(Id devid: Devlist)
      {
           if(!MapDevAndPlot.containskey(devid))
            MapDevAndPlot.put(devid,new List<Plot__c>());
            
      }
    
           
     for(Plot__c E:MapPlots.values()){
         
            if(!MapDevAndPlot.containsKey(E.Development__c)){
                MapDevAndPlot.put(E.Development__c,new List<Plot__c>{E});
                
              }else{
                MapDevAndPlot.get(E.Development__c).add(E);
              }
              
            if(!MapPlotComments.containsKey(e.ID))
            {
                if(e.Construction_Comments__c!=null)
                MapPlotComments.put(e.Id,String.valueof(e.Construction_Comments__c));
                else{
                  MapPlotComments.put(e.Id,String.valueof('')); 
                  
              }
                
                
              }
              else{
                  MapPlotComments.put(e.Id,String.valueof('')); 
                  
              }
              
               if(!MapPlotNumber.containsKey(e.ID))
            {
                MapPlotNumber.put(e.Id,e.Plot_Number__c);
                
              }else{
                  MapPlotNumber.put(e.Id,''); 
                  
             }
              if(!MapPlotCustomer.containsKey(e.ID))
            {
                 if(e.Customer_Demo_Date__c!=null){
                     string dtt = DateTime.newInstance(e.Customer_Demo_Date__c.year(),e.Customer_Demo_Date__c.month(),e.Customer_Demo_Date__c.day()).format('dd-MM-YYYY');
                MapPlotCustomer.put(e.Id,dtt);
                
              }else{
                  MapPlotCustomer.put(e.Id,''); 
                  
             }
             }else{
                  MapPlotCustomer.put(e.Id,''); 
                  
             }
              
            if(!MapPlotStatus.containsKey(e.ID))
            {
                MapPlotStatus.put(e.Id,e.Sales_Status__c);
                
              }
              
         if(!MapPlotSubStrudtDate.containsKey(E.ID))
         {
             if(E.Sub_Structures__r!=null && E.Sub_Structures__r.size()>0 && E.Sub_Structures__r[0].Date_Completed__c!=null){
                 string dtt = DateTime.newInstance(E.Sub_Structures__r[0].Date_Completed__c.year(),E.Sub_Structures__r[0].Date_Completed__c.month(),E.Sub_Structures__r[0].Date_Completed__c.day()).format('dd-MM-YYYY');
             MapPlotSubStrudtDate.put(E.Id,dtt);
             
         }
         else if(String.valueof(e.Forecast_Sub_Structure_Completion_Date__c) != null){
             string dtt = DateTime.newInstance(e.Forecast_Sub_Structure_Completion_Date__c.year(),e.Forecast_Sub_Structure_Completion_Date__c.month(),e.Forecast_Sub_Structure_Completion_Date__c.day()).format('dd-MM-YYYY');
                  MapPlotSubStrudtDate.put(e.Id,dtt);
         }
         else {
             
               MapPlotSubStrudtDate.put(e.Id,'');
         }
         
         }
       
         
          if(!MapPlotSuprstructDate.containsKey(E.ID))
          {
              if(E.Super_Structures__r!=null && E.Super_Structures__r.size()>0 && E.Super_Structures__r[0].Date_Completed__c!=null){
                  string dtt = DateTime.newInstance(E.Super_Structures__r[0].Date_Completed__c.year(),E.Super_Structures__r[0].Date_Completed__c.month(),E.Super_Structures__r[0].Date_Completed__c.day()).format('dd-MM-YYYY');
                  
             MapPlotSuprstructDate.put(E.Id,dtt);
             
         }
         else if(String.valueof(e.Forecast_Super_Structure_Completion_Date__c) != null){
             string dtt = DateTime.newInstance(e.Forecast_Super_Structure_Completion_Date__c.year(),e.Forecast_Super_Structure_Completion_Date__c.month(),e.Forecast_Super_Structure_Completion_Date__c.day()).format('dd-MM-YYYY');
                  MapPlotSuprstructDate.put(e.Id,dtt);
         }
         else {
             
               MapPlotSuprstructDate.put(e.Id,'');
         }
          }
         
          if(!MapPlotFirstFixDate.containsKey(E.ID))
          {
              if(E.First_Fixes__r!=null && E.First_Fixes__r.size()>0 && E.First_Fixes__r[0].Date_Completed__c!=null){
              string dtt = DateTime.newInstance(E.First_Fixes__r[0].Date_Completed__c.year(),E.First_Fixes__r[0].Date_Completed__c.month(),E.First_Fixes__r[0].Date_Completed__c.day()).format('dd-MM-YYYY');
             MapPlotFirstFixDate.put(E.Id,dtt);
             
         }
         else if(String.valueof(e.Forecast_First_Fix_Completion_Date__c) != null){
             string dtt = DateTime.newInstance(e.Forecast_First_Fix_Completion_Date__c.year(),e.Forecast_First_Fix_Completion_Date__c.month(),e.Forecast_First_Fix_Completion_Date__c.day()).format('dd-MM-YYYY');
                  MapPlotFirstFixDate.put(e.Id,dtt);
         }
         else {
             
               MapPlotFirstFixDate.put(e.Id,'');
         }
          }
          
          if(!MapPlotPreDecorationDate.containsKey(E.ID))
          {
              if(E.Pre_Decoration__r!=null && E.Pre_Decoration__r.size()>0 && E.Pre_Decoration__r[0].Date_Completed__c!=null){
                  string dtt = DateTime.newInstance(E.Pre_Decoration__r[0].Date_Completed__c.year(),E.Pre_Decoration__r[0].Date_Completed__c.month(),E.Pre_Decoration__r[0].Date_Completed__c.day()).format('dd-MM-YYYY');
             MapPlotPreDecorationDate.put(E.Id,dtt);
              
         } 
          else if(String.valueof(e.Forecast_Pre_Decoration_Completion_Date__c) != null){
              string dtt = DateTime.newInstance(E.Forecast_Pre_Decoration_Completion_Date__c.year(),E.Forecast_Pre_Decoration_Completion_Date__c.month(),E.Forecast_Pre_Decoration_Completion_Date__c.day()).format('dd-MM-YYYY');
                  MapPlotPreDecorationDate.put(e.Id,dtt);
         }
         else {
             
               MapPlotPreDecorationDate.put(e.Id,'');
         }
              
             
     }
         
         if(!MapPlotCompQuAuditDate.containsKey(E.ID)){
             // system.debug('MapPlotCompQuAuditDate----->' +MapPlotCompQuAuditDate);
             if(E.Completion_Quality_Audits__r!=null && E.Completion_Quality_Audits__r.size()>0 && E.Completion_Quality_Audits__r[0].Date_Completed__c!=null){
                 
                 string dtt = DateTime.newInstance(E.Completion_Quality_Audits__r[0].Date_Completed__c.year(),E.Completion_Quality_Audits__r[0].Date_Completed__c.month(),E.Completion_Quality_Audits__r[0].Date_Completed__c.day()).format('dd-MM-YYYY');
             MapPlotCompQuAuditDate.put(E.Id,dtt);
            
         }
          else if(String.valueof(e.Forecast_Completion_Quality_Audit_Date__c) != null){
              
              string dtt = DateTime.newInstance(E.Forecast_Completion_Quality_Audit_Date__c.year(),E.Forecast_Completion_Quality_Audit_Date__c.month(),E.Forecast_Completion_Quality_Audit_Date__c.day()).format('dd-MM-YYYY');
                  MapPlotCompQuAuditDate.put(e.Id,dtt);
         }
         else {
             
             
             
               MapPlotCompQuAuditDate.put(e.Id,'');
         }
         }
         
         if(!MapPlotQuaMangrInspDate.containsKey(E.ID)){
             if(E.Quality_Manager_Inspections__r!=null && E.Quality_Manager_Inspections__r.size()>0 && E.Quality_Manager_Inspections__r[0].Date_Completed__c!=null){
               
                 string dtt = DateTime.newInstance(E.Quality_Manager_Inspections__r[0].Date_Completed__c.year(),E.Quality_Manager_Inspections__r[0].Date_Completed__c.month(),E.Quality_Manager_Inspections__r[0].Date_Completed__c.day()).format('dd-MM-YYYY');
             MapPlotQuaMangrInspDate.put(E.Id,dtt);
             
         } else if(String.valueof(e.Forecast_Quality_Manager_Inspection_Date__c) != null){
             string dtt = DateTime.newInstance(E.Forecast_Quality_Manager_Inspection_Date__c.year(),E.Forecast_Quality_Manager_Inspection_Date__c.month(),E.Forecast_Quality_Manager_Inspection_Date__c.day()).format('dd-MM-YYYY');
                  MapPlotQuaMangrInspDate.put(e.Id,dtt);
         }
         else {
             
               MapPlotQuaMangrInspDate.put(e.Id,'');
         }
         }
          if(!MapPlotQuaMangrInspDate1.containsKey(E.ID)){
             if(E.Quality_Manager_Inspections__r!=null && E.Quality_Manager_Inspections__r.size()>0 && E.Quality_Manager_Inspections__r[0].First_Inspection_Date__c!=null){
                 
                 string dtt = DateTime.newInstance(E.Quality_Manager_Inspections__r[0].First_Inspection_Date__c.year(),E.Quality_Manager_Inspections__r[0].First_Inspection_Date__c.month(),E.Quality_Manager_Inspections__r[0].First_Inspection_Date__c.day()).format('dd-MM-YYYY');
             MapPlotQuaMangrInspDate1.put(E.Id,dtt);
             
         } 
         else {
             
               MapPlotQuaMangrInspDate1.put(e.Id,'');
         }
         }else {
             
               MapPlotQuaMangrInspDate1.put(e.Id,'');
         }
          if(!MapPlotCompletedDate.containsKey(E.ID)){
              if(E.Forecast_Completion_Date__c!=null){
                  string dtt = DateTime.newInstance(E.Forecast_Completion_Date__c.year(),E.Forecast_Completion_Date__c.month(),E.Forecast_Completion_Date__c.day()).format('dd-MM-YYYY');
         		MapPlotCompletedDate.put(E.Id,dtt); 
              }else {
             
               MapPlotCompletedDate.put(e.Id,'TBC');
         }
         }
          else {
             
               MapPlotCompletedDate.put(e.Id,'');
         }
         
         
         //for coloring
         
         if(!MapPlotSubStrudtDateColor.containsKey(E.ID)){
             if(E.Sub_Structures__r!=null && E.Sub_Structures__r.size()>0 && E.Sub_Structures__r[0].Date_Completed__c!=null){
             MapPlotSubStrudtDateColor.put(E.Id,'Actual');
            
         } else if(String.valueof(e.Forecast_Sub_Structure_Completion_Date__c) != null){
                  MapPlotSubStrudtDateColor.put(e.Id,'Forecast');
         }
         else {
             
               MapPlotSubStrudtDateColor.put(e.Id,'');
         }
         }
         
         if(!MapPlotSuprstructDateColor.containsKey(E.ID))
          {
              if(E.Super_Structures__r!=null && E.Super_Structures__r.size()>0 && E.Super_Structures__r[0].Date_Completed__c!=null){
             MapPlotSuprstructDateColor.put(E.Id,'Actual');
              
         }
         else if(String.valueof(e.Forecast_Super_Structure_Completion_Date__c) != null){
                  MapPlotSuprstructDateColor.put(e.Id,'Forecast');
         }
         else {
             
               MapPlotSuprstructDateColor.put(e.Id,'');
         }
          }
         
          if(!MapPlotFirstFixDateColor.containsKey(E.ID))
          {
              if(E.First_Fixes__r!=null && E.First_Fixes__r.size()>0 && E.First_Fixes__r[0].Date_Completed__c!=null){
              
             MapPlotFirstFixDateColor.put(E.Id,'Actual');
              
         }
         else if(String.valueof(e.Forecast_First_Fix_Completion_Date__c) != null){
                  MapPlotFirstFixDateColor.put(e.Id,'Forecast');
         }
         else {
             
               MapPlotFirstFixDateColor.put(e.Id,'');
         }
          }
          
          if(!MapPlotPreDecorationDateColor.containsKey(E.ID))
          {
              if(E.Pre_Decoration__r!=null && E.Pre_Decoration__r.size()>0  && E.Pre_Decoration__r[0].Date_Completed__c!=null){
             MapPlotPreDecorationDateColor.put(E.Id,'Actual');
              
         } 
          else if(String.valueof(e.Forecast_Pre_Decoration_Completion_Date__c) != null){
                  MapPlotPreDecorationDateColor.put(e.Id,'Forecast');
         }
         else {
             
               MapPlotPreDecorationDateColor.put(e.Id,'');
         }
     }
         
         if(!MapPlotCompQuAuditDateColor.containsKey(E.ID)){
             if(E.Completion_Quality_Audits__r!=null && E.Completion_Quality_Audits__r.size()>0  && E.Completion_Quality_Audits__r[0].Date_Completed__c!=null){
             MapPlotCompQuAuditDateColor.put(E.Id,'Actual');
             
         }
          else if(String.valueof(e.Forecast_Completion_Quality_Audit_Date__c) != null){
                  MapPlotCompQuAuditDateColor.put(e.Id,'Forecast');
         }
         else {
             
               MapPlotCompQuAuditDateColor.put(e.Id,'');
         }
         }
         
         if(!MapPlotQuaMangrInspDateColor.containsKey(E.ID)){
             if(E.Quality_Manager_Inspections__r!=null && E.Quality_Manager_Inspections__r.size()>0  && E.Quality_Manager_Inspections__r[0].Date_Completed__c!=null && E.Quality_Manager_Inspections__r[0].Status__c == 'Completed'){
             MapPlotQuaMangrInspDateColor.put(E.Id,'Actual');
           
         } else if(String.valueof(e.Forecast_Quality_Manager_Inspection_Date__c) != null){
                  MapPlotQuaMangrInspDateColor.put(e.Id,'Forecast');
         }
         else {
             
               MapPlotQuaMangrInspDateColor.put(e.Id,'');
         }
         }
         if(!MapPlotQuaMangrInspDateColor1.containsKey(E.ID)){
             if(E.Quality_Manager_Inspections__r!=null && E.Quality_Manager_Inspections__r.size()>0  && E.Quality_Manager_Inspections__r[0].First_Inspection_Date__c!=null){
             MapPlotQuaMangrInspDateColor1.put(E.Id,'Actual');
           
         } 
         else {
             
               MapPlotQuaMangrInspDateColor1.put(e.Id,'');
         }
         }
          if(!MapPlotCompletedDateColor.containsKey(E.ID)){ 
              if(E.Forecast_Completion_Date__c!=null){
         		
         		if(MapPlotQuaMangrInspDate.get(E.id)!='' && MapPlotCompletedDate.get(E.id)!='')
         		{
         		    List<String> strdate = MapPlotQuaMangrInspDate.get(E.id).split('-');
         		 Date  dtinspection =    Date.newInstance(Integer.valueof(strdate[2]), Integer.valueof(strdate[1]),Integer.valueof(strdate[0]));
         		  List<String> strdateComplete = MapPlotCompletedDate.get(E.id).split('-');
         		 Date dtinspectionComplete =    Date.newInstance(Integer.valueof(strdateComplete[2]), Integer.valueof(strdateComplete[1]),Integer.valueof(strdateComplete[0]));
                  Integer Remain =  dtinspection.daysbetween(dtinspectionComplete);
                  Integer range=7;
                   if(RAG_Range__c.getAll()!=null) 
                   {
                       for(String st : RAG_Range__c.getAll().keyset())
                       range=Integer.valueof(RAG_Range__c.getAll().get(st).maximum__c);
                      
                   }
                
                  if (Remain > range){
                     MapPlotCompletedDateColor.put(e.Id,'Background-Color:#03CC03;');
                  }
                  else if (Remain <range && Remain > 0){
                     MapPlotCompletedDateColor.put(e.Id,'Background-Color:#EBB501;');
                      
                  }
                  else if ( Remain <= 0) {
                      // system.debug('Remain-->'+remain);
                      MapPlotCompletedDateColor.put(e.Id,'Background-Color:#FF3333;');
                  }else {
             
               MapPlotCompletedDateColor.put(e.Id,'Background-Color:#03CC03;');
                    }
                    
              }else {
             
               MapPlotCompletedDateColor.put(e.Id,'Background-Color:#eaebed;');
                    }
                  
         
               } else {
             
               MapPlotCompletedDateColor.put(e.Id,'Background-Color:#03CC03;');
         }
             
               
               }else {
             
               MapPlotCompletedDateColor.put(e.Id,'Background-Color:#eaebed;');
         }
        
        
        } 
    //   for(Plot__c p: MapPlotCompQuAuditDate.values()){
            system.debug('MapDevAndPlot'+MapDevAndPlot.get('a000E000000xM3Y'));
            
    //     }
        
        for (ID devid : MapDevAndPlot.keyset())
        {
            List<MemberWrapper>memberWrapperList = new  List<MemberWrapper>();
            for(Plot__c mem : MapDevAndPlot.get(devid)){
            memberWrapperList.add(new MemberWrapper(mem,MapPlotCompQuAuditDate));
        }
        List<Plot__c> plotList = new  List<Plot__c> ();
        if(memberWrapperList.size()>0)
        {
        memberWrapperList.sort();
        plotList.clear();
        
        for(MemberWrapper MW : memberWrapperList){
            plotList.add(MW.member);
            
        }
        MapDevAndPlot.put(devid,plotList);
        }
        }
        
     
        
    
    
}
  public pageReference exportMethod(){
        
         PageReference pr = new PageReference('/apex/WeeklyConstructionQAReportPDF?dt='+dt);
         
       
         pr.setRedirect(true);
         return pr;
        }  
    
        
 public class MemberWrapper implements Comparable {

        public Plot__c member = new Plot__c();
        public Map<id,String>MapPlotCompQuAuditDate;
        
        
        
        // Constructor
        public MemberWrapper(Plot__c memberRecord,Map<id,String>MapPlotCompQuAuditDate) {
            member = memberRecord;
            this.MapPlotCompQuAuditDate= MapPlotCompQuAuditDate;
        }
        
        // Compare members based on the Member__c Age.
        public Integer compareTo(Object compareTo) {
            // Cast argument to MemberWrapper
            if(compareTo!=null)
            {
                
              MemberWrapper compareTomember = (MemberWrapper)compareTo;
              
              if((MapPlotCompQuAuditDate.get(member.id)==null ||  MapPlotCompQuAuditDate.get(member.id)=='' )
              && (MapPlotCompQuAuditDate.get(compareTomember.member.id)==null ||  MapPlotCompQuAuditDate.get(compareTomember.member.id)==''))
              {
                     String plotMembernum =member.Plot_Number__c ;
            String plotnum ='';
            for(string s : plotMembernum.split('(?!^)')){
                    if(s.isNumeric()){
                        plotnum = plotnum+s;
                    }
                    else
                    {
                       
                        break;
                    }
                }
            String plotComparetoMembernum=compareTomember.member.Plot_Number__c;
             String plotnumCompareto ='';
          for(string s : plotComparetoMembernum.split('(?!^)')){
                    if(s.isNumeric()){
                        plotnumCompareto = plotnumCompareto+s;
                    }
                    else
                    {
                       
                        break;
                    }
                }
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            system.debug(plotnum+'Hormese123'+plotnumCompareto);
            if(plotnumCompareto!='' && plotnum!='' && plotnum.isNUmeric() && plotnumCompareto.isNumeric() )
            {
            if (Integer.valueOf(plotnum) > Integer.valueOf(plotnumCompareto)) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (Integer.valueOf(plotnum) < Integer.valueOf(plotnumCompareto)) {
                // Set return value to a negative value.
                returnValue = -1;
            }
             system.debug(plotnum+'Hormese123'+plotnumCompareto);
               system.debug(plotnum+'Hormese124'+returnValue);
            return returnValue;       
            }
            else if(plotnum=='')
            {
             return 1;
             
            }
            else
            return 0;
              }
              else if( MapPlotCompQuAuditDate.get(member.id)==null ||  MapPlotCompQuAuditDate.get(member.id)=='')
              return 1;
              else if(MapPlotCompQuAuditDate.get(compareTomember.member.id)==null ||  MapPlotCompQuAuditDate.get(compareTomember.member.id)=='')
              return -1;
              
            List<String> MemberOriginalDate =  MapPlotCompQuAuditDate.get(member.id).split('-');
             Date  OriginalDate =    Date.newInstance(Integer.valueof(MemberOriginalDate[2]), Integer.valueof(MemberOriginalDate[1]),Integer.valueof(MemberOriginalDate[0]));
             
              
            List<String> MemberCompareToDate = MapPlotCompQuAuditDate.get(compareTomember.member.id).split('-');
            Date ComparetoDate =    Date.newInstance(Integer.valueof(MemberCompareToDate[2]), Integer.valueof(MemberCompareToDate[1]),Integer.valueof(MemberCompareToDate[0]));
         		  
            if(OriginalDate>ComparetoDate ){
               
                return 1;
            }
            else if(OriginalDate<ComparetoDate ){
            
              return -1;
            }
            
              
                  else {
                      
              
            String plotMembernum =member.Plot_Number__c ;
            String plotnum ='';
            for(string s : plotMembernum.split('(?!^)')){
                    if(s.isNumeric()){
                        plotnum = plotnum+s;
                    }
                    else
                    {
                       
                        break;
                    }
                }
            String plotComparetoMembernum=compareTomember.member.Plot_Number__c;
             String plotnumCompareto ='';
          for(string s : plotComparetoMembernum.split('(?!^)')){
                    if(s.isNumeric()){
                        plotnumCompareto = plotnumCompareto+s;
                    }
                    else
                    {
                       
                        break;
                    }
                }
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if(plotnumCompareto!='' && plotnum!='' && plotnum.isNUmeric() && plotnumCompareto.isNumeric() )
            {
            if (Integer.valueOf(plotnum) > Integer.valueOf(plotnumCompareto)) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (Integer.valueOf(plotnum) < Integer.valueOf(plotnumCompareto)) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue;       
            }
            else if(plotnum=='')
            {
             return 1;
             
            }
            else
            return 0;
        }
        return 0;
        }
        else
        return 0;
    
}
}
}