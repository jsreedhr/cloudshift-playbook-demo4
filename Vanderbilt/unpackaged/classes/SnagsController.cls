public without sharing class SnagsController {
    
    public list <Snag__c> listSnags{get; set;}
    public list<Plot__c> listPlot{get; set;}
    public string rowNo{get;set;}
    Public String PlotId;
    public integer noOfRows{get; set;}
    //public integer projectDuration{get; set;}
    public map<string,integer> mapMonthNo;
    public map<integer,string> mapNoMonth;
    public List<booleanProjectWrapper> lstOfbooleanProjectWrapper{get;set;}
    public boolean selectAll{get; set;}
    public Boolean showerror{get; set;}
    public integer errorPos{get; set;}
    //constructor to grt the records
    
    public SnagsController(apexpages.standardController stdController){
    
        listPlot = new list<Plot__c>();
        listSnags = new list<Snag__c>();
        lstOfbooleanProjectWrapper = new List<booleanProjectWrapper>(); 
        selectAll= false;
        noOfRows=1;
        PlotId = apexpages.currentpage().getparameters().get('id');
        listPlot = [select name, id, Development_Name__c, Development_Marketing_Name__c from Plot__c where id=: PlotId limit 1];
        if(listPlot!=null && listPlot.size()>0){
        listSnags = [select Id, Plot_Property__r.id,Category__c, Date_Completed__c, Description__c, Location__c, Status__c, CreatedDate, Name FROM Snag__c where Plot_Property__c =: PlotId limit 4999];
      
        for(Snag__c obj_wih :listSnags){
            lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,obj_wih));
            
        }
        }
    } 
    
    
    public Void createRecords() {
        selectAll=false;
         system.debug('######------------' + lstOfbooleanProjectWrapper.size());
        if(lstOfbooleanProjectWrapper.size()> 0)
        
        {
            
        integer l =lstOfbooleanProjectWrapper.size()-1;
        for(integer i=1 ; i<=lstOfbooleanProjectWrapper[0].ValuetoList; i++){
        Snag__c objwih = new Snag__c();
         system.debug('######------------++++' + l);
     
        objwih.Plot_Property__c = PlotId;
              
        listSnags.add(objwih);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objwih));
       
        }
        } 
        
        else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'You need atleast 1 record'));
        }
        
    }
        
        
    
    // To Create a new snag record on click of Add row button
    
    
    public Void addRow() {
        selectAll=false;
        for(integer i=0 ; i<noOfRows; i++){
        Snag__c objwih = new Snag__c();
        objwih.Plot_Property__c = PlotId;
        objwih.Status__c = 'Raised';
        listSnags.add(objwih);
        lstOfbooleanProjectWrapper.add(new booleanProjectWrapper(false,objwih));
        }      
    }
    
    public void selectAll(){
        
        
        if(selectAll==true){
            for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= true;
            }
            
        }
        else{
             for(booleanProjectWrapper wrapperlist :lstOfbooleanProjectWrapper){
                wrapperlist.isSelected= false;
            }
        }
    }
    
    Public void deleteSelectedRows(){
        selectAll=false;
        system.debug('----'+lstOfbooleanProjectWrapper);
        list<Snag__c> toDeleteRows = new list<Snag__c>();
        for(integer j =(lstOfbooleanProjectWrapper.size()-1); j>=0; j--){
            if(lstOfbooleanProjectWrapper[j].isSelected==true){
                if(lstOfbooleanProjectWrapper[j].objwih.id != null){
                    toDeleteRows.add(lstOfbooleanProjectWrapper[j].objwih);
                }
                
                lstOfbooleanProjectWrapper.remove(j);
                
            }
            
            
        }
          system.debug('----'+toDeleteRows);
        delete toDeleteRows;
    }
    /**
    *   Method Name:    DelRow 
    *   Description:    To delete the record by passing the row no.
    *   Param:  RowNo

    */
   
    public Void delRow() {
       
        selectAll=false;
        system.debug('--------'+RowNo);
        list<Snag__c> todelete = new list<Snag__c>();
        
        if(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objwih.id != null){
            todelete.add(lstOfbooleanProjectWrapper[integer.valueof(RowNo)].objwih);
            
        }
        
        
        lstOfbooleanProjectWrapper.remove(integer.valueof(RowNo));
        delete todelete;
       
        
    }
     /**
    *   Method Name:    saveWorkinHand 
    *   Description:    To save the records and then redirect it to respective opportunity
    */
    
    public PageReference saveWorkinHand() {
        if(showerror !=true)
        {
        list<Snag__c> ListtoUpsert= new list<Snag__c> ();
        boolean isBlank =false;
        //system.debug('@@@@@@@@@@'+listProjRev.size());
        Set<ID> oppId = new Set<ID>();
         for(booleanProjectWrapper objwih: lstOfbooleanProjectWrapper){
         
         
            if(string.valueof(objwih.objwih.Plot_Property__c) != null)
            {
           
                 ListtoUpsert.add(objwih.objwih); 
                 oppId.add(objwih.objwih.Plot_Property__c);
            }
          
                else if(string.valueof(objwih.objwih.Plot_Property__c) != null)
                {
            
                isBlank= true;
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'All the fields are required'));
               return null;                      
                
           }
            
        }
            
            for(booleanProjectWrapper objwih: lstOfbooleanProjectWrapper){
         
            system.debug('lstOfbooleanProjectWrapper --->' +lstOfbooleanProjectWrapper);
            system.debug('objwih.objwih.Status__c--->' +objwih.objwih.Status__c);
            system.debug('objwih.objwih.Location__c-->' +objwih.objwih.Location__c);
            system.debug('objwih.objwih.Category__c-->' +objwih.objwih.Category__c);
            system.debug('objwih.objwih.Description__c' +objwih.objwih.Description__c);
            
            if(objwih.objwih.Status__c==Null || objwih.objwih.Location__c==Null || objwih.objwih.Category__c==Null || objwih.objwih.Description__c==Null){
           
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
                                      'Please ensure every snag has a defined status, category, location and description'));  
            return null;  
            }         
                     
        }        
      
        try{
            Upsert ListtoUpsert;
        } 
            catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,
            'There is missing information on the plot, please edit and save the plot record to identify the missing information.'));                         
            return null; 
        }
    
          PageReference acctPage = new PageReference('/' + PlotId);
        acctPage.setRedirect(true);
        return acctPage;
        }
        else
        return null;
    }
    
      public class booleanProjectWrapper{
        public Boolean isSelected{get;set;}
        public Snag__c objwih{get;set;}
        public integer ValuetoList{get;set;}
        
        public booleanProjectWrapper(boolean isSelect, Snag__c objwihs){
          objwih = objwihs;
          isSelected= isSelect;
          ValuetoList=1;
           
        }
    }
    

}