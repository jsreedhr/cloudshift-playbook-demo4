global class schduleBatchMonthlyContractManager implements Schedulable {

   global void execute(SchedulableContext ctx) {
      ContractsManagerMonthlyBatch p = new ContractsManagerMonthlyBatch();
        database.executeBatch(p);
   }   
}