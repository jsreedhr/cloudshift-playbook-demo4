/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_IncentivesTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_IncentivesTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new Incentives__c());
    }
}