@istest

/**
 *  @Company: dQuotient
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Nitha T S                 01/08/2017         Last Modified
 */

public class TestOpportunityTriggerFunctions{
        public testmethod static void Test_updateStatus(){
                           
                User u1 = new User();
                Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
                u1.ProfileId = profileId.id;
                u1.LastName = 'last';
                u1.Email = 'puser000@amamama.com';
                u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
                u1.CompanyName = 'TEST';
                u1.Title = 'title';
                u1.Alias = 'alias';
                u1.TimeZoneSidKey = 'America/Los_Angeles';
                u1.EmailEncodingKey = 'UTF-8';
                u1.LanguageLocaleKey = 'en_US';
                u1.LocaleSidKey = 'en_US';
                insert u1;
            
                Account a = new Account();
                a.name ='ABC';
                a.type ='Other';
                Insert a;
            
                contact c = new contact();
                c.lastname='ABC';
                c.accountId=a.id;
                c.email='abc@gmail.com';
                c.Salutation='Mr.';
                insert c;
            
            	Development__c dev = new Development__c ();
    			dev.Marketing_Name__c = 'Court Road';
    			dev.Name = 'Abingdon';
    			dev.COINS_Development_Code__c = '0009';
    			dev.Street__c = '87 North Court Road';
                dev.City__c = 'Abingdon';
                dev.Country__c = 'United Kingdom';
                dev.Post_Code__c = 'OX14 1NN';
            	dev.Number_of_Plots__c = 1;
                dev.Contracts_Manager__c = u1.id;
                dev.Site_Manager__c = u1.id;
                dev.Sales_Manager__c = u1.id;
                dev.Active__c = TRUE;
            
            	insert dev;
                
                Plot__c p = new Plot__c();
                p.name = 'abc';
            	p.Development__c = dev.id;
                p.Plot_Number__c = 't5';
                p.Sales_Status__c = 'reserved';
                p.Build_Status__c = 'foundations';
                p.Release_Price__c = 8787;
                p.Release_Price_Approved__c = true;
                p.Sales_Status__c = 'Not Released';
                p.NHBC_PG_Reference_Number__c='dsfs';
                p.Number_of_Beds__c = 5;
                p.Open_Market_vs_Affordable__c = 'Affordable';
                p.Size_Sq_Foot__c = 57;
            	p.House_Type__c = 'House';
                
                insert p;
                
                Coins_Credentials__c cred = new Coins_Credentials__c();
                cred.Name = 'Credentials';
                cred.Coins_Endpoint_URL__c = 'dgcdhc';
                cred.Coins_Environment__c = 'dcwc';
                cred.Coins_Password__c = '#fcsd';
                cred.Coins_Username__c = 'qwerty';
                insert cred;
           
                list<opportunity> updatedOppList = new list<Opportunity>();
                list<opportunity> oppList = new list<Opportunity>();
                opportunity op = new opportunity();
                op.amount = 1000;
                op.Reservation_Fee__c=900; 
                op.AccountId=a.id;
                op.name='XYZ';
                op.Reservation_Approved__c = true;
                op.StageName='Reserved';
                op.CloseDate= system.today() +365;
                op.Anticipated_Exchange_Date__c =system.today() +300;
                op.Reservation_Date__c = system.today() +2;
                op.Reservation_Deadline__c = system.today() +10;
                op.Primary_Purchaser__c = c.id;
                op.Plot_Property__c = p.id;
                op.Actual_Cancellation_Date__c = system.today() -1;
                op.Contracts_Out__c = system.today();
                op.Plot_Property__c = p.id;
                oppList.add(op);
                
                Plot__c p1 = new Plot__c();
                p1.name = 'abc';
            	p1.Development__c = dev.id;
                p1.Plot_Number__c = 't5';
                p1.Sales_Status__c = 'reserved';
                p1.Build_Status__c = 'foundations';
                p1.Release_Price__c = 8787;
                p1.Release_Price_Approved__c = true;
                p1.Sales_Status__c = 'Not Released';
                p1.NHBC_PG_Reference_Number__c='dsfs';
                p1.Number_of_Beds__c = 5;
                p1.Open_Market_vs_Affordable__c = 'Affordable';
                p1.Size_Sq_Foot__c = 57;
            	p1.House_Type__c = 'House';
                insert p1;
                
                opportunity op1 = new opportunity();
                op1.amount = 1000;
                op1.Reservation_Fee__c=900; 
                op1.AccountId=a.id;
                op1.name='XYZ';
                op1.Reservation_Approved__c  = true;
                op1.StageName='Reserved';
                op1.CloseDate= system.today() + 355;
                op.Anticipated_Exchange_Date__c =system.today() +300;
                op1.Reservation_Date__c = system.today() +2;
                op1.Reservation_Deadline__c = system.today() +10;
                op1.Primary_Purchaser__c = c.id;
                op1.Actual_Cancellation_Date__c = system.today() -1;
                op1.Contracts_Out__c = system.today();
                op1.Plot_Property__c = p1.id;
                oppList.add(op1);
                
                Plot__c p2 = new Plot__c();
                p2.name = 'abc';
            	p2.Development__c = dev.id;
                p2.Plot_Number__c = 't5';
                p2.Sales_Status__c = 'reserved';
                p2.Build_Status__c = 'foundations';
                p2.Release_Price__c = 8787;
                p2.Release_Price_Approved__c = true;
                p2.Sales_Status__c = 'Not Released';
                p2.NHBC_PG_Reference_Number__c='dsfs';
                p2.Number_of_Beds__c = 5;
                p2.Open_Market_vs_Affordable__c = 'Affordable';
                p2.Size_Sq_Foot__c = 57;
            	p2.House_Type__c = 'House';
                insert p2;
                
                opportunity op2 = new opportunity();
                op2.amount = 1000;
                op2.Reservation_Fee__c=900; 
                op2.AccountId=a.id;
                op2.name='XYZ';
                op2.Reservation_Approved__c = true;
                op2.StageName='Reserved';
                op2.CloseDate= system.today() + 355;
                op.Anticipated_Exchange_Date__c =system.today() +300;
                op2.Reservation_Date__c = system.today() +2;
                op2.Reservation_Deadline__c = system.today() +10;
                op2.Primary_Purchaser__c = c.id;
                op2.Actual_Cancellation_Date__c = system.today() -1;
                op2.Contracts_Out__c = system.today();
                op2.Plot_Property__c = p2.id;
                oppList.add(op2);
                
                Plot__c p3 = new Plot__c();
                p3.name = 'abc';
            	p3.Development__c = dev.id;
                p3.Plot_Number__c = 't5';
                p3.Sales_Status__c = 'reserved';
                p3.Build_Status__c = 'foundations';
                p3.Release_Price__c = 8787;
                p3.Release_Price_Approved__c = true;
                p3.Sales_Status__c = 'Not Released';
                p3.NHBC_PG_Reference_Number__c='dsfs';
                p3.Number_of_Beds__c = 5;
                p3.Open_Market_vs_Affordable__c = 'Affordable';
                p3.Size_Sq_Foot__c = 57;
            	p3.House_Type__c = 'House';
                insert p3;
                
                opportunity op3 = new opportunity();
                op3.amount = 1000;
                op3.Reservation_Fee__c=900; 
                op3.AccountId=a.id;
                op3.name='XYZ';
                op3.Reservation_Approved__c  = true;
                op3.StageName='Reserved';
                op3.CloseDate= system.today()  + 355;
                op.Anticipated_Exchange_Date__c =system.today() +300;
                op3.Reservation_Date__c = system.today() +2;
                op3.Reservation_Deadline__c = system.today() +10;
                op3.Primary_Purchaser__c = c.id;
                op3.Actual_Cancellation_Date__c = system.today() -1;
                op3.Contracts_Out__c = system.today();
                op3.Plot_Property__c = p3.id;
                oppList.add(op3);
                
            
                insert oppList;           
        
                
                op.stageName = 'Reserved - Approved';
                op.Contracts_Out__c = system.today();
                
                op1.Actual_Exchange_Date__c = system.today();
                op1.stageName = 'Exchanged';
            	op1.Reservation_signed__c = System.today();
            	op1.CML_disclosure_form_completed_attached__c = TRUE;
            	op1.Customer_Changes_for_Construction__c = 'No';
                op1.Cml_Form_Completed__c = system.today();
                op1.Contract_Signed__c = system.today();
                op1.Deposit_Funds__c = system.today();
                op1.Enquiries_Raised__c = system.today();
                op1.Exchange_Funds_Received__c = 200;
                op1.Exchange_Funds_Received_Date__c = system.today();
                op1.Id_Received__c = system.today();
                op1.Papers_Received__c = System.today();
                op1.Proceedable_date__c = System.today();
                op1.Res_Memo_Sent__c = system.today();
                op1.Searches_Applied_For__c = system.today();
                op1.Searches_Paid__c = system.today();
                op1.Searches_Received__c = system.today();
                op1.Solictors_Info_Received__c = system.today();
                op1.Solicitors_Instructed__c = system.today();
                op1.Target_Exchange_Date__c = system.today();
                
                op2.Actual_Completion_Date__c = system.today() - 80;
            	op2.Customer_Changes_for_Construction__c = 'No';
            	op2.CML_disclosure_form_completed_attached__c = TRUE;
            	op2.Electric_Meter_Reading__c = '1234';
            	op2.Electric_Meter_Reading_Date__c = System.Today();
            	op2.Gas_Meter_Reading__c = '1234';
            	op2.Gas_Meter_Reading_Date__c = System.today();
            	op2.Water_Meter_Reading__c = '1234';
            	op2.Water_Meter_Reading_Date__c = System.today();
                op2.stageName = 'Completed';
                op2.Contract_Signed__c = system.today();
                op2.Completion_Date_Set__c = system.today();
                op2.Completion_Funds_Available__c = TRUE;
                op2.Customer_Demo_Booked__c = system.today();
            
                op3.stageName = 'Cancelled';
                op3.Contract_Signed__c = system.today();
                
                updatedOppList.add(op);
                updatedOppList.add(op1);
                updatedOppList.add(op2);
                updatedOppList.add(op3);
                
                
                update  updatedOppList;
                
                
        
        }
        public testmethod static void Test_MarketingOptOut()
        {

            RecordType personAccountRecordType =  [Select Id FROM RecordType where Name = 'Person Account' and SObjectType='Account' LIMIT 1];
            RecordType businessAccountRecordType = [Select id from RecordType where Name = 'Business Account' and SObjectType='Account' LIMIT 1];
                
            Account newPersonAccount = new Account();
            newPersonAccount.Salutation='Mr.';
            newPersonAccount.FirstName='James';
            newPersonAccount.LastName='Bond';
            newPersonAccount.PersonEmail='jb@mi6.com';
            newPersonAccount.RecordType=personAccountRecordType;
            insert newPersonAccount;

            Account newBusinessAccount = new Account();
            newBusinessAccount.Type='Other';
            newBusinessAccount.name='Test';
            newBusinessAccount.RecordType=businessAccountRecordType;
            insert newBusinessAccount;

            contact c1 = new contact();
            c1.salutation='Mr';
            c1.lastName='Primary Person';
            c1.email='jb@mi6.com';
            c1.accountid=newBusinessAccount.id;
            insert c1;

            contact c2 = new contact();
            c2.salutation='Mr';
            c2.lastName='Primarys Person';
            c2.email='jb1@mi6.com';
            c2.accountid=newBusinessAccount.id;
            insert c2;

            Development__c dev = new Development__c ();
    		dev.Marketing_Name__c = 'Court Road';
    		dev.Name = 'Abingdon';
    		dev.COINS_Development_Code__c = '0009';
    		dev.Street__c = '87 North Court Road';
            dev.City__c = 'Abingdon';
            dev.Country__c = 'United Kingdom';
            dev.Post_Code__c = 'OX14 1NN';
            dev.Number_of_Plots__c = 1;
            
            dev.Active__c = TRUE;
            insert dev;

            Plot__c p = new Plot__c();
            p.Development__c = dev.id;
            p.name = 'abc';
            p.Plot_Number__c = 't5';
            p.Sales_Status__c = 'reserved';
            p.Build_Status__c = 'foundations';
            p.Release_Price__c = 8787;
            p.Release_Price_Approved__c = true;
            p.Sales_Status__c = 'Not Released';
            p.NHBC_PG_Reference_Number__c='dsfs';
            p.Number_of_Beds__c = 3;
            p.Open_Market_vs_Affordable__c = 'Affordable';
            p.Size_Sq_Foot__c = 57;
            p.House_Type__c = 'House';
            insert p;

            Plot__c p2 = new Plot__c();
            p2.Development__c = dev.id;
            p2.name = 'abcd';
            p2.Plot_Number__c = 't6';
            p2.Sales_Status__c = 'reserved';
            p2.Build_Status__c = 'foundations';
            p2.Release_Price__c = 8787;
            p2.Release_Price_Approved__c = true;
            p2.Sales_Status__c = 'Not Released';
            p2.NHBC_PG_Reference_Number__c='dsfs';
            p2.Number_of_Beds__c = 3;
            p2.Open_Market_vs_Affordable__c = 'Affordable';
            p2.Size_Sq_Foot__c = 57;
            p2.House_Type__c = 'House';
            insert p2;

            list<opportunity> oppList = new list<Opportunity>();
            
            //Opportunity with Person Account
            Opportunity op = new Opportunity();
            op.amount = 1000;
            op.Reservation_Fee__c=900; 
            op.AccountId=newPersonAccount.id;
            op.name='XYZ';
            op.Reservation_Approved__c = true;
            op.StageName='Reserved';
            op.CloseDate= system.today() +365;
            op.Anticipated_Exchange_Date__c =system.today() +300;
            op.Reservation_Date__c = system.today() +2;
            op.Reservation_Deadline__c = system.today() +10;
            op.Primary_Purchaser__c = c1.id;
            op.Secondary_Purchaser__c=c2.id;
            op.Plot_Property__c = p.id;
            insert op;
            op.name='ABC';
            update op;

            Account personAccount = [Select personcontactId from Account where id=:newPersonAccount.id];
            Opportunity op2 = new Opportunity();
            op2.amount = 1000;
            op2.Reservation_Fee__c=900; 
            op2.AccountId=newPersonAccount.id;
            op2.name='XYZ';
            op2.Reservation_Approved__c = true;
            op2.StageName='Reserved';
            op2.CloseDate= system.today() +365;
            op2.Anticipated_Exchange_Date__c =system.today() +300;
            op2.Reservation_Date__c = system.today() +2;
            op2.Reservation_Deadline__c = system.today() +10;
            op2.Primary_Purchaser__c = personAccount.personContactId;
            op2.Plot_Property__c = p2.id;
            insert op2;
            op2.name='ABC';
            update op2;
        }
    



}