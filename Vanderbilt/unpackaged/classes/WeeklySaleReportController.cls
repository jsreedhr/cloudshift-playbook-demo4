public with sharing class WeeklySaleReportController {
        
        public list<opportunity> reservationSummaryLst {get;set;}
        public list<MemberWrapper> reservationSummaryWrapLst {get;set;}
        public list<opportunity> reservationcancellationLst {get;set;}
         public list<MemberWrapper> reservationcancellationLstwrap {get;set;}
        public list<opportunity> reservationtoExchangeLst {get;set;}
         public list<MemberWrapper> reservationtoExchangeLstwrap {get;set;}
        public list<opportunity> exchangedPlotToCompleteLst {get;set;}
         public list<MemberWrapper> exchangedPlotToCompleteLstwrap {get;set;}
        public list<opportunity> homesCompletedinaWeekLst {get;set;}
         public list<MemberWrapper> homesCompletedinaWeekLstwrap {get;set;}
        public list<opportunity> SummaryLst {get;set;}
         public list<MemberWrapper> SummaryLstwrap {get;set;}
        public List<string> Summary{get;set;}
        public Integer weekNo {get;set;}
        public string dtstring {get;set;}
        public date dt {get;set;}
        
        public integer totalReservationsToExchange{get;set;}
        public integer totalExchangedPlotsToComplete{get;set;}
        public integer totalCompletions{get;set;}
        
        public integer totalReservations{get;set;}
        public integer totalCancellations{get;set;}
        public integer totalNetReservations{get;set;}
        public List<subtotalwrp> subtotalwrpList{get;set;}
       
        public date dtForStartMonth {get;set;}
        public date dtForEndMonth {get;set;}
        public date dtForStartYear {get;set;}
        public date dtForEndYear {get;set;}
        
        public integer monthTodateTotalReservations{get;set;}
        public integer monthTodateTotalCancellations{get;set;}
        public integer monthTodateTotalNetReservations{get;set;}
        public integer monthTodateTotalExchanges{get;set;}
        public integer monthTodateTotalCompletions{get;set;}
        
        public integer yearTodateTotalReservations{get;set;}
        public integer yearTodateTotalCancellations{get;set;}
        public integer yearTodateTotalNetReservations{get;set;}
        public integer yearTodateTotalExchanges{get;set;}
        public integer yearTodateTotalCompletions{get;set;}
        
        public date weekStartDate1;
        public date weekEndDate1;
        
        public List<wrapperSummary> lstSummary{get;set;}
        
        
        
        public WeeklySaleReportController(){
          string a = ApexPages.currentPage().getParameters().get('dt');
           reservationSummaryWrapLst = new list<MemberWrapper>();
           reservationcancellationLstwrap = new list<MemberWrapper>();
           reservationtoExchangeLstwrap = new list<MemberWrapper>();
           exchangedPlotToCompleteLstwrap = new list<MemberWrapper>();
           homesCompletedinaWeekLstwrap = new list<MemberWrapper>();
           SummaryLstwrap= new list<MemberWrapper>();
            
            if(a== null){
            
                dt = (System.today().toStartofWeek());
                
                dt = dt.addDays(-1);
              //  dt = dt.addDays(7);
                
                weekStartDate1 = dt.addDays(-6);
            }
            else{
                 // value in url 2016-12-04 where 04 is date 
                
                a = a.substring(0,a.indexof(' '));
             
                List<String> aList = a.split('-');  // this should give ind0-2016 ind1-12 ind2-04
                 dt = Date.newInstance(Integer.valueof(aList[0]),integer.valueOf(aList[1]),integer.valueOf(aList[2]));
                 
                    weekStartDate1 = dt;
            }
            
            dtstring = dt.format(); 
            List<string> dtList = dtstring.split('/');
            
         //   dtstring = dtList[1]+'/'+dtList[0]+'/'+dtList[2]; 
            dtstring = dtList[1]+'/'+dtList[0]+'/'+dtList[2];
            dateTime tt = dt.addDays(-7); 
            weekNo = integer.valueof(tt.Format('w'));
            
           
            GenerateWeekNo();
   
        }
        
        public void GenerateWeekNo(){

                        Summary = new List<String>{'Summary'};
                        
                        List<string> dtstr = dtstring.split('/');
                     //   dt = Date.newInstance(Integer.valueof(dtstr[2]),Integer.valueof(dtstr[0]),Integer.valueof(dtstr[1])); 
                        dt = Date.newInstance(Integer.valueof(dtstr[2]),Integer.valueof(dtstr[0]),Integer.valueof(dtstr[1]));
                        dtForStartMonth = (dt.toStartOfMonth()); 
                        dtForStartYear = Date.newInstance(dt.year(),1,1);
                        dtForEndMonth = dtForStartMonth.addDays(30);
                        dtForEndYear = dtForStartYear.addDays(365);
        
                        weekStartDate1 = dt.addDays(-6);

                        System.Debug('Month Date:'+dtForStartMonth+'Year:'+dtForStartYear); 
       
       
       
        dateTime pp = dt.addDays(-7);
        
        weekNo = integer.valueof(pp.Format('w'));
      
        reservationSummaryLst = [select id,site__C,Plot_Number__c,Price_Sq_Foot__c,Primary_Purchaser_Last_Name__c,Reservation_Date__c,Plot_Description__c,Size_Sq_Foot__c,Sales_Release_Price__c,Financial_Achievable_Price__c,Reservation_Net_Price__c,Net_vs_Release_Price__c,Net_vs_Achievable_Price__c,Percent_vs_Achievable__c,Weekly_Report_Comments__c,Anticipated_Exchange_Date__c,Actual_Exchange_Date__c,CloseDate,Actual_Completion_Date__c  from opportunity where stageName ='Reserved - Approved' and Reservation_Date__c >= :weekStartDate1  and Reservation_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' order by Site__c,Plot_Number__c];
        reservationcancellationLst = [select id,site__C,Plot_Number__c,Price_Sq_Foot__c,Primary_Purchaser_Last_Name__c,Reservation_Date__c,Plot_Description__c,Size_Sq_Foot__c,Sales_Release_Price__c,Financial_Achievable_Price__c,Reservation_Net_Price__c,Net_vs_Release_Price__c,Net_vs_Achievable_Price__c,Percent_vs_Achievable__c,Weekly_Report_Comments__c,Anticipated_Exchange_Date__c,Actual_Exchange_Date__c,CloseDate,Actual_Completion_Date__c  from opportunity where stageName ='Cancelled' and Actual_cancellation_date__c >= :weekStartDate1  and Actual_cancellation_date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' order by Site__c,Plot_Number__c];
        subtotalwrpList = new List<subtotalwrp>();
        integer size=0,sqft=0, srp=0, fap=0, rnp=0;
        for(opportunity Opp: reservationSummaryLst){
            sqft +=Opp.Size_Sq_Foot__c!=null?Integer.valueof(Opp.Size_Sq_Foot__c):0;
            srp += Opp.Sales_Release_Price__c!=null?Integer.valueof(Opp.Sales_Release_Price__c):0;
            fap+=Opp.Financial_Achievable_Price__c!=null?Integer.valueof(Opp.Financial_Achievable_Price__c):0;
            rnp+=Opp.Reservation_Net_Price__c!=null?Integer.valueof(Opp.Reservation_Net_Price__c):0;
        }
        subtotalwrpList.add(new subtotalwrp(reservationSummaryLst.size(), sqft, srp,fap,rnp));
        size=0;sqft=0; srp=0; fap=0; rnp=0;
        for(opportunity Opp: reservationcancellationLst){
             sqft +=Opp.Size_Sq_Foot__c!=null?Integer.valueof(Opp.Size_Sq_Foot__c):0;
            srp += Opp.Sales_Release_Price__c!=null?Integer.valueof(Opp.Sales_Release_Price__c):0;
            fap+=Opp.Financial_Achievable_Price__c!=null?Integer.valueof(Opp.Financial_Achievable_Price__c):0;
            rnp+=Opp.Reservation_Net_Price__c!=null?Integer.valueof(Opp.Reservation_Net_Price__c):0;
        }
        subtotalwrpList.add(new subtotalwrp(reservationcancellationLst.size(), sqft, srp,fap,rnp));
        
        reservationtoExchangeLst = [select id,site__C,Reservation_Deadline__c,Plot_Number__c,Primary_Purchaser_Last_Name__c,Reservation_Date__c,Plot_Description__c,Size_Sq_Foot__c,Sales_Release_Price__c,Financial_Achievable_Price__c,Reservation_Net_Price__c,Net_vs_Release_Price__c,Net_vs_Achievable_Price__c,Percent_vs_Achievable__c,Weekly_Report_Comments__c,Anticipated_Exchange_Date__c,Actual_Exchange_Date__c,CloseDate,Actual_Completion_Date__c  from opportunity where stageName ='Reserved - Approved'and Reservation_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' order by Site__c,Plot_Number__c];
        exchangedPlotToCompleteLst = [select id,site__C,Reservation_Deadline__c,Plot_Number__c,Primary_Purchaser_Last_Name__c,Reservation_Date__c,Plot_Description__c,Size_Sq_Foot__c,Sales_Release_Price__c,Financial_Achievable_Price__c,Reservation_Net_Price__c,Net_vs_Release_Price__c,Net_vs_Achievable_Price__c,Percent_vs_Achievable__c,Weekly_Report_Comments__c,Anticipated_Exchange_Date__c,Actual_Exchange_Date__c,CloseDate,Actual_Completion_Date__c  from opportunity where stageName ='Exchanged' and Actual_Exchange_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' order by Site__c,Plot_Number__c];
        homesCompletedinaWeekLst = [select id,site__C,Reservation_Deadline__c,Plot_Number__c,Primary_Purchaser_Last_Name__c,Reservation_Date__c,Plot_Description__c,Size_Sq_Foot__c,Sales_Release_Price__c,Financial_Achievable_Price__c,Reservation_Net_Price__c,Net_vs_Release_Price__c,Net_vs_Achievable_Price__c,Percent_vs_Achievable__c,Weekly_Report_Comments__c,Anticipated_Exchange_Date__c,Actual_Exchange_Date__c,CloseDate,Actual_Completion_Date__c,Plot_Property__r.Forecast_Completion_Date__c  from opportunity where stageName ='Completed' and Actual_Completion_Date__c   >= :weekStartDate1  and Actual_Completion_Date__c   <= :dt and Open_Market_vs_Affordable__c!='Affordable' order by Site__c,Plot_Number__c];
        SummaryLst = [select id,site__C,Reservation_Deadline__c,Plot_Number__c,Primary_Purchaser_Last_Name__c,Reservation_Date__c,Plot_Description__c,Size_Sq_Foot__c,Sales_Release_Price__c,Financial_Achievable_Price__c,Reservation_Net_Price__c,Net_vs_Release_Price__c,Net_vs_Achievable_Price__c,Percent_vs_Achievable__c,Weekly_Report_Comments__c,Anticipated_Exchange_Date__c,Actual_Exchange_Date__c,CloseDate,Actual_Completion_Date__c  from opportunity where stageName ='Reserved'];
      
        totalReservations = [select count() from Opportunity where stageName ='Reserved - Approved'and Open_Market_vs_Affordable__c!='Affordable' and Reservation_Date__c >= :weekStartDate1  and Reservation_Date__c <= :dt ];
        totalCancellations = [select count() from Opportunity where stageName ='Cancelled' and Actual_cancellation_date__c >= :weekStartDate1  and Actual_cancellation_date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable'];
        totalNetReservations = totalReservations - totalCancellations;
       
        totalReservationsToExchange = reservationtoExchangeLst.size();
        totalExchangedPlotsToComplete = exchangedPlotToCompleteLst.size();
        totalCompletions = homesCompletedinaWeekLst.size();
        
        
        monthTodateTotalReservations= [SELECT count() FROM Opportunity WHERE stageName in ('Reserved - Approved', 'Exchanged', 'Completed', 'Cancelled') and Reservation_Date__c >= :dtForStartMonth and Reservation_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable'];
       
        monthTodateTotalCancellations = [SELECT count() FROM Opportunity WHERE stageName ='Cancelled' and Actual_cancellation_date__c >= :dtForStartMonth and Actual_cancellation_date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' ];
        monthTodateTotalNetReservations = monthTodateTotalReservations - monthTodateTotalCancellations;
        monthTodateTotalExchanges = [SELECT count() FROM Opportunity WHERE Actual_Exchange_Date__c >= :dtForStartMonth and Actual_Exchange_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' ];
        monthTodateTotalCompletions = [SELECT count() FROM Opportunity WHERE Actual_Completion_Date__c >= :dtForStartMonth and Actual_Completion_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' ];
        
        yearTodateTotalReservations= [SELECT count() FROM Opportunity WHERE stageName in ('Reserved - Approved', 'Exchanged', 'Completed', 'Cancelled') and Reservation_Date__c >= :dtForStartYear and Reservation_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable'];
        yearTodateTotalCancellations = [SELECT count() FROM Opportunity WHERE stageName ='Cancelled' and Actual_cancellation_date__c >= :dtForStartYear and Actual_cancellation_date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' ];
        yearTodateTotalNetReservations = yearTodateTotalReservations - yearTodateTotalCancellations;
        yearTodateTotalExchanges = [SELECT count() FROM Opportunity WHERE  Actual_Exchange_Date__c >= :dtForStartyear and Actual_Exchange_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' ];
        yearTodateTotalCompletions = [SELECT count() FROM Opportunity WHERE Actual_Completion_Date__c >= :dtForStartyear and Actual_Completion_Date__c <= :dt and Open_Market_vs_Affordable__c!='Affordable' ];
        
        lstSummary = new list<wrapperSummary>();
         reservationSummaryWrapLst.clear();

        reservationSummaryWrapLst.clear();
        
        reservationtoExchangeLstwrap.clear();

        reservationcancellationLstwrap.clear();
        
        exchangedPlotToCompleteLstwrap.clear();
        
        homesCompletedinaWeekLstwrap.clear();
        
        SummaryLstwrap.clear();

          
        lstSummary.add(new wrapperSummary('Month to date',monthTodateTotalReservations,monthTodateTotalNetReservations,monthTodateTotalCancellations,monthTodateTotalExchanges,monthTodateTotalCompletions)); // for all the month
        lstSummary.add(new wrapperSummary('Year to date',yearTodateTotalReservations,yearTodateTotalNetReservations,yearTodateTotalCancellations,yearTodateTotalExchanges,yearTodateTotalCompletions)); // for all the year
        
        for(Opportunity opp : reservationSummaryLst)
        {
            MemberWrapper memb = new MemberWrapper(opp);
            reservationSummaryWrapLst.add(memb);
            
        }
        reservationSummaryWrapLst.sort();
        
        for(Opportunity opp : reservationtoExchangeLst)
        {
            MemberWrapper memb = new MemberWrapper(opp);
            reservationtoExchangeLstwrap.add(memb);
        }
        reservationtoExchangeLstwrap.sort();
        for(Opportunity opp : reservationcancellationLst)
        {
            MemberWrapper memb = new MemberWrapper(opp);
            reservationcancellationLstwrap.add(memb);
        }
        reservationcancellationLstwrap.sort();
        
        for(Opportunity opp : exchangedPlotToCompleteLst)
        {
            MemberWrapper memb = new MemberWrapper(opp);
            exchangedPlotToCompleteLstwrap.add(memb);
        }
        exchangedPlotToCompleteLstwrap.sort();
        
        for(Opportunity opp : homesCompletedinaWeekLst)
        {
            MemberWrapper memb = new MemberWrapper(opp);
            homesCompletedinaWeekLstwrap.add(memb);
        }
        homesCompletedinaWeekLstwrap.sort();
        
        for(Opportunity opp : SummaryLst)
        {
            MemberWrapper memb = new MemberWrapper(opp);
            SummaryLstwrap.add(memb);
        }
        SummaryLstwrap.sort();
     
       
       
       
        system.debug('aaaaaaaaaa'+monthTodateTotalReservations);
        
        }
        
        public pageReference exportMethod(){
        system.debug('aaaaaaaaaaaaaaaaaaaaa');
         PageReference pr = new PageReference('/apex/WeeklySalesReportpdf?dt='+dt);
         system.debug('bbbbbbbbbbbbbbbbbbbbbb');
       
         pr.setRedirect(true);
         return pr;
        }
        
        
        Public class wrapperSummary{
            public string Summary{get;set;}
            public integer Reservations{get;set;}
            public integer NetReservation{get;set;}
            public integer Cancellation{get;set;}
            Public integer Exchange{get;set;}
            public integer Completion{get;set;}
            
            public wrapperSummary( string Summary, integer Reservations, integer NetReservation, integer Cancellation, integer Exchange, integer Completion){
                this.Summary = Summary;
                this.Reservations = Reservations;
                this.NetReservation = NetReservation;
                this.Cancellation = Cancellation;
                this.Exchange = Exchange;
                this.Completion = Completion;
            }
            
        }
        
        public class MemberWrapper implements Comparable
        {
            public Opportunity member{get;set;}
            
            public MemberWrapper(Opportunity member )
            {
            this.member=member;
            }
            
             public Integer compareTo(Object compareTo) {
            // Cast argument to MemberWrapper
            if(compareTo!=null)
            {
                
              MemberWrapper compareTomember = (MemberWrapper)compareTo;
            
            if(compareTomember.member.Site__c!=member.Site__c)
            {
                String str1= member.Site__c;
                String str2= compareTomember.member.Site__c;
                if(str1>str2)
                return 1;
                if(str2>str1)
                return -1;
            }
            
            
            if(compareTomember.member.Site__c==member.Site__c)
            {
                     String plotMembernum =member.Plot_Number__c ;
            String plotnum ='';
            
        if(member.Plot_Number__c==null)
        return -1;
            for(string s : plotMembernum.split('(?!^)')){
                    if(s.isNumeric()){
                        plotnum = plotnum+s;
                    }
                    else
                    {
                       
                        break;
                    }
                }
            String plotComparetoMembernum=compareTomember.member.Plot_Number__c;
             String plotnumCompareto ='';
            if(plotComparetoMembernum==null)
            return 1;
          for(string s : plotComparetoMembernum.split('(?!^)')){
                    if(s.isNumeric()){
                        plotnumCompareto = plotnumCompareto+s;
                    }
                    else
                    {
                       
                        break;
                    }
                }
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            system.debug(plotnum+'Hormese123'+plotnumCompareto);
            if(plotnumCompareto!='' && plotnum!='' && plotnum.isNUmeric() && plotnumCompareto.isNumeric() )
            {
            if (Integer.valueOf(plotnum) > Integer.valueOf(plotnumCompareto)) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (Integer.valueOf(plotnum) < Integer.valueOf(plotnumCompareto)) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue;       
            }
            else if(plotnum=='')
            {
             return 1;
             
            }
            else
            return 0;
             }
              return 0;
             }
              return 0;
             }
        }
        public class subtotalwrp{
            public integer size{get;set;}
            public integer sqft{get;set;}
            public integer srp{get;set;}
            public integer fap{get;set;}
            public integer rnp{get;set;}
            
            public subtotalwrp(integer size, integer sqft, integer srp, integer fap, integer rnp){
                this.size =size;
                this.sqft = sqft;
                this.srp = srp;
                this.fap = fap;
                this.rnp =rnp;
            }
        }
        

}