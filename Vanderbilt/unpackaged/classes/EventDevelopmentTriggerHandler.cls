/**
 *	Class Name:	EventDevelopmentTriggerHandler 
 *	Description: This is a helper class for EventDevelopmentTrigger on Event
 * 	CreatedDate: 08/12/2016
 */
public with sharing class EventDevelopmentTriggerHandler
{
	 /**
	 *	Method Name:	updateEventFields 
	 *	Description:	This is a method for populating Development__c field on Event from the related Lead
	 *	Param:	list<event> eventList
	 *	Return:	Nil
	 */
	public static void updateEventFields(list<event> eventList)
	{
		list<id> leadIdList = new list<id>();
		for(event e : eventList)
		{
			if(e.whoid != NULL)
				leadIdList.add(e.whoId);
		}
		if(leadIdList!=NULL && leadIdList.size()>0)
		{
			map<id,Lead> leadMap = new map<id,Lead>([Select id,Primary_Development_Interest__c from Lead where id IN :leadIdList LIMIT 999]);
			if(leadMap!=NULL && leadMap.values().size()>0)
			{
				for(event e:eventList)
				{
					if(e.whoid!=NULL)
					{
						if(leadMap.containskey(e.whoId))
							e.Development__c=leadMap.get(e.whoId).Primary_Development_Interest__c;
					}
				}
			}
		}
	}
}