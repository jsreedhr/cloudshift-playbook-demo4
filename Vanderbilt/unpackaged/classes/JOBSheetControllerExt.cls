/**
 *  @Class Name:    JOBSheetControllerExt 
 *  @Description:   This is a controller for JOBSheetPage
 *  @Company: dQuotient
 *  CreatedDate: 27/09/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran George               27/09/2016                  Orginal Version
 *
 */
public with sharing class JOBSheetControllerExt{
    
    private Opportunity objOpportunity;
    public String strCaseid;
    public String jobsheetid;
    public Job_Sheet__c objJobSheet{get;set;}
    public boolean isDisabled {get; set;}
    public List<Job_Sheet__c> jobSheetList{get;set;}
    public String pageMessage{get;set;}
    public boolean isSuccess {get; set;}
    public boolean isError {get; set;}
    public List<Issue__c>lstOfIssues{get;set;}
    public List<Case>lstOfCases{get;set;}
    
    
    /**
     *  Method Name: JOBSheetControllerExt 
     *  Description: This is the constructor for JOBSheetControllerExt class
     *  Param: ApexPages.StandardController
     *  Return: None
    */
    public JOBSheetControllerExt(ApexPages.StandardController stdController){
        
        lstOfIssues=new List<Issue__c>();
        lstOfCases=new List<Case>();
        jobsheetid=Apexpages.currentPage().getParameters().get('id');
        system.debug('jobsheetid+++'+jobsheetid);
        jobsheetid=String.escapeSingleQuotes(jobsheetid);
        if(stdController != null){
            
            objJobSheet = (Job_Sheet__c)stdController.getRecord();
            jobSheetList=new List<Job_Sheet__c>();
            jobSheetList=[select id,Appointment_Date_Time__c,Case__c,Case_Number__c,City__c,Contractor_Name__r.Name,Co_ordinator__c,Customer_Name__c,Date_Time_Opened__c,Development__c,
                            Plot_Property_Name__c, Handyman__c,Handyman__r.name,Home_Phone__c,Mobile_Phone__c,Operative_Notes__c,Plot_Number__c,Postal_Code__c,Special_Instructions__c,State_Province__c,
                            Street__c,Warranty_End_Date__c,Work_Phone__c from Job_Sheet__c where id=:jobsheetid];
            if(jobSheetList!=null &&jobSheetList.size()>0){                
                strCaseid=jobSheetList[0].Case__c;
                system.debug('strCaseid+++'+strCaseid);
            }
        }   
        if(strCaseid!= null){
            if(jobSheetList[0].Handyman__c!=null &&jobSheetList[0].Contractor_Name__c!=null){
                lstOfIssues = [Select id,name,
                                        Case__c,
                                        Category__c,
                                        Contractor__c,
                                        Description__c,
                                        Handyman__c ,
                                        Handyman__r.name,
                                        Job_Number__c,
                                        Status__c
                                        From
                                        Issue__c 
                                        where Case__c=:strCaseId AND Handyman__c=:jobSheetList[0].Handyman__c AND Contractor__c=:jobSheetList[0].Contractor_Name__c AND Status__c!='Completed'  limit 250];
                system.debug('lstOfIssues+++'+lstOfIssues);
            }
            
            if(jobSheetList[0].Handyman__c==null){
                lstOfIssues = [Select id,name,
                                        Case__c,
                                        Category__c,
                                        Contractor__c,
                                        Description__c,
                                        Handyman__c ,
                                        Handyman__r.name,
                                        Job_Number__c,
                                        Status__c
                                        From
                                        Issue__c 
                                        where Case__c=:strCaseId  AND Contractor__c=:jobSheetList[0].Contractor_Name__c AND Status__c!='Completed' limit 250];
                system.debug('lstOfIssues+++'+lstOfIssues);
            }
            
            if(strCaseId!=null && strCaseId!='')
                lstOfCases=[select id,Account.Id,Contact.Id from Case where id=:strCaseId limit 1];
    }
    
    
} 
    public Pagereference attachPdf()
    {
        string sheetid;
         Attachment att = new Attachment();
         att.name='';
         if(jobSheetList[0].Development__c!=null){
             att.name+=jobSheetList[0].Development__c;
              att.name+='-';
         } 
         if(jobSheetList[0].Plot_Number__c!=null){
             att.name+=jobSheetList[0].Plot_Number__c;
             att.name+='-';
         }
         if(jobSheetList[0].Handyman__r.name!=null){
             att.name+=jobSheetList[0].Handyman__r.name;
             att.name+='-';
         }
         if(jobSheetList[0].Contractor_Name__r.Name!=null){
             att.name+=jobSheetList[0].Contractor_Name__r.Name;
             att.name+='-';
         }
             att.name+=system.now().day();
             att.name+='-';
             att.name+=system.now().month();
             att.name+='-';
             att.name+=system.now().year();
             att.name+=' ';
             att.name+=system.now().hour();
             att.name+=':';
             att.name+=system.now().minute();
         PageReference invoicePage = Page.Job_Sheet_pdf;
         invoicePage .getParameters().put('id',jobsheetid);
         system.debug('jobsheetid+++'+jobsheetid);
         // return page content as blob type
         // Alt: att.body = invoicePage .getContentAsPDF();
         if(Test.isRunningTest()){
            att.body=Blob.valueOf('UNIT.TEST');
         }
         else
            att.body = invoicePage .getContent();
         // Associate with project's record Id
         att.parentid = jobsheetid;
         insert att;
         Pagereference pg=new Pagereference('/'+jobsheetid) ;
        return pg;
    }
    public Pagereference GenerateAndSend(){
       string sheetid;
         Attachment att = new Attachment();
         att.name='';
        if(jobSheetList[0].Development__c!=null){
             att.name+=jobSheetList[0].Development__c;
              att.name+='-';
         } 
         if(jobSheetList[0].Plot_Number__c!=null){
             att.name+=jobSheetList[0].Plot_Number__c;
             att.name+='-';
         }
         if(jobSheetList[0].Handyman__r.name!=null){
             att.name+=jobSheetList[0].Handyman__r.name;
             att.name+='-';
         }
         if(jobSheetList[0].Contractor_Name__r.Name!=null){
             att.name+=jobSheetList[0].Contractor_Name__r.Name;
             att.name+='-';
         }
             att.name+=system.now().day();
             att.name+='-';
             att.name+=system.now().month();
             att.name+='-';
             att.name+=system.now().year();
             att.name+=' ';
             att.name+=system.now().hour();
             att.name+=':';
             att.name+=system.now().minute();
             att.name+='.pdf';
         PageReference invoicePage = Page.Job_Sheet_pdf;
         invoicePage .getParameters().put('id',jobsheetid);
         system.debug('jobsheetid+++'+jobsheetid);
         // return page content as blob type
         // Alt: att.body = invoicePage .getContentAsPDF();
         if(Test.isRunningTest()){
            att.body=Blob.valueOf('UNIT.TEST'); 
         }
         else{
            att.body = invoicePage .getContent();
         }
         // Associate with project's record Id
         att.parentid = jobsheetid;
         insert att;
        
         Document objDoc = new Document();
         objDoc.Body =att.body;
         objDoc.FolderId = System.Label.JobSheetFolder;//'00l0Y000000r7kwQAA'
         objDoc.isPublic=true;
         objDoc.Name = att.name;
         try{
            insert objDoc;
         }
         Catch(Exception e){
             
         }
         if(jobSheetList[0].Handyman__c!=null)
         {
          Pagereference pg=new Pagereference('/_ui/core/email/author/EmailAuthor?template_id='+System.Label.JobSheetTemplate+'&p2_lkid='+jobSheetList[0].Handyman__c+' &doc_id='+obJDoc.id+'&p3_lkid='+lstOfCases[0].Id+'&retURL=%2F'+jobSheetList[0].Case__c+'&p26='+System.label.DefaultFrom);   
         return pg;
             
         }
         else {
         Pagereference pg=new Pagereference('/_ui/core/email/author/EmailAuthor?template_id='+System.Label.JobSheetTemplate+'&doc_id='+obJDoc.id+'&p3_lkid='+lstOfCases[0].Id+'&retURL=%2F'+jobSheetList[0].Case__c+'&p26='+System.label.DefaultFrom);
         return pg; 
         }
    }
}