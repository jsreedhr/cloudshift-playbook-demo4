@isTest
/**
 *  @Class Name:    Test_Case_Email_Trigger_Utility 
 *  @Description:   This is a test class for Case_Email_Trigger_Utility
 *  @Company: dQuotient
 *  CreatedDate: 19/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran George              19/10/2016                  Orginal Version
 *  Nitha T S                 01/08/2017                  Last Modified
 */
private class Test_Case_Email_Trigger_Utility {

	/**
     *  Method Name:    EmailCaseTest 
     *  Description:    Method to Test Case_Email_Trigger_Utility
     *  Param:          None
     *  Return:         None
     */
	private static testMethod void EmailCaseTest() {
	    //createBusinessAccount
	    Account objAccount=TestData.createBusinessAccount();
	    insert objAccount;
	     Account objAccount2=TestData.createBusinessAccount2();
	    insert objAccount2;
	     //createContactUnderAccount
	    Contact objContact=TestData.createContact(objAccount);
	    insert objContact;
	   
	    Development__c newDev=TestData.createDevelopment(objContact);
	    insert newDev;
	    
	    //Create Plot
        Plot__c objPlot=TestData.createPlotNoDev(objAccount,objAccount2);
        objPlot.Warranty_End__c=system.today().addDays(2);
        objPlot.Name = '1 Aldridge Place';
        objPlot.House_Type__c = 'House';
        objPlot.Plot_Number__c = '2';
        objPlot.Development__c = newDev.id;
        objPlot.Sales_Status__c = 'Not Released';
        objPlot.Build_Status__c = 'Foundations';
        objPlot.Communal_Plot__c = TRUE;
        objPlot.Number_of_Beds__c = 3;
        objPlot.Open_Market_vs_Affordable__c = 'Affordable';
        objPlot.Size_Sq_Foot__c = 57;
        insert objPlot;
        
        Plot_Specification__c pltspec=TestData.createPlotSpecification(objPlot);
        insert pltspec;
        
        //Create Case
        Case objCase=TestData.createCaseNoPlot(objAccount,objContact);
        insert objCase;
        
        //Check the trigger logic
        //system.assert(objCase.Plot__c!=null);
        system.assert(objPlot!=null);
        //system.assertEquals(objPlot.id,objCase.Plot__c);
        Development_Specification__c newdevspec=TestData.createDevSpecification(newDev);
        newdevspec.Specification__c='https://cs89.salesforce.com/sfc/#version?selectedDocumentId=0690E000000CaS';
        insert newdevspec;
        
        
	}

}