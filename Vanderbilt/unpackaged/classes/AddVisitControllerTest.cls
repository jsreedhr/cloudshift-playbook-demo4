@isTest
public class AddVisitControllerTest
{
    public testmethod static void testMethod1()
    {
        Account objAccount=TestData.createBusinessAccount();
        insert objAccount;
    
        Contact objContact=TestData.createContact(objAccount);
        insert objContact;
        
        Development__c objDev=TestData.createDevelopment(objContact);
        insert objDev;

        Lead newLead = new Lead();
        newLead.salutation='Mr.';
        newLead.lastName='abcd';
        newLead.email='abcd@gmail.com';
        newLead.Primary_Development_Interest__c=objDev.id;
        insert newLead;

        Test.setCurrentPage(new PageReference('Page.mypage'));
        System.currentPageReference().getparameters().put('leadId',newLead.id);
        System.currentPageReference().getparameters().put('option','1');

        AddVisitController con1 = new AddVisitController();
        PageReference p = con1.addVisitController();

        Test.setCurrentPage(new PageReference('Page.mypage'));
        System.currentPageReference().getparameters().put('leadId',newLead.id);
        System.currentPageReference().getparameters().put('option','2');

        AddVisitController con2 = new AddVisitController();
        PageReference p1 = con1.addVisitController();
        
    }
    
}