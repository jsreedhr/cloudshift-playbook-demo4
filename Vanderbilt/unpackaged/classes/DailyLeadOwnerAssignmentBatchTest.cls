@isTest
private class DailyLeadOwnerAssignmentBatchTest {
  public static testMethod void TestLeadOwnerAssignmentBatch() {
  UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;

        insert u1;
  
   
}
    @future
    public static void testfunc(Id userid){
    
    Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
    dev.Sales_Consultant__c = userid;
    dev.Sales_Manager__c = userid; 
    dev.Active__c = TRUE;
    dev.Enable_Auto_Created_Inspections__c = TRUE;       
     
    insert dev;

    Lead l = new Lead();    
    l.First_Name__c = 'Test';
    l.Last_Name__c = 'Lead Name';
    l.Email = 'testlead123@gmail.com';
    l.Primary_Development_Interest__c = dev.id;
    l.OwnerId = userid;
    l.IsConverted = false;
    l.LeadSource = 'Campaign Upload';
    l.status = 'New Enquiry';
    insert l;
        
    Test.startTest();
          ScheduleLeadOwnerAssignmentBatch obj = new  ScheduleLeadOwnerAssignmentBatch();
          String cron = '0 0 23 * * ?';
          System.schedule('Test Schedule', cron, obj);    
    Test.stopTest();
  }
}