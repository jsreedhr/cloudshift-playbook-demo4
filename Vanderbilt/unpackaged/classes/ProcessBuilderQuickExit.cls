/**
 * Created by Nidheesh on 23/05/2018.
 */
 
public with sharing class ProcessBuilderQuickExit {
    @InvocableMethod(label='ProcessBuilder Quick Exit' description='We will use this class to quickly exit from PB without any action')
    public static void ProcessBuilderQuickExit(){}
}