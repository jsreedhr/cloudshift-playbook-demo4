/**
*  Name: DeveloperTriggerFunctions
*  Copyright © 2017 Salesforce
*  Purpose: This is a helper class, as part of the trigger framework, for the Development__c object.
*  In this class we will develop all the methods that are used by the triggers
*
*  History:
*  Version         Author              Date                Detail
*  1.0             David Kurtanjek     13/11/2017          added the method CreateConsultants
*  1.1             Jeevraj Rao         14/11/2017          added the method createTechnicalApprovals
*/

public without sharing class DeveloperTriggerFunctions {
    
    public static void CreateConsultants(List <Development__c> lst_developments){
        List <String> lst_devsitestoquery = new List <String>();
        List <Development__c> lst_devstoprocess = new List <Development__c>();
        Map <String, ID> map_DevName_SiteID = new Map <String, ID>();
        Map <String, Date> map_DevName_SiteTargetExchange = new Map <String, Date>();
        Map <String, Id> map_DevName_DevId = new Map <String, Id>();
        List <Consultant__c> lst_constocreate = new List <Consultant__c>();
        List <Consultant__c> lst_addconstocreate = new List <Consultant__c>();
        List <Consultant__c> lst_constoupdate = new List <Consultant__c>();
        Set <String> set_contypestoquery = new Set <String>();
        List <String> pickListValuesList = new List <String>();
        List <String> nonmatchingpickListValuesList = new List <String>();
        List <String> matchingpickListValuesList = new List <String>();
        
        //Build a list of the types we need to consider
        Schema.DescribeFieldResult F = Consultant__c.Type__c.getDescribe();    
        List<Schema.PicklistEntry> lst_types = F.getPicklistValues();
        for(Schema.PicklistEntry pickListVal : lst_types){
            pickListValuesList.add(pickListVal.getLabel());
        }
        
        Integer j = 0;
        while (j < pickListValuesList.size())
        {
            if(pickListValuesList.get(j) == 'Other')
            {
                pickListValuesList.remove(j);
            }else
            {
                j++;
            }
        }
        
        //Loop over the developments and check that it was created by the process
        for(Development__c d: lst_developments){
            System.debug('First loop');
            IF(d.Created_by_Process__c == TRUE){
                lst_devsitestoquery.add(d.Name);
                lst_devstoprocess.add(d);
                map_DevName_DevId.put(d.Name, d.id);               
            }
        }
        
        IF(!lst_devsitestoquery.isEmpty()){
            //Query the list of sites
            List <Site__c> lst_sites = new List <Site__c>([SELECT ID, Name, Target_Exchange_Date__c FROM Site__c WHERE Name in: lst_devsitestoquery]);
            System.debug('lst_sites ---> '+lst_sites);
            
            IF(!lst_sites.isEmpty()){
                System.debug('Sites query');                
                for(String d: lst_devsitestoquery){
                    System.debug('Map loop');
                    for(Site__c s: lst_sites){
                        IF(d == s.Name){
                            map_DevName_SiteID.put(d, s.id);
                            map_DevName_SiteTargetExchange.put(d, s.Target_Exchange_Date__c);
                        }                            
                    }
                }
                
                //Query to see if any existing consultants
                List <Consultant__c> lst_cons = new List <Consultant__c>([SELECT ID, Type__c, Site__c FROM Consultant__c WHERE Site__c in: map_DevName_SiteID.values()]);             
                
                //A version for if there are no consultants
                IF(lst_cons.isEmpty()){
                    System.debug('Consultants query');
                    for(Development__c d: lst_devstoprocess){
                        for(String pl: pickListValuesList){
                            Consultant__c c = new Consultant__c();
                            c.Type__c = pl;
                            c.Development__c = d.id;
                            c.Target_Exchange_Date__c = map_DevName_SiteTargetExchange.get(d.Name);
                            IF(map_DevName_SiteID.get(d.Name) != Null){
                                c.Site__c = map_DevName_SiteID.get(d.Name);
                            }
                            lst_constocreate.add(c);
                        }
                    }
                    System.debug('Insertion DML');
                    insert lst_constocreate;
                }
                
                //A version if there are consultants
                IF(!lst_cons.isEmpty()){
                    
                    for(Consultant__c c: lst_cons){
                        set_contypestoquery.add(c.Type__c);
                    }
                    
                    for(String pl: pickListValuesList){
                        if(set_contypestoquery!=null && set_contypestoquery.contains(pl))
                        {
                            matchingpickListValuesList.add(pl);   
                        }
                        else
                        {
                            nonmatchingpickListValuesList.add(pl);
                        }
                    }                    
                    
                    System.debug('Consultants query');
                    for(Development__c d: lst_devstoprocess){
                        for(String pl: nonmatchingpickListValuesList){
                            
                            Consultant__c c = new Consultant__c();
                            c.Type__c = pl;
                            c.Development__c = d.id;
                            c.Target_Exchange_Date__c = map_DevName_SiteTargetExchange.get(d.Name);
                            IF(map_DevName_SiteID.get(d.Name) != Null){
                                c.Site__c = map_DevName_SiteID.get(d.Name);
                            }
                            lst_addconstocreate.add(c);
                        }
                    }
                    
                    // for(String s: matchingpickListValuesList){
                    for(Consultant__c cn: lst_cons){
                        for(Development__c d: lst_devstoprocess){
                            IF(map_DevName_SiteID.get(d.Name) != Null){
                                cn.Development__c = map_DevName_DevId.get(d.Name);
                                cn.Target_Exchange_Date__c = map_DevName_SiteTargetExchange.get(d.Name);
                                lst_constoupdate.add(cn);
                            }
                        }
                    }
                    // }
                    insert lst_addconstocreate;
                    update lst_constoupdate;
                }    
            }
        }    
    }
    
    /**
* @desc     Create Technical Approval Record if checkbox is set as true in DevelopmentObject.
* @author   Jeevraj Rao (jeevraj@cloudshiftgroup.com)
* @since    14/11/2017
* @param newDevelopmentList - new Development list from the trigger contex.
* @param oldDevelopmentMap - old Development map from the trigger contex.
*/
    
    public static void createTechnicalApprovals(List<Development__c> newDevelopmentList, Map<Id, Development__c> oldDevelopmentMap) {
        System.debug('newDevelopmentList--->' +newDevelopmentList);
        Boolean isUpdate = Trigger.isUpdate;
        //Declaring Variables
        List<Technical_Approval__c> newTechnicalApprovalList = new List<Technical_Approval__c>();
        Map<Id,String> mapComponent = new Map<id,String>();
        Map<Id,List<String>> mapSplittedString = new Map<Id,List<String>>();
        Map<Id,Map<String, String>> mapDevelopmentPlotNumberHouseType = new Map<Id,Map<String, String>>();
        
        for(Development__c d: newDevelopmentList){
            if (!mapSplittedString.containsKey(d.id) && String.isNotBlank(d.List_of_Plot_House_Type_s__c)){
                mapSplittedString.put(d.id, d.List_of_Plot_House_Type_s__c.split(', ')); //Map of Development and House Types
            }
            System.Debug('mapSplittedString ---> ' +mapSplittedString);

            
            if (!mapDevelopmentPlotNumberHouseType.containsKey(d.id)){
                List<String> listPlotNumberHouseType = new List<String>();
                if(String.isNotBlank(d.Plot_Number_with_House_Type__c)){
                    listPlotNumberHouseType = d.Plot_Number_with_House_Type__c.split(',');
                }
                System.Debug('listPlotNumberHouseType ---> ' +listPlotNumberHouseType);
                Map<String, String> mapPlotNumberHouseType = new Map<String, String>(); //Map of House Type and Number
                if(!listPlotNumberHouseType.isEmpty() && listPlotNumberHouseType != NULL){
                    for(String str : listPlotNumberHouseType){
                        if(mapPlotNumberHouseType.containsKey(str.substringBefore('-').trim())){
                            String s = mapPlotNumberHouseType.get(str.substringBefore('-').trim())+', '+str.substringAfter('-').trim();
                            mapPlotNumberHouseType.put(str.substringBefore('-').trim(), s);
                        }else{
                            mapPlotNumberHouseType.put(str.substringBefore('-').trim(), str.substringAfter('-').trim());
                        }    
                    }
                    System.Debug('mapPlotNumberHouseType ---> ' +mapPlotNumberHouseType);
                    mapDevelopmentPlotNumberHouseType.put(d.id, mapPlotNumberHouseType); //Map of Development and map of House Type and Number
                }    
            }
        }
        
        for(Integer T = 0; T < MetadataMagic.ComponentList.size(); T++) { //Query metadata values and make a map
            Component__mdt component = MetadataMagic.ComponentList[T];
            
            if (!mapComponent.containsKey(component.id)){
                mapComponent.put(component.id, component.MasterLabel);
            }
        }
        
        for(Development__c d: newDevelopmentList) {
            if(!mapSplittedString.isEmpty() && mapSplittedString.containsKey(d.id)) {
                for(String s : mapSplittedString.get(d.id)){
                    for(Component__mdt component : MetadataMagic.ComponentList) {
                        if (isUpdate && d.Create_Technical_Approvals__c == true && oldDevelopmentMap.get(d.id).Create_Technical_Approvals__c == false) {
                            //Create new Technical Approval Records.
                            Technical_Approval__c techApproval = new Technical_Approval__c();
                            techApproval.Name = d.Name+' - '+s+' - '+mapComponent.get(component.id);
                            if(techApproval.Name.length()>80)
                            techApproval.Name=techApproval.Name.substring(0,79);
                            techApproval.Development__c = d.id;
                            techApproval.House_Type__c = s;
                            techApproval.Component__c = mapComponent.get(component.id);
                            if(mapDevelopmentPlotNumberHouseType.get(d.id).containsKey(s)){
                                techApproval.Plot_Numbers__c = mapDevelopmentPlotNumberHouseType.get(d.id).get(s);
                            }
                            newTechnicalApprovalList.add(techApproval);
                        }
                    }
                }
            }
        }
        
        System.Debug('newTechnicalApprovalList --->' +newTechnicalApprovalList);
        
        if(!newTechnicalApprovalList.isEmpty() && newTechnicalApprovalList.size()>0) {
            try {
                upsert newTechnicalApprovalList;
            }
            catch (Exception ex){
                String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
                CustomLogging.logMessage('DeveloperTriggerFunctions', 'createTechnicalApprovals', message, CustomLogging.INFO);
            }
        }
    }
    
}