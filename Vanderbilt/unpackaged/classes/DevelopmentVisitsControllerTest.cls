@istest
public class DevelopmentVisitsControllerTest {

        public testmethod static void method1(){
        
        Account objAccount=TestData.createBusinessAccount();
        insert objAccount;
    
        Contact objContact=TestData.createContact(objAccount);
            insert objContact;
        Development__c objDev=TestData.createDevelopment(objContact);
            insert objDev;
    
        DevelopmentVisitsController  dvc = new DevelopmentVisitsController();
        dvc.getRecords();
        
        dvc.exportMethod();
    
    
    }
}