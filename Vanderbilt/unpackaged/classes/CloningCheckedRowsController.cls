public class CloningCheckedRowsController {
    public List<WrapperClass> listWrapper {get;set;}
    public Boolean allBool {get;set;}
	public string Plotid{get;set;}
    public Plot__c objPlot;
    public CloningCheckedRowsController() {
		Plotid=ApexPages.currentPage().getParameters().get('id');
		objPlot=new Plot__c();
		if(String.isNotBlank(Plotid)){
			objPlot=[select id, Development__c,Development__r.name,name from Plot__c where id=:PlotId limit 1];
		}
        listWrapper = new List<WrapperClass>();
        
        
            for(Development_Specification__c dc: [select id, Development__c, Name,File_Name__c, Specification__c,CreatedBy.Name,CreatedDate from Development_Specification__c where Development__c =:objPlot.Development__c order by File_Name__c,CreatedDate limit 10000]) {
                listWrapper.add(new WrapperClass(dc));
            }
        
    }
    
    public class WrapperClass {
        public Development_Specification__c dcs{get;set;}
        public string filename {get; set;}
		public string Specification {get; set;}
		public string developid {get; set;}
		public string devspecid{get;set;}
		public Boolean checked {get; set;}
		public string createdby{get;set;}
		public Datetime CreatedDate{get;set;}
		public WrapperClass(Development_Specification__c dcs) 
		{
			filename = dcs.File_Name__c;
			Specification=dcs.Specification__c;
			developid=dcs.Development__c;
			devspecid=dcs.Name;
			checked = false;
			createdby=dcs.CreatedBy.Name;
			CreatedDate=dcs.CreatedDate;
		}
    }
    
    public Pagereference process() {
        List<Development_Specification__c> listDevSpecForProcess = new List<Development_Specification__c>();
        List<WrapperClass> listTempWrapper = new List<WrapperClass>();
        for(WrapperClass w : listWrapper) {
            if(w.checked) {
                listDevSpecForProcess.add(w.dcs);
            } else {
                listTempWrapper.add(w);
            }
        }
        if(listDevSpecForProcess.size() > 0) {
            insert listDevSpecForProcess;
            return new PageReference('/'+PlotId);
            listWrapper = listTempWrapper;
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Select atleast one employee to Clone'));
            return null;
        }
        return null;
    }
    
    public void selectAll() {
        if(allBool) {
            for(WrapperClass w : listWrapper) {
                w.checked = true;
            }
        } else {
            for(WrapperClass w : listWrapper) {
                w.checked = false;
            }
        }
    }
}