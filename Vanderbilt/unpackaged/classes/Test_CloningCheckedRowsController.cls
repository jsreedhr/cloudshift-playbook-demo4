@isTest
private class Test_CloningCheckedRowsController {
    
static testMethod void SalesReport(){
    
    
    UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;
     insert u1;
System.RunAs(u1)
     {
        Test.startTest();
         testfunc(u1.id);
         Test.stopTest();
     }
}
    @future
    public static void testfunc(Id userid)
    {
    Boolean allBool = True;
        
    Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
         dev.Sales_Manager__c = userid; 
    dev.Active__c = TRUE;
       
     
    insert dev;
     
    Plot__c lst_plots = new Plot__c ();
    lst_plots.Name = '1 Aldridge Place';
    lst_plots.Number_of_Beds__c = 2;
    lst_plots.House_Type__c = 'House';
    lst_plots.Planning_Sqft__c = 200;
    lst_plots.Open_Market_vs_Affordable__c = 'Open Market';
    lst_plots.Plot_Number__c = '2';
    lst_plots.Development__c = dev.id;
    lst_plots.Sales_Status__c = 'Not Released';
    lst_plots.Build_Status__c = 'Foundations';
    lst_plots.Communal_Plot__c = TRUE;
    lst_plots.Customer_Demo_Date__c = System.today();
    
    Insert lst_plots;
        
   Development_Specification__c dsc = new Development_Specification__c();
     dsc.Development__c = dev.id;
     dsc.File_Name__c = 'abc';
    dsc.Specification__c = dev.id;
    dsc.CreatedById = userid;
    dsc.CreatedDate = system.today();
    
    
         insert dsc;
  
        
        PageReference Pageref = page.newTestClone;
     test.setCurrentPage(Pageref);
    ApexPages.currentPage().getParameters().put('id',lst_plots.id);
        CloningCheckedRowsController Report = new CloningCheckedRowsController();
        
    
         Report.process();
        Report.allBool = True;
        Report.selectAll();
   
      
        
        
       CloningCheckedRowsController.WrapperClass Wrapper = new CloningCheckedRowsController.WrapperClass(dsc);
    
           
           }
}