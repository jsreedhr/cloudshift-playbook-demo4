public without sharing class LeadTriggerFunctions {
    
    public static void SetPrimaryDevLookupFromWebtoLead(List <Lead> lst_leads_to_process){
        
        //Method designed to auto-populate the primary development based on the site passed in via web to lead
        
        List <Lead> lst_leadstocreate = new List <Lead>();        
        for(Lead l: lst_leads_to_process){
            IF(l.Primary_Development_of_Interest_W2L__c != Null && l.Primary_Development_Interest__c == Null){
                lst_leadstocreate.add(l);
            }
        }
        
        IF(!lst_leads_to_process.isEmpty()){
            List <Development__c> lst_devs = new List <Development__c>([SELECT ID, Name, Marketing_Name__c FROM Development__c WHERE Marketing_Name__c !=Null LIMIT 9999]);
            
            Map <String, Id> map_SiteNameId = new Map <String, Id>(); 
            
            IF(!lst_devs.isEmpty()){
                for(Development__c d: lst_devs){
                    map_SiteNameId.put(d.Marketing_Name__c, d.id);
                }
                for(Lead l: lst_leadstocreate){
             	l.Primary_Development_Interest__c = map_SiteNameId.get(l.Primary_Development_of_Interest_W2L__c);                       
                }
            }
        }
    }
    
    public static void AssociateWebLeadtoCampaign(List <Lead> lst_leads_to_process){
        
        //Method designed to link a lead to campaign where possible based on the values in the campaigns.
        
        List <Lead> lst_leadstocreatecampaignlinks = new List <Lead>();        
        for(Lead l: lst_leads_to_process){
            IF(l.Primary_Development_of_Interest_W2L__c != Null && l.LeadSource == 'Website - Vanderbilt'){
                lst_leadstocreatecampaignlinks.add(l);
            }
        }
                
        IF(!lst_leadstocreatecampaignlinks.isEmpty()){
            List <Campaign> lst_camps = new List <Campaign>([SELECT ID, Name, Type, isActive, Development_Marketing_Name__c FROM Campaign WHERE Type = 'Website Direct' AND isActive = TRUE LIMIT 9999]);
            
            Map <String, Id> map_DevNameId = new Map <String, Id>();
            List <CampaignMember> lst_cms = new List <CampaignMember>();
            
            IF(!lst_camps.isEmpty()){
                for(Campaign c: lst_camps){
                    map_DevNameId.put(c.Development_Marketing_Name__c, c.id);
                }
                for(Lead l: lst_leadstocreatecampaignlinks){
                CampaignMember cm = new CampaignMember();
                cm.CampaignId = map_DevNameId.get(l.Primary_Development_of_Interest_W2L__c); 
             	cm.LeadId = l.id;
                	IF(map_DevNameId.get(l.Primary_Development_of_Interest_W2L__c) != null){
                	lst_cms.add(cm);
               	 	}
                }
                IF(!lst_cms.isEmpty()){
                    insert lst_cms;
                }
            }
        }
    }
           
    public static void ConvertLeadEnhancements(List <Lead> lst_leads){
        
        Date Defaultoppclose = Date.newInstance(3000, 01, 01);
        
        //Dynamically query the record type for Person Account
        RecordType rt = [select Id from RecordType where Name = 'Person Account' limit 1];
        
        List<Opportunity> oUpdList = new List<Opportunity>();
        
        for (Lead l : lst_leads){
            system.debug('******************************'+l.isconverted); 
            
            system.debug('qqqqqqqqqqqqqqqqqqqqqq'+l.isconverted);
            system.debug('rrrrrrrrrrrrrrrrrrrrrr'+l.convertedOpportunityId);
            system.debug('ssssssssssssssssssssss'+l.Secondary_Contact__c);
            system.debug('tttttttttttttttttttttt'+l.Last_Name__c );
            
            
            if (l.IsConverted && l.convertedOpportunityId != null && l.Secondary_Contact__c == null && l.Last_Name__c != null){
                //Do not need to bulkify as lead conversion is only possible in single context session
                Account personaccount = new Account();
                personaccount.Salutation = l.Salutation__c;
                personaccount.FirstName = l.First_Name__c;
                personaccount.LastName = l.Last_Name__c;
                personaccount.PersonEmail = l.Secondary_Email__c;
                personaccount.RecordTypeid = rt.id;
                personaccount.LastName = l.Last_Name__c;
                personaccount.Type = 'B2C Customer';
                personaccount.Primary_Phone__c = l.Secondary_Primary_Phone__c;
                personaccount.Phone = l.Secondary_Work_Phone__c;
                personaccount.PersonMobilePhone = l.Secondary_Mobile__c;
                personaccount.Age__pc = l.Secondary_Age__c;
                //Assumes the following attributes are the same as the primary contact
                personaccount.Segment_Contact__pc = l.Segment__c;
                personaccount.PersonMailingStreet = l.Street;
                personaccount.PersonMailingCity = l.City;
                personaccount.PersonMailingState = l.State;
                personaccount.PersonMailingPostalCode = l.PostalCode;
                personaccount.PersonMailingCountry = l.Country;
                personaccount.Other_Development_Interest__pc = l.Other_Development_Interest__c;
                personaccount.County_Borough__pc = l.County_or_Borough__c;
                personaccount.Region__pc = l.Region__c;
                personaccount.Opt_in_Status__pc	= l.Opt_in_Status_Secondary__c;
                personaccount.Opt_in_source__pc = l.Opt_in_source_secondary__c;
                personaccount.Opt_in_date_time__pc= l.Opt_in_date_time_Secondary__c;
                personaccount.Opt_in_expiry_date__pc= l.Opt_in_expiry_date_secondary__c;
                personaccount.Email_Marketing__pc = l.Email_Marketing_Secondary__c;
                personaccount.Phone_Marketing__pc = l.Phone_Marketing_Secondary__c;
                personaccount.Postal_Marketing__pc = l.Postal_Marketing_Secondary__c;
                
                
                insert (personaccount);
                system.debug('qqqqqqqqqqqqqqqqqqqqqq'+l.isconverted);
                system.debug('rrrrrrrrrrrrrrrrrrrrrr'+l.convertedOpportunityId);
                system.debug('ssssssssssssssssssssss'+l.Secondary_Contact__c);
                system.debug('tttttttttttttttttttttt'+l.Last_Name__c );
                
                
                system.debug('uuuuuuuuuuuuuuuuuuuuuuu'+ personAccount);
                
                List <Contact> lst_personcontacts = new List <Contact>([SELECT ID FROM Contact WHERE AccountId =: personaccount.id LIMIT 1]);
                
                IF(lst_personcontacts.size()==1){
                    String secondarycontactid = [SELECT ID FROM Contact WHERE AccountId =: personaccount.Id LIMIT 1].ID;
                    
                    oUpdList.add(new Opportunity(id = l.convertedOpportunityId, Primary_Purchaser__c= l.convertedContactId, Secondary_Purchaser__c=secondarycontactid, CloseDate = Defaultoppclose));
                }
            }
            
            else if (l.IsConverted && l.convertedOpportunityId != null && l.Secondary_Contact__c != null){
                oUpdList.add(new Opportunity(id = l.convertedOpportunityId, Primary_Purchaser__c= l.convertedContactId, Secondary_Purchaser__c= l.Secondary_Contact__c, CloseDate = Defaultoppclose));
            }
            
            else if (l.IsConverted && l.convertedOpportunityId != null && l.Secondary_Contact__c == null && l.Last_Name__c == null){
                oUpdList.add(new Opportunity(id = l.convertedOpportunityId, Primary_Purchaser__c= l.convertedContactId, CloseDate = Defaultoppclose));
            }
            
        }
        update oUpdList;  
    }
}