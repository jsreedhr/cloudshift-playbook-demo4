public with sharing class PlotCoinsTriggerHandler
{

    public static void coinsCallout(list<Plot__c> plots,map<id,Plot__c> oldPlotsMap)
    {
        list<id> plotsWithCallout = new list<id>();
        for(Plot__c plot : plots)
        {
            if(plot.Sales_Status__c=='Completed' && plot.Sales_Status__c!=oldPlotsMap.get(plot.id).Sales_Status__c && plot.Coins_Response_Recieved_At__c==NULL)
                plotsWithCallout.add(plot.id);
        }
        if(plotsWithCallout.size()>0)
        {
            for(id pId : plotsWithCallout)
                CoinsWsdl.generateXML(pId);
        }
    }
}