global class ScheduleLeadOwnerAssignmentBatch implements Schedulable {

   global void execute(SchedulableContext ctx) {
      DailyLeadOwnerAssignmentBatch p = new DailyLeadOwnerAssignmentBatch();
        database.executeBatch(p);
   }   
}