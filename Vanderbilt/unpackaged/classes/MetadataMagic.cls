/**
* @desc     This Class used for reterving metadata values.
* @author   Jeevraj Rao (jeevraj@cloudshiftgroup.com)
* @since    14/11/2017
*/
	 
public with sharing class MetadataMagic {

    /**
    * @desc     This Method gets Component Metadata's Values.
    * @author   Jeevraj Rao (jeevraj@cloudshiftgroup.com)
    * @since    14/11/2017
    */

    @testVisible public static List<Component__mdt> ComponentList {
		get {
			if (null == ComponentList) {
				ComponentList = [SELECT Id, DeveloperName,MasterLabel, NamespacePrefix FROM Component__mdt];
			}
			return ComponentList;
		}
		set;
	}
}