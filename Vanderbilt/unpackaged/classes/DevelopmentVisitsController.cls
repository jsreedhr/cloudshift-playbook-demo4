/**
 *  Class Name: DevelopmentVisitsController 
 *  Description: This is a controller for DevelopmentVisitsPage
 *  CreatedDate: 09/12/2016
 */
public with sharing class DevelopmentVisitsController
{
    public list<DevelopmentWrapper> wrapperList{get;set;}
    public string dtString {get;set;}
    public list<Date> weekStartList{get;set;}
    public list<Integer> weekNumberList{get;set;}
    public list<Integer> totalList{get;set;}
    public Integer TResersvations{get;set;}
    public Integer TUnits{get;set;}
    public Integer TVisits{get;set;}
    public Integer TRevisits{get;set;}
    
    public DevelopmentVisitsController()
    {
        
        String param = ApexPages.currentpage().getParameters().get('date');
        if(param!=NULL)
        {
            dtstring=param;
        }
        else
        {
        //Getting previous Sunday
        Date dateObj = (System.today().toStartofWeek());
        //Getting Sunday instead of Monday
        dateObj = dateObj.addDays(-1);
        dtString = dateObj.format(); 
        List<string> dtList = dtstring.split('/');
        dtstring = dtList[1]+'/'+dtList[0]+'/'+dtList[2];
        }

        //Retrieving Records
        getRecords();

    }
    public void getRecords()
    {
        List<string> dtstr = dtstring.split('/');
        Date dateObj = Date.newInstance(Integer.valueof(dtstr[2]),Integer.valueof(dtstr[0]),Integer.valueof(dtstr[1]));
        
        //To start 4 weeks earlier from the selected date
        dateObj=dateObj.addDays(-28);
        //Getting first 5 sundays
        weekStartList = new list<Date>();
        weekNumberList = new list<Integer>();
        for(Integer i=0;i<5;i++)
        {
            weekStartList.add(dateObj);
            Datetime dtFormat = dateObj;
            weekNumberList.add(Integer.valueOf(dtFormat.format('w')));
            dateObj = dateObj.addDays(7);
        }

        //Initialising Wrapper
        wrapperList = new list<DevelopmentWrapper>();
        totalList = new list<Integer>();
        //Retrieving all Developments
        list<Development__c> developmentsRetrieved = new list<Development__c>();
        developmentsRetrieved = [Select id,name,Release_Date__c,Marketing_Name__c from Development__c order by Marketing_Name__c LIMIT 999];

        if(developmentsRetrieved!=NULL && developmentsRetrieved.size()>0)
        {
            //Retrieving All Plots & Units Available Plots related to Development
            list<AggregateResult> totalPlotsResult = new list<AggregateResult>();
            list<AggregateResult> plotsAvailableResult = new list<AggregateResult>();
            list<AggregateResult> plotsDisplay = new list<AggregateResult>();
            totalPlotsResult = [Select count(id) countPlots,Development__c dev FROM Plot__c WHERE Development__c IN:developmentsRetrieved AND Sales_Status__c!='Available to Reserve' AND Sales_Status__c!='Not Released' AND Communal_Plot__c=False and Open_Market_vs_Affordable__c!='Affordable'and Development__c!= 'a0r0Y00000dNS2B' GROUP BY Development__c];
            plotsAvailableResult = [Select count(id) countPlots,Development__c dev,Sales_Status__c status from Plot__c WHERE Development__c IN:developmentsRetrieved AND (Sales_Status__c='Available to Reserve' OR Sales_Status__c='Not Released') AND Communal_Plot__c=False and Open_Market_vs_Affordable__c!='Affordable' and Development__c!= 'a0r0Y00000dNS2B' GROUP BY Development__c,Sales_Status__c ];
            plotsDisplay = [Select count(id) countPlots,Development__c dev from Plot__c WHERE Development__c IN:developmentsRetrieved AND (Sales_Status__c='Reserved' OR Sales_Status__c='Available to Reserve') AND Communal_Plot__c=False and Open_Market_vs_Affordable__c!='Affordable'and Development__c!= 'a0r0Y00000dNS2B' GROUP BY Development__c];
            
            //Mapping NUmber of plots to Development Id
            map<Id,Integer> unitsAvailableMap = new map<id,Integer>();
            map<id,Integer> allUnitsMap = new map<id,Integer>();
            map<id,Integer> plotsDisplayMap = new map<id,Integer>();
            for(AggregateResult a : totalPlotsResult)
            {
                allUnitsMap.put((Id)a.get('dev'),(Integer)a.get('countPlots'));
            }
            for(AggregateResult a : plotsAvailableResult)
            {
                if(unitsAvailableMap.containsKey((Id)a.get('dev')))
                {
                    Integer count = unitsAvailableMap.get((Id)a.get('dev'));
                    count = count + (Integer)a.get('countPlots');
                    unitsAvailableMap.put((Id)a.get('dev'),count);
                }
                else
                    unitsAvailableMap.put((Id)a.get('dev'),(Integer)a.get('countPlots'));
            }
            for(AggregateResult a : plotsDisplay)
            {
                plotsDisplayMap.put((Id)a.get('dev'),(Integer)a.get('countPlots'));
            }
            System.debug('mapdata'+unitsAvailableMap);
            System.debug('mapdata'+allUnitsMap);

            
            //Retrieving Visits and Re-Visists
            list<Event> visitsList = [Select id,Development__c,StartDateTime,Subject from Event where Development__c IN:developmentsRetrieved AND Subject=:label.visit ];
            list<Event> reVisitsList = [Select id,Development__c,StartDateTime,Subject from Event where Development__c IN:developmentsRetrieved AND Subject=:label.revisit ];
            list<map<id,Integer>> visitsMapList = new list<map<id,Integer>>();
            list<map<id,Integer>> reVisitsMapList = new list<map<id,Integer>>();
            for(Date dm : weekStartList)
            {
                Date d = dm.addDays(-6);
                map<id,Integer> visitInteger = new map<id,Integer>();
                for(Event v : visitsList)
                {
                    if(v.StartDateTime.date()>= d && v.StartDateTime.date()<d.addDays(7))
                    {
                        if(v.Development__c!=NULL)
                        {   
                            if(visitInteger.containskey(v.Development__c))
                            {
                                Integer count=visitInteger.get(v.Development__c);
                                count=count+1;
                                visitInteger.put(v.Development__c,count);
                            }
                            else
                                visitInteger.put(v.Development__c,1);
                        }
                    }
                }
                visitsMapList.add(visitInteger);
                
                map<id,Integer> revisitInteger = new map<id,Integer>();
                for(Event v : reVisitsList)
                {
                    Date d2=d.addDays(7);
                    if(v.StartDateTime.date()>= d && v.StartDateTime.date()<d2)
                    {
                        if(revisitInteger.containskey(v.Development__c))
                        {
                            Integer count=revisitInteger.get(v.Development__c);
                            count=count+1;
                            revisitInteger.put(v.Development__c,count);
                        }
                        else
                            revisitInteger.put(v.Development__c,1);
                    }
                }
                reVisitsMapList.add(revisitInteger);
            }
            //Creating a record for each Development in wrapperList
            for(Development__c d : developmentsRetrieved)
            {
                //
                Development__c devObj = d;
                Integer unitsAvailable=0;
                Integer totalReservations=0;
                Integer plotsDisplayNum=0;
                if(unitsAvailableMap.containskey(d.id))
                    unitsAvailable=unitsAvailableMap.get(d.id);
                if(allUnitsMap.containskey(d.id))
                    totalReservations= allUnitsMap.get(d.id);
                if(plotsDisplayMap.containskey(d.id))
                    plotsDisplayNum=plotsDisplayMap.get(d.id);
                Integer totalVisits=0;
                Integer totalRevisits=0;
                list<Integer> visits = new list<Integer>();
                list<Integer> reVisits = new list<Integer>();
                Integer i=0;
                for(Date dtOBJ : weekStartList)
                {
                    Integer visitsNum = 0;
                    Integer revisitsNum = 0;
                    if(visitsMapList.get(i).containskey(d.id))
                        visitsNum=visitsMapList.get(i).get(d.id);
                    if(reVisitsMapList.get(i).containskey(d.id))
                        revisitsNum=reVisitsMapList.get(i).get(d.id);
                    totalVisits+=visitsNum;
                    totalRevisits+=revisitsNum;
                    visits.add(visitsNum);
                    reVisits.add(revisitsNum);
                    i=i+1;
                }
                DevelopmentWrapper wrapperObj = new DevelopmentWrapper(d,totalReservations,unitsAvailable,totalVisits,totalRevisits,visits,reVisits);
                //Logic to add Development to Display List
                if(plotsDisplayNum>0)
                    wrapperList.add(wrapperObj);
            }
            //Calculating Values for Total Viistors Row
            for(Integer i=0;i<5;i++)
            {
                Integer V=0;
                Integer R=0;
                Integer TR=0;
                Integer TU=0;
                Integer TV=0;
                Integer TRV=0;
                for(DevelopmentWrapper w : wrapperList)
                {
                    V+=w.visits.get(i);
                    R+=w.revisits.get(i);
                    TR+=w.totalReservations;
                    TU+=w.unitsAvailable;
                    TV+=w.totalVisits;
                    TRV+=w.totalRevisits;

                }
                TResersvations=TR;
                TUnits=TU;
                TVisits=TV;
                TRevisits=TRV;
                totalList.add(V);
                totalList.add(R);
            }
                System.debug(wrapperList);
        }
    }
    //Function to redirect to Pdf Page
    public PageReference exportMethod()
    {
        PageReference pdf = new PageReference('/apex/WeeklyVisitorReportPdf?date='+dtstring);
        pdf.setRedirect(true);
        return pdf;
    }
/**
 *  Class Name: DevelopmentWrapper 
 *  Description: A Wrapper class for storing details about a Development
 *  CreatedDate: 09/12/2016
 */
    public with sharing class DevelopmentWrapper
    {
        public Development__c development{get;set;}
        public Integer totalReservations{get;set;}
        public Integer unitsAvailable{get;set;}
        public Integer totalVisits{get;set;}
        public Integer totalRevisits{get;set;}
        public list<Integer> visits{get;set;}
        public list<Integer> reVisits{get;set;}
        public list<list<Integer>> visitsRevisits{get;set;}
        public DevelopmentWrapper(Development__c development,Integer totalReservations,Integer unitsAvailable,
                                  Integer totalVisits,Integer totalRevisits,list<Integer> visits,list<Integer> reVisits)
        {
            this.development = development;
            this.totalReservations = totalReservations;
            this.unitsAvailable = unitsAvailable;
            this.totalVisits = totalVisits;
            this.totalRevisits = totalRevisits;
            this.visits = visits;
            this.reVisits = reVisits;
            this.visitsrevisits = new list<list<integer>>();
            for(integer i=0;i<5;i++)
            {
                list<integer> l1 = new list<integer>();
                l1.add(visits.get(i));
                l1.add(reVisits.get(i));
                this.visitsRevisits.add(l1);
            }
        }

    }
}