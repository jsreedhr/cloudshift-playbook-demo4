@isTest(SeeAllData=true)
private class ContractManagerHandlertest {
    
static testMethod void updateManger(){
    
    UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;

        insert u1;
     
     
     User u2 = new User();
   

     u2.ProfileId = profileId.id ;
     u2.LastName = 'last';
     u2.Email = 'puser001@amamama.com';
     u2.Username = 'puser001@amamama.com' + System.currentTimeMillis();
     u2.CompanyName = 'TEST';
     u2.Title = 'title';
     u2.Alias = 'alias';
     u2.TimeZoneSidKey = 'America/Los_Angeles';
     u2.EmailEncodingKey = 'UTF-8';
     u2.LanguageLocaleKey = 'en_US';
     u2.LocaleSidKey = 'en_US';
     u2.UserRoleId = r2.Id;
      insert u2;
      system.debug('start my-------->' +u2);
    
      System.RunAs(u1)
     {
        Test.startTest();
         testfunc(u1.id,u2.Id);
         Test.stopTest();
     }
    
   
}
    @future
    public static void testfunc(Id userid,Id user2Id)
    {
         
         user usr1=[select id from user where id=:userid limit 1];
      System.RunAs(usr1)
     {
    
    Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
    dev.Sales_Manager__c = userid;
    dev.Number_of_Plots__c = 1; 
       
    insert dev;
    
    Development__c sc1 = [SELECT id, Contracts_Manager__c, Site_Manager__c FROM  Development__c where id=:dev.id ];
        system.debug('start my-------->' +sc1);
  
        
    Plot__c lst_plots = new Plot__c ();
    lst_plots.Name = '1 Aldridge Place';
    lst_plots.Plot_Number__c = '2';
    lst_plots.Development__c = dev.id;
    lst_plots.Sales_Status__c = 'Not Released';
    lst_plots.Build_Status__c = 'Foundations';
    lst_plots.Communal_Plot__c = TRUE;
    lst_plots.Customer_Demo_Date__c = System.today();
    lst_plots.Forecast_Completion_Quality_Audit_Date__c = System.today();
    lst_plots.Forecast_Completion_Date__c = System.today();
    lst_plots.Forecast_First_Fix_Completion_Date__c = System.today();
    lst_plots.Forecast_Pre_Decoration_Completion_Date__c = System.today();
    lst_plots.Forecast_Quality_Manager_Inspection_Date__c =System.today();
    lst_plots.Forecast_Sub_Structure_Completion_Date__c = system.today();
    lst_plots.Forecast_Super_Structure_Completion_Date__c = system.today();
    lst_plots.Number_Of_Beds__c = 2;
    lst_plots.Open_Market_vs_Affordable__c ='Affordable';
    lst_plots.Construction_Comments__c = 'for test'; 
    lst_plots.Size_Sq_Foot__c = 23;
    lst_plots.House_Type__c = 'House';
    //lst_plots.Contracts_Manager_ID__c=String.valueof(usr1);
    
    Insert lst_plots;
    
    Sub_Structure__c sub = new Sub_Structure__c ();
    sub.Name = 'ALB Test';
    sub.Plot_Property__c = lst_plots.id;
    sub.Groundworks_externals_complete__c = TRUE;
    sub.Brickwork_to_standard_of_superstructure__c = TRUE;
    sub.Setting_out_and_level_check_completed__c = TRUE;
    sub.Driveways_roads_completed_to_base_course__c = TRUE;
    sub.Clean_safe_level_access_to_plot__c = TRUE;
    sub.Drainage_connected_open_ends_capped__c = TRUE;
    sub.Scaffold_patio_base_in_to_correct_level__c = TRUE;
    sub.Scaffold_patio_dressed_w_type_1_crush__c = TRUE;
    sub.Oversite_clean_Excess_materials_stored__c = TRUE;
   sub.Plot_service_ducts_installed_considered__c = TRUE;
    sub.Plot_programme_on_site_understood__c = TRUE;
    sub.Areas_of_concern_highlighted_to_team__c = TRUE;
    sub.Building_control_sign_off_complete__c = TRUE;
    sub.Technical_pack_in_place_to_complete_roof__c = TRUE;
    sub.Materials_in_place_to_completion_of_roof__c = TRUE;
    sub.SC_in_place_to_completion_of_roof__c = TRUE;
    sub.Robust_details_applicable_SCs_notified__c = TRUE;
    sub.Photos_Added__c = TRUE;
    sub.Mains_services_installed_to_site__c = TRUE;
    sub.Status__c = 'Completed';
    sub.Plot_Detail_Objects_Created__c = TRUE;
        sub.In_Approval__c = TRUE;
        sub.Approved__c = TRUE;
    sub.Date_Completed__c = System.today();
        sub.Contracts_Manager__c = dev.Contracts_Manager__c;
       // sub.Site_Manager__c = dev.Site_Manager__c;
    
    Insert sub;
    
     Super_Structure__c supersub = new Super_Structure__c();
   
     supersub.Status__c = 'Completed';
     supersub.Name = 'X';
        supersub.Contracts_Manager__c = dev.Contracts_Manager__c;
        //supersub.Site_Manager__c = dev.Site_Manager__c;
     supersub.Plot_Property__c = lst_plots.id;
    // supersub.Contracts_Manager__c = lst_plots.Contracts_Manager_ID__c;
     supersub.Date_Completed__c = System.today();
        supersub.Material_protection_policy_implemented__c = TRUE;
        supersub.Cavities_clean__c = TRUE;
        supersub.Brick_bond_correct__c = TRUE;
       supersub.Joints_fully_filled__c = TRUE;
        supersub.DPC_trays_correct_inspected_regularly__c = TRUE;
        supersub.Stone_work_protected_per_policy__c = TRUE;
        supersub.Weep_holes_in_correct_location__c = TRUE;
        supersub.Brickwork_looks_right__c = TRUE; 
        supersub.Setting_out_cavity_widths_correct__c = TRUE;
        supersub.Lintel_steel_bearings_padstones_correct__c =TRUE;
        supersub.Movement_joints_as_engineers_design__c = TRUE;
        supersub.Bed_joint_reinforcement_installed__c = TRUE;
        supersub.Fire_socks_installed__c = TRUE;
        supersub.Cavity_closed_as_detailed_on_drawings__c = TRUE;
        supersub.Floors_installed_in_line_with_drawings__c = TRUE;
        supersub.Checked_measure_of_structural_openings__c = TRUE;
        supersub.Party_wall_completed_w_robust_details__c = TRUE;
        supersub.Air_leakage_requirements_considered__c = TRUE;
        supersub.Roof_installed_to_drawings__c = TRUE;
        supersub.Facia_soffit_complete_correct__c = TRUE;
        supersub.Valley_hip_detail_complete_correct__c = TRUE;
        supersub.Ridge_detailing_note_flat_roof_areas__c = TRUE;
        supersub.Roof_abutments_detailing_complete__c = TRUE;
        supersub.Flat_roof_areas_detailing_complete__c = TRUE;
        supersub.Wall_plate_straps_installed_fixed__c = TRUE;
        supersub.Ventilation_considered_installed__c = TRUE;
       supersub.Installed_correctly__c = TRUE;
        supersub.Condensation_considered_protected__c = TRUE;
        supersub.Trays_at_correct_levels_flashing_correct__c = TRUE;
        supersub.Trays_to_correct_levels__c = TRUE;
        supersub.To_architects_details__c = TRUE;
        supersub.Cold_bridging_condensation_considered__c = TRUE;
        supersub.Full_partial_fill_insulation_installed__c =TRUE;
        supersub.Fire_stopping_placed_before_felt_batten__c = TRUE;
        supersub.Carpentry_Contracts_Manager_sign_off__c = TRUE;
        supersub.Roofing_Contracts_Manager_sign_off__c = TRUE;
        supersub.Bricklayer_Contract_Manager_sign_off__c = TRUE;
        supersub.I_am_confident_with_the_quality_of_work__c = TRUE;
        supersub.Customer_extras_reviewed__c = TRUE;
        supersub.Customer_occupation_date_reviewed__c = TRUE;
        supersub.Building_control_inspection_completed__c = TRUE;
       supersub.Photos_Added__c = TRUE;
   
    Insert supersub;
    
    First_Fix__c firstfix = new First_Fix__c();
    firstfix.Status__c = 'Completed';
    firstfix.Name = 'X';
        firstfix.Contracts_Manager__c =user2Id;
        //firstfix.Site_Manager__c = dev.Site_Manager__c;
    firstfix.Plot_Property__c = lst_plots.id;
  
    firstfix.Date_Completed__c = System.today();
       firstfix.Cleaned_protection_materials_installed__c = TRUE;
        firstfix.Weather_tight_doors_windows_roofs__c = TRUE;
        firstfix.Screed_correct_quality_line_level__c = TRUE;
        firstfix.Studwork_MF_ceilings_correct__c = TRUE;
        firstfix.Electrical_points_installed_to_layouts__c = TRUE;
        firstfix.Plumbing_installed_to_layouts__c = TRUE;
        firstfix.Renewables_installed_to_layouts__c = TRUE;
        firstfix.Penetrations_in_external_walls_sealed__c = TRUE;
        firstfix.Services_ducted_or_into_the_plot__c = TRUE;
        firstfix.SVPs_installed_through_floors_correctly__c = TRUE;
        firstfix.Ventilation_ducts_installed_supported__c = TRUE;
        firstfix.Waste_pipes_protected_from_structure__c = TRUE;
        firstfix.Installed_in_line_with_design_details__c = TRUE;
        firstfix.Supported_correctly_w_inspection_hatches__c = TRUE;
        firstfix.Window_door_formers_cavity_closures__c = TRUE;
        firstfix.Brickwork_making_good_completed__c = TRUE;
        firstfix.Inaccessible_areas_addressed__c = TRUE;
        firstfix.Complete_to_required_standard__c = TRUE;
        firstfix.Air_test_mastic_completed__c = TRUE;
        firstfix.Air_testing_fully_considered__c = TRUE;
        firstfix.Fixing_noggins_in_place_e_g_radiators__c = TRUE;
        firstfix.Openings_for_internal_doors_correct__c = TRUE;
        firstfix.Plumber_pressure_test_complete__c = TRUE;
        firstfix.Fire_compartments_completed_correctly__c = TRUE;
        firstfix.Trade_inspections_comp_lists_recorded__c = TRUE;
        firstfix.Building_control_inspection_complete__c = TRUE;
        firstfix.Correct_standard_no_defects_built_in__c = TRUE;
        firstfix.Customer_extras_reviewed__c = TRUE;
        firstfix.Customer_occupation_date_reviewed__c = TRUE;
        firstfix.Sales_notified_of_plot_passing_stage__c = TRUE;
        firstfix.I_am_proud_of_the_product_at_this_point__c = TRUE;
        firstfix.Photos_Added__c = TRUE;
        firstfix.Reasons_Checklist_Incomplete__c = 'for test';
        
        
    Insert firstfix;
    
    Pre_Decoration__c predec = new Pre_Decoration__c();
    predec.Status__c = 'Completed';
    predec.Name = 'X';
        predec.Contracts_Manager__c = dev.Contracts_Manager__c;
        //predec.Site_Manager__c = dev.Site_Manager__c;
    predec.Plot_Property__c = lst_plots.id;
   
    predec.Date_Completed__c = System.today();
        predec.All_2nd_fix_trades_100_complete__c = TRUE;
        predec.Ceramic_tiling_complete__c = TRUE;
        predec.Sanitaryware_installed_protected__c = TRUE;
        predec.Shower_screens_installed_protected__c = TRUE;
        predec.Water_live__c = TRUE;
        predec.Electrical_circuits_live_lighting_on__c = TRUE;
        predec.Gas_live__c = TRUE;
        predec.Floors__c = TRUE;
        predec.Pre_finished_doors__c = TRUE;
        predec.Kitchen_worktops_doors__c = TRUE;
        predec.Baths_basins_WCs_showers__c = TRUE;
        predec.Ext_footpaths_complete_w_clean_access__c = TRUE;
        predec.Cleaned_thoroughly_Excess_mtls_removed__c = TRUE;
        Predec.Customer_extras_checked__c = TRUE;
        predec.Sales_notified_of_plot_passing_stage__c = TRUE;
        predec.Customer_occupation_date_reviewed__c = TRUE;
        predec.I_have_escalated_items_out_of_my_control__c = TRUE;
        predec.I_am_in_control_of_BC_site_conditions__c = TRUE;
        predec.Photos_Added__c = TRUE;
    
       
     
    Insert predec;
    
     Completion_Quality_Audit__c compqualaudit = new Completion_Quality_Audit__c();
     compqualaudit.Status__c = 'Completed';
     compqualaudit.Name = 'X';
        compqualaudit.Contracts_Manager__c = dev.Contracts_Manager__c;
       // compqualaudit.Site_Manager__c = dev.Site_Manager__c;
     compqualaudit.Plot_Property__c = lst_plots.id;
    // compqualaudit.Contracts_Manager__c = lst_plots.Contracts_Manager_ID__c;
     compqualaudit.Date_Completed__c = System.today();
     compqualaudit.X100_completed_ready_for_occupation__c = TRUE;
        compqualaudit.Plot_air_test_complete__c = TRUE;
        compqualaudit.Plot_file_complete_w_all_warranty_info__c = TRUE;
        compqualaudit.Plot_re_cleaned_prior_to_QM_inspection__c = TRUE;
        compqualaudit.Trade_specific_inspections_completed__c = TRUE;
        compqualaudit.Plumbing_commissioned_certified__c = TRUE;
        compqualaudit.Electrical_commissioned_certified__c = TRUE;
        compqualaudit.Commissioning_certs_issued_to_Cust_Care__c = TRUE;
        compqualaudit.Gas_safe_certificate_received__c = TRUE;
        compqualaudit.Renewable_energy_commissioned_certified__c = TRUE;
        compqualaudit.Heating_hot_water_in_operation__c = TRUE;
        compqualaudit.EPC_certificate_received__c = TRUE;
        compqualaudit.All_plot_keys_issued_to_Sales__c = TRUE;
        compqualaudit.Building_control_CML_certificate_issued__c = TRUE;
        compqualaudit.Cust_Care_Quality_Mgr_invited_to_inspect__c = TRUE;
        compqualaudit.Customer_demonstration_inspection_booked__c = TRUE;
        compqualaudit.Customer_occupation_date_agreed__c = TRUE;
        compqualaudit.Photos_Added__c = TRUE;
        compqualaudit.Electric_Meter_Number__c = '123';
        compqualaudit.Gas_Meter_Number__c = '123';
        compqualaudit.Water_Meter_Number__c = '123';
     Insert compqualaudit;
    
    Quality_Manager_Inspection__c qualmngrinsp = new Quality_Manager_Inspection__c();
    qualmngrinsp.Status__c = 'Completed';
    qualmngrinsp.Name = 'X';
        qualmngrinsp.Contracts_Manager__c = dev.Contracts_Manager__c;
       // qualmngrinsp.Site_Manager__c = dev.Site_Manager__c;
    qualmngrinsp.Plot_Property__c = lst_plots.id;
    qualmngrinsp.Contracts_Manager__c = lst_plots.Contracts_Manager_ID__c;
    qualmngrinsp.Date_Completed__c = System.today();
    qualmngrinsp.First_Inspection_Date__c = System.today();
        qualmngrinsp.Completion_notified_correctly__c = TRUE;
        qualmngrinsp.Handover_pack_available_complete__c = TRUE;
        qualmngrinsp.Heating_lighting_operational__c = TRUE;
        qualmngrinsp.Cleaned_to_right_standard__c = TRUE;
        qualmngrinsp.First_impressions_correct__c = TRUE;
        qualmngrinsp.Safe_access_available_for_occupation__c = TRUE;
        qualmngrinsp.External_areas_complete__c =TRUE;
        qualmngrinsp.Inspection_completed_list_issued_to_SM__c = TRUE;
        qualmngrinsp.Close_out_works_completed__c = TRUE;
        qualmngrinsp.Plot_accepted__c = TRUE;
        qualmngrinsp.Photos_Added__c = TRUE;
       
    Insert qualmngrinsp;
        
        sc1.Contracts_Manager__c = user2Id;
        update sc1;
        
        update sub;
        update supersub;
        update firstfix;
        update predec;
        update compqualaudit;
        update qualmngrinsp;
        
        
     }
        
    }
}