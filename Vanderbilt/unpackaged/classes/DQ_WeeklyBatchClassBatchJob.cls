global class DQ_WeeklyBatchClassBatchJob implements Database.Batchable<sObject> 
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([
            SELECT Id, Name, Contracts_Manager__c, Site_Manager__c 
            FROM Development__c 
            WHERE Active__c = TRUE 
            AND Enable_Auto_Created_Inspections__c = TRUE
        ]);
    }

    global void execute(Database.BatchableContext BC, List<Development__c> scope)
    {
        List <Inspection__c> inspectionsToCreate = New List <Inspection__c>();
		Id weeklyInspectId = Schema.SObjectType.Inspection__c.RecordTypeInfosByName.get('Site Manager Weekly Inspection').RecordTypeId;

        Date nextMonday = Date.today().addDays(7).toStartofWeek();
        for(Development__c dev : scope)
        {
            Inspection__c WI = new Inspection__c(
				Name = dev.name + '_Inspection',
				Status__c = 'Not Started',
				RecordTypeID = System.Label.Inspection_Weekly_Site_Manager_RT_Id,
				Development__c = dev.id,
				Contracts_Manager__c = dev.Contracts_Manager__c,
				Site_Manager__c = dev.Site_Manager__c,
				OwnerId = dev.Site_Manager__c,
				Created_From_Batch__c = True,  
                Report_Date__c = nextMonday
			);         
			inspectionsToCreate.add(WI);                                                        
		}        
		system.debug(inspectionsToCreate);
        insert inspectionsToCreate;
		system.debug(inspectionsToCreate);
    }

    global void finish(Database.BatchableContext BC) 
	{

    }

}