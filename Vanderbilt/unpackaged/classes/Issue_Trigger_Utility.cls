/**
 *  @Class Name:    Issue_Trigger_Utility 
 *  @Description:   This is a utility class for Issue Trigger
 *  @Company: dQuotient
 *  CreatedDate: 04/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran				06/10/2016                  Orginal Version 
 * */
 
public with sharing class Issue_Trigger_Utility{

    
 
    /**
     *  Method Name:    ContractorsListPopulateInsert 
     *  Description:    Method to Populate ContractorList
     *  Param:          Trigger.newMap
     *  Return:         None
     */
    public static void ContractorsListPopulateInsert(map<id,Issue__c> mapNewIssue){
                Set<Id> setIssueId=new Set<Id>();
                Set<Id> setCaseId=new Set<Id>();
                List<Issue__c> lstOfIssues=new List<Issue__c>();
	            for(Issue__c objIssue:mapNewIssue.values()){
	                setIssueId.add(objIssue.id);
	                setCaseId.add(objIssue.Case__c);
	            }
	            system.debug('setIssueId+++'+setIssueId);
                lstOfIssues=[select id,Case__c,Case__r.Contractors_List__c,Contractor__c,Contractor__r.name from Issue__c where Case__c in :setCaseId];
                Map<Id,Case> CaseMap=new Map<Id,Case>([select id,Contractors_List__c from Case where id in:setCaseId]);
                system.debug('lstOfIssues+++++'+lstOfIssues);
                
                for(Case Cs:CaseMap.values() ){
                    
                    Cs.Contractors_List__c ='';
                     System.debug('-------->'+Cs);
                }
               
                if(lstOfIssues!=null &&lstOfIssues.size()>0){
                for (Issue__c objIssue:  lstOfIssues){
                    if(CaseMap.containsKey(objIssue.case__c)&&String.isNotBlank(objIssue.Contractor__c)&& CaseMap.get(objIssue.case__c).Contractors_List__c!='') {
                           System.debug('1-------->'+objIssue.Contractor__r.name);
                      
                          CaseMap.get(objIssue.case__c).Contractors_List__c+=',';
                          CaseMap.get(objIssue.case__c).Contractors_List__c+=' ';
                          CaseMap.get(objIssue.case__c).Contractors_List__c+=objIssue.Contractor__r.name;
                          
                       }
                       else if(CaseMap.containsKey(objIssue.case__c)&&String.isNotBlank(objIssue.Contractor__c)&&CaseMap.get(objIssue.case__c).Contractors_List__c==''){
                            System.debug('2-------->'+objIssue.Contractor__r.name);
                            CaseMap.get(objIssue.case__c).Contractors_List__c=objIssue.Contractor__r.name;
                       }
                       
                    
                 
                }
                
                }
                try{
                    update CaseMap.values();
                }catch(Exception e){
                    
                }
                 
              
            
           
           
            
    
    }
    
    
}