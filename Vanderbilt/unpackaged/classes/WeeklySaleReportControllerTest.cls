@istest
public class WeeklySaleReportControllerTest{
static testMethod void SalesReport(){
    
    UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;
     insert u1;
System.RunAs(u1)
     {
        Test.startTest();
         method1(u1.id);
         Test.stopTest();
     }
}
    @future
        public static void method1(Id userid){
                 
                    Account a = new Account();
                    a.name ='ABC';
                    a.type ='Other';
                    Insert a;
                    
                    contact c = new contact();
                    c.lastname='ABC';
                    c.accountId=a.id;
                    c.email='abc@gmail.com';
                    c.Salutation='Mr.';
                    insert c;
            
            Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
    dev.Sales_Manager__c = userid; 
    dev.Active__c = TRUE;
    insert dev;
                 
    Plot__c plot = new Plot__c();
    plot.Name = '1 Court Road';
    plot.Plot_Number__c = '1';
    plot.Development__c = dev.id;
    plot.Number_of_Beds__c = 3;
    plot.Open_Market_vs_Affordable__c = 'Affordable';
    plot.Size_Sq_Foot__c = 57;
    plot.House_Type__c = 'House';
    
    insert plot;
                          
                    list<opportunity> oppList = new list<Opportunity>();
                    opportunity op = new opportunity();
                   op.amount = 1000;
                    op.Reservation_Fee__c=900; 
                    op.AccountId=a.id;
                    op.name='XYZ';
                    op.StageName='Reserved';
                    op.CloseDate= system.today()+1;
                    op.Reservation_Date__c = system.today() +2;
                    op.Reservation_Deadline__c = system.today() +10;
                    op.Primary_Purchaser__c = c.id;
                   // op.Development_lookup__c = dev.id;
                    op.Plot_Property__c = plot.id;
                    insert op;
          
             PageReference Pageref = page.WeeklySalesReport;
     test.setCurrentPage(Pageref);
   // ApexPages.currentPage().getParameters().put(oppList);
        WeeklySaleReportController Report = new WeeklySaleReportController();
        
    
         Report.exportMethod();
        
      
                  
          WeeklySaleReportController.MemberWrapper Wrapper = new WeeklySaleReportController.MemberWrapper(op);
    Wrapper.compareTo(Wrapper);
        
        }
        
        }