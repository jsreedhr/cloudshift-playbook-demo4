public with sharing class CoinsWsdl
{
    @future(callout=true)
    public static void generateXML(id plotid)
    {
        Plot__c plotObj = new Plot__c();
        plotObj =[Select id,COINS_Development_Code__c,Plot_Number__c,Completion_Date__c,Reservation_Price__c,Legal_Fees__c,
                 Incentives_Cash__c,Extras_Standard_Rated__c,Extras_Zero_Rated__c,Incentives_Non_Cash__c,Reservation_Deposit__c,
                 Legal_Disbursements__c,Ground_Rent_Per_Annum__c,Est_Service_Charge__c from Plot__c where id=:plotid LIMIT 1];
        if(plotObj!=null )
        {
            Coins_Credentials__c cred = Coins_Credentials__c.getall().get('Credentials');
            if(plotObj.COINS_Development_Code__c!=NULL && plotObj.Plot_Number__c!=NULL && plotObj.Completion_Date__c!=NULL)
            {
                //Formatting Completion date for Coins
                String completionDate = DateTime.newInstance(plotObj.Completion_Date__c.year(),plotObj.Completion_Date__c.month(),plotObj.Completion_Date__c.day()).format('YYYY-MM-dd');
                //Adding 0's before Plot Number
                String plotNum;
                String plotNumSize = String.valueOf(plotObj.Plot_Number__c);
                if(plotNumSize.length()==1)
                    plotNum='00'+plotObj.Plot_Number__c;
                else
                if(plotNumSize.length()==2)
                    plotNum='0'+plotObj.Plot_Number__c;
                else
                    plotNum=plotObj.Plot_Number__c;
                String myXML;
                myXML= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">';
                myXML+='<soapenv:Header/>';
                myXML+='<soapenv:Body>';
                myXML+='<COINSInterface>';
                myXML+='<Header id="" confirm="" action="" entity="" arguments="" ackID="" testMsg="">';
                myXML+='<UserID></UserID>';
                myXML+='<From>?</From>';
                myXML+='<!--Optional:-->';
                myXML+='<HostName>UKLD5UX011.coinscloud.com</HostName>';
                myXML+='<Environment>';
                myXML+=cred.Coins_Environment__c;
                myXML+='</Environment>';
                myXML+='<Login>';
                myXML+='<User>';
                myXML+=cred.Coins_Username__c;
                myXML+='</User>';
                myXML+='<Password>';
                myXML+=cred.Coins_Password__c;
                myXML+='</Password>';
                myXML+='<CID>3</CID>';
                myXML+='<extUser></extUser>';
                myXML+='<extAuth>?</extAuth>';
                myXML+='</Login>';
                myXML+='</Header>';
                myXML+='<Body>';
                myXML+='<hs_statementRow>';
                myXML+='<kco>3</kco>';
                myXML+='<job_num>';
                myXML+=plotObj.COINS_Development_Code__c;
                myXML+='</job_num><vwb_code>';
                myXML+=plotNum;
                myXML+='</vwb_code><hst_tdate>';
                myXML+=completionDate;
                myXML+='</hst_tdate><hde_stmnt>cs00</hde_stmnt>';
                /*Placeholder*/
                myXML+='Replace_me';
                myXML+='</hs_statementRow>';
                myXML+='</Body>';
                myXML+='</COINSInterface>';
                myXML+='</soapenv:Body>';
                myXML+='</soapenv:Envelope>';
                String placeholderString='';
                if(plotObj.COINS_Development_Code__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>100</hdd_line><hsd_value>';
                    s+=plotObj.Reservation_Price__c;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Incentives_Cash__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>110</hdd_line><hsd_value>';
                    s+=plotObj.Incentives_Cash__c;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Extras_Standard_Rated__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>150</hdd_line><hsd_value>';
                    s+=plotObj.Extras_Standard_Rated__c/1.2;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Extras_Zero_Rated__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>140</hdd_line><hsd_value>';
                    s+=plotObj.Extras_Zero_Rated__c;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Incentives_Non_Cash__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>179</hdd_line><hsd_value>';
                    s+=plotObj.Incentives_Non_Cash__c;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Legal_Fees__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>320</hdd_line><hsd_value>';
                    s+=plotObj.Legal_Fees__c/1.2;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Legal_Disbursements__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>340</hdd_line><hsd_value>';
                    s+=plotObj.Legal_Disbursements__c/1.2;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Ground_Rent_Per_Annum__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>550</hdd_line><hsd_value>';
                    s+=plotObj.Ground_Rent_Per_Annum__c;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(plotObj.Est_Service_Charge__c!=NULL)
                {
                    String s;
                    s='<hs_stmntdetRow><hdd_line>560</hdd_line><hsd_value>';
                    s+=plotObj.Est_Service_Charge__c;
                    s+='</hsd_value></hs_stmntdetRow>';
                    placeholderString+=s;
                }
                if(String.isNotEmpty(placeholderString))
                {
                    String target='Replace_me';
                    myXML=myXML.replace(target,placeholderString);
                }
                System.debug('XML: '+myXML);
                HttpRequest req = new HttpRequest();
                req.setEndpoint(cred.Coins_Endpoint_URL__c);    
                req.setBody(myXML) ;
                req.setMethod('POST');
                Http http = new Http();
                String response;
                if(Test.isRunningTest())
                    response ='action="ACKNOWLEDGE"';
                else
                {
                    HTTPResponse res = http.send(req);
                    response = res.getbody();
                    System.Debug('HTTP Response: '+response);
                }
                System.debug(response);
                if(response!=NULL)
                {
                    if(response.contains('action="ACKNOWLEDGE"'))
                    {
                        System.debug('SUCCESFUL');
                        plotObj.COINS_Response_Message__c='SUCCESFUL';
                    }   
                    else
                        plotObj.COINS_Response_Message__c='FAILED';
                }
            }
            else
                plotObj.COINS_Response_Message__c='FAILED - Please ensure the plot has valid Coins Development code,Plot Number & Completion Date';
            plotObj.Coins_Response_Recieved_At__c=System.Now();
            try{
            update plotObj;
            }catch(DmlException e)
            {
                System.Debug('DML Exception:'+e);
            }    
        }
    }
}