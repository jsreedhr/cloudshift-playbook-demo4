/**
 *  @Class Name:    PlotDevSpecificationControllerExt 
 *  @Description:   This is a controller for Cloning Development Specification
 *  @Company: dQuotient
 *  CreatedDate: 17/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran George               17/10/2016         Orginal Version
 *
 */

public class PlotDevSpecificationControllerExt 
{

	public List<SelectedWrapper> listWrappSel {get; set;}
	Public Plot__c objPlot;
	public String PlotId{get;set;}
	transient public String Developmentname{get;set;}
    public PlotDevSpecificationControllerExt() 
	{
	    List<Development_Specification__c>dcs=new List<Development_Specification__c>();
		listWrappSel = new List<SelectedWrapper>();
		PlotId = apexPages.currentPage().getParameters().get('id');
		if(string.isNotBlank(PlotId)){
		    objPlot=[select id, Development__c,Development__r.name,name from Plot__c where id=:PlotId limit 1];
		    Developmentname=objPlot.Development__r.name;
		    
		}
        if(objPlot!=null)
            dcs = [select id, Development__c, Name,File_Name__c, Specification__c,CreatedBy.Name,CreatedDate from Development_Specification__c where Development__c =:objPlot.Development__c order by File_Name__c,CreatedDate limit 10000];
		if(dcs!=null &&dcs.size()>0){
    		for(Development_Specification__c dc : dcs)
    		{
    			listWrappSel.add(new SelectedWrapper(dc));
    		}
		}
		
	}
	
	public PageReference processSelected() {
		
		List<Plot_Specification__c> lstPlotSpecToClone = new List<Plot_Specification__c>();
	
		for(SelectedWrapper wdev: listWrappSel) 
		{
			if(wdev.selected  == true)
			{   system.debug('------wrap++++'+wdev);
				Plot_Specification__c newPlotSpec=new Plot_Specification__c();
			    newPlotSpec.Plot__c=ObjPlot.id;
			    newPlotSpec.File_Name__c=wdev.dcs.File_Name__c;
			    newPlotSpec.Specification__c=wdev.dcs.Specification__c;
			    newPlotSpec.isClone__c=true;
				lstPlotSpecToClone.add(newPlotSpec);
			    system.debug('------lstPlotSpecToClone++++'+lstPlotSpecToClone);
			    
			}
		}
		system.debug('lstPlotSpecToClone++++'+lstPlotSpecToClone.size());
		if(lstPlotSpecToClone.size() >0 &&lstPlotSpecToClone!=null)
		{
			try{
			insert lstPlotSpecToClone;
			}
			catch(Exception e){
				
			}
			return new PageReference('/'+PlotId);
		}
        
		return null;
	}
	public Pagereference Cancel(){
	    return new PageReference('/'+PlotId);
	}
	
	public class SelectedWrapper
	{
		public Boolean selected {get; set;}
	    public Development_Specification__c dcs{get;set;}
		public SelectedWrapper(Development_Specification__c dcs) 
		{
			this.dcs = dcs;
			selected=false;
			
		}
	}
}