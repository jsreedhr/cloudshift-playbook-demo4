@isTest
private class Test_schduleBatchWeeklySiteManager {
	public static testMethod void SchduleWeeklyReport() {
	UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
     Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;

        insert u1;
  System.RunAs(u1)
     {
        Test.startTest();
         allowtest(u1.id);
         Test.stopTest();
     }
    
   
}
    @future
    public static void allowtest(Id userid)
    {
    
    Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
    dev.Sales_Manager__c = userid; 
    dev.Active__c = TRUE;
       
     
    insert dev;


		String recordstr=System.Label.Inspection_Weekly_Site_Manager_RT_Id ;

		Inspection__c inspect = new Inspection__c();		
		inspect.Development__c = dev.id;
		inspect.Name = 'Old Trafford Manager Monthly Inspection';
		inspect.RecordTypeId = recordstr;
		inspect.Report_Date__c = System.Date.Today().AddDays(7);
		inspect.Site_Manager__c = userid;
       
        
		insert inspect;

		
           schduleBatchWeeklySiteManager obj = new  schduleBatchWeeklySiteManager();
          String cron = '0 0 23 * * ?';
          System.schedule('Test Schedule', cron, obj);    
      
	}
}