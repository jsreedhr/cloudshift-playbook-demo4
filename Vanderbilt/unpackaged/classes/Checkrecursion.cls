public class Checkrecursion
{
     public static boolean isFirstTime = true;

    public static boolean runAfterUpdateOnce()
    {
        
    if(isFirstTime)
    {
     isFirstTime = false;
     return true;   
    }
    else
    {
        return isFirstTime;
    }
        
    }
     public static boolean isFirstTimeSubStructure = true;

    public static boolean runAfterUpdateOnceSubStructure()
    {
        
    if(isFirstTimeSubStructure)
    {
     isFirstTimeSubStructure = false;
     return true;   
    }
    else
    {
        return isFirstTimeSubStructure;
    }
        
    }
}