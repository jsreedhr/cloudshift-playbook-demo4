@isTest
/**
 *  @Class Name:    ReservationAgreementControllerExt
 *  @Description:   This is a test class for ReservationAgreementControllerExt
 *  @Company: dQuotient
 *  CreatedDate: 13/12/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Pankaj Nayak              25/11/2016                  Orginal Version
 *
 */
private class ReservationAgreementControllerExtTest{

    private static testMethod void reservationAgreementTest() {
       
        
            Account a = new Account();
            a.name ='ABC';
            a.type ='Other';
            Insert a;
            
            contact c = new contact();
            c.lastname='ABC';
            c.accountId=a.id;
            c.email='abc@gmail.com';
            c.Salutation='Mr.';
            insert c;
            
            
    
            list<opportunity> oppList = new list<Opportunity>();
            opportunity op = new opportunity();
            op.amount = 1000;
            op.Reservation_Fee__c=900; 
            op.AccountId=a.id;
            op.name='XYZ';
            op.StageName='Reserved';
            op.CloseDate= system.today();
            op.Reservation_Date__c = system.today() +2;
            op.Reservation_Deadline__c = system.today() +10;
            op.Primary_Purchaser__c = c.id;
            op.Anticipated_Exchange_Date__c = system.today() - 2;
        
            
            
            
            insert op;
            op.amount = 89898;
            update op;
            
            Apexpages.currentPage().getParameters().put('id',op.id);
            ApexPages.StandardController stdcontroller = new ApexPages.StandardController(op);
            ReservationAgreementControllerExt ctr = new ReservationAgreementControllerExt(stdcontroller);
            ctr.attachPdf();
            
    }

}