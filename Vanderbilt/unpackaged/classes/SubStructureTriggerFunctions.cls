public without sharing class SubStructureTriggerFunctions {
    
    //Method to create plot checklists when a sub-structure has been completed
    public static void createplotchecklists(List <Sub_Structure__c> lst_substructures){
    
        
    List <Sub_Structure__c> lst_substoprogress = new List<Sub_Structure__c>();
    Set <ID> set_plotids = new Set <ID>();
    List <Super_Structure__c> lst_superstructures = new List <Super_Structure__c>();
    List <First_Fix__c> lst_firstfixes = new List <First_Fix__c>();
    List <Pre_Decoration__c> lst_predecs = new List <Pre_Decoration__c>();
    List <Completion_Quality_Audit__c> lst_compqualaudits = new List <Completion_Quality_Audit__c>();
    List <Quality_Manager_Inspection__c> lst_qualmngrinspections = new List <Quality_Manager_Inspection__c>();   
        
        for(Sub_Structure__c s: lst_substructures){
            
        	If(s.Plot_Detail_Objects_Created__c == False && s.Status__c == 'Completed'){
                lst_substoprogress.add(s);
                set_plotids.add(s.Plot_Property__c);                    
            }           
        }
        
        IF(!lst_substoprogress.isEmpty()){
            
           List <Plot__c> lst_plots = new List <Plot__c>([SELECT ID, Development__c, Communal_Plot__c, Contracts_Manager_ID__c FROM Plot__c WHERE ID in: set_plotids]);
           
            IF(!lst_plots.isEmpty()){
                
                for(Plot__c p: lst_plots){
                    Super_Structure__c supersub = new Super_Structure__c();
                    supersub.Status__c = 'Not Started';
                    supersub.Name = 'X';
                    supersub.Plot_Property__c = p.id;
					supersub.Contracts_Manager__c = p.Contracts_Manager_ID__c;
					IF(p.Communal_Plot__c == False){
                        supersub.RecordTypeId = System.Label.Super_Structure_Plot_RT_Id;
                    }
                    IF(p.Communal_Plot__c == True){
                        supersub.RecordTypeId = System.Label.Super_Structure_Communal_Plot_RT_Id;
                    }                    
					lst_superstructures.add(supersub);
                    
                    First_Fix__c firstfix = new First_Fix__c();
                    firstfix.Status__c = 'Not Started';
                    firstfix.Name = 'X';
                    firstfix.Plot_Property__c = p.id;
					firstfix.Contracts_Manager__c = p.Contracts_Manager_ID__c;
                    IF(p.Communal_Plot__c == False){
                        firstfix.RecordTypeId = System.Label.First_Fix_Plot_RT_Id;
                    }
                    IF(p.Communal_Plot__c == True){
                        firstfix.RecordTypeId = System.Label.First_Fix_Communal_Plot_RT_Id;
                    }                    
					lst_firstfixes.add(firstfix);
                    
                    Pre_Decoration__c predec = new Pre_Decoration__c();
                    predec.Status__c = 'Not Started';
                    predec.Name = 'X';
                    predec.Plot_Property__c = p.id;
					predec.Contracts_Manager__c = p.Contracts_Manager_ID__c;
                    IF(p.Communal_Plot__c == False){
                        predec.RecordTypeId = System.Label.Pre_Decoration_Plot_RT_Id;
                    }
                    IF(p.Communal_Plot__c == True){
                        predec.RecordTypeId = System.Label.Pre_Decoration_Communal_Plot_RT_Id;
                    }
					lst_predecs.add(predec);
                    
                    Completion_Quality_Audit__c compqualaudit = new Completion_Quality_Audit__c();
                    compqualaudit.Status__c = 'Not Started';
                    compqualaudit.Name = 'X';
                    compqualaudit.Plot_Property__c = p.id;
					compqualaudit.Contracts_Manager__c = p.Contracts_Manager_ID__c;
					IF(p.Communal_Plot__c == False){
                        compqualaudit.RecordTypeId = System.Label.Completion_Quality_Audit_Plot_RT_Id;
                    }
                    IF(p.Communal_Plot__c == True){
                        compqualaudit.RecordTypeId = System.Label.Completion_Quality_Audit_Communal_Plot_RT_Id;
                    }                    
					lst_compqualaudits.add(compqualaudit);
                    
                    Quality_Manager_Inspection__c qualmngrinsp = new Quality_Manager_Inspection__c();
                    qualmngrinsp.Status__c = 'Not Started';
                    qualmngrinsp.Name = 'X';
                    qualmngrinsp.Plot_Property__c = p.id;
					qualmngrinsp.Contracts_Manager__c = p.Contracts_Manager_ID__c;
					IF(p.Communal_Plot__c == False){
                        qualmngrinsp.RecordTypeId = System.Label.Quality_Manager_Inspection_Plot_RT_Id;
                    }
                    IF(p.Communal_Plot__c == True){
                        qualmngrinsp.RecordTypeId = System.Label.Quality_Manager_Inspection_Communal_Plot_RT_Id;
                    }                    
					lst_qualmngrinspections.add(qualmngrinsp);
                }
                insert lst_superstructures;
                insert lst_firstfixes;
                insert lst_predecs;
                insert lst_compqualaudits;
                insert lst_qualmngrinspections;
                
                for(Sub_Structure__c s: lst_substoprogress){
                s.Plot_Detail_Objects_Created__c = TRUE;                    
                } 
            }
        }
    }
}