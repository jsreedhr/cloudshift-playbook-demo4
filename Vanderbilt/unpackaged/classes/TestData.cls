/**
 *  @Class Name:    TestData
 *  @Description:   This is a utility class for Test Data
 *  @Company: dQuotient
 *  CreatedDate: 18/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran George        18/10/2016                  Orginal Version
 *  Jeevraj Rao         15/11/2017                  added CreateSobject
 */
 @isTest
 public class TestData{
    
    private class TestDataException extends Exception {}

    /************************** USER ***********************************/
    public static final String USER_PROFILE_ADMIN                           = 'System Administrator';
    
    /*************************** DEVELOPMENT **************************************/    
    public static final String DEVELOPMENT_OBJECT                           = 'Development__c';
    
    /*************************** ACCOUNT **************************************/
    public static final String ACCOUNT_OBJECT                               = 'Account';
    private static final Id ACCOUNT_RT_ID                                   = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();

    private static final String SOBJECT_NAME_IS_NOT_VALID_MSG               = 'SObject Name Is Not Valid';
    private static final String SOBJECT_DEFAULT_VALUES_ARE_NOT_DEFINED_MSG  = 'Default Values Are Not Defined';
    
    private static final String STREET                                      = 'MLC Centre, 2/108 King St';
    private static final String CITY                                        = 'Sydney';
    private static final String COUNTY                                      = 'NSW';
    private static final String POSTCODE                                    = 'NSW 2000';
    private static final String COUNTRY                                     = 'Australia';
    
    //static map to store Profiles
    private static Map<String, Id>  mapNameToProfileId;
    
    public static final Map<String, Object> ACCOUNT_DEFAULT_VALUES_MAP = new Map<String, String>{
            'RecordtypeId'              =>  ACCOUNT_RT_ID,
            'Name'                      =>  'TestAccount',
            'Type'                      =>  'Solicitor'
    };
    
    public static final Map<String, Object> DEVELOPMENT_DEFAULT_VALUES_MAP = new Map<String, String>{
            'Marketing_Name__c'         => 'FMO',
            'COINS_Development_Code__c' => '123',
            'Street__c'                 => STREET,
            'City__c'                   => CITY,
            'State_Province__c'         => COUNTY,
            'Post_Code__c'              => POSTCODE,
            'Country__c'                => COUNTRY,
            'Name'                      => 'Test'
    };
    
    private static Map<String, Map<String, Object>> sObjectToDefaultValuesMap = new Map<String, Map<String, Object>> {
            DEVELOPMENT_OBJECT          =>      DEVELOPMENT_DEFAULT_VALUES_MAP,
            ACCOUNT_OBJECT              =>      ACCOUNT_DEFAULT_VALUES_MAP
    };
    
    // public method to get profile Id by profile name
    public static String getProfileId(String profileName){
        if (mapNameToProfileId == null) {
            mapNameToProfileId = new Map<String, Id>();
            for (Profile profile : [SELECT Id, Name
            FROM Profile]) {
                mapNameToProfileId.put(profile.Name, profile.Id);
            }
        }

        if (!mapNameToProfileId.containsKey(profileName)) {
            throw new TestDataException('There is no profile with this name: ' + profileName);
        }

        return mapNameToProfileId.get(profileName);
    }
    
    /*************************** USER ***************************/
    public static Integer userCounter = 0;
    public static User createUser(String profileId) {
        User myUser = new User();
        myUser.ProfileId         = profileId;
        myUser.UserName          = 'unit.Tester' + userCounter + '@test.com';
        myUser.FirstName         = 'Unit';
        myUser.LastName          = 'Tester' + userCounter;
        myUser.Alias             = 'UT' + userCounter;
        myUser.Email             = 'dev' + userCounter + '@Tester.com';
        myUser.EmailEncodingKey  = 'UTF-8';
        myUser.LanguageLocaleKey = 'en_US';
        myUser.LocaleSidKey      = 'en_US';
        myUser.TimeZoneSidKey    = 'Europe/London';
        ++userCounter;
        return myUser;
    }
    
    public static sObject createSobject(String sObjectName, Boolean toInsert) {
        return createSobject(sObjectName, toInsert , null);
    }
    
    /*************************** Generic Sobject ***************************/

    public static sObject createSobject(String sObjectName, Boolean toInsert, Map<String,Object> sObjectFields) {

        String actualSObjectName = sObjectName;
        if(!Schema.getGlobalDescribe().containsKey(actualSObjectName)) {
            throw new TestDataException(SOBJECT_NAME_IS_NOT_VALID_MSG);
        }

        sObject newsObj = Schema.getGlobalDescribe().get(actualSObjectName).newSObject();

        if(!sObjectToDefaultValuesMap.containsKey(sObjectName)) {
            throw new TestDataException(SOBJECT_DEFAULT_VALUES_ARE_NOT_DEFINED_MSG);
        }

        Map<String, Object> sObjectDefaultValuesMap = sObjectToDefaultValuesMap.get(sObjectName);

        for(String field: sObjectDefaultValuesMap.keySet()){
            newsObj.put(field, sObjectDefaultValuesMap.get(field));
        }

        if(sObjectFields != null) {
            for(String field: sObjectFields.keySet()){
                newsObj.put(field, sObjectFields.get(field));
            }
        }

        if(toInsert){
            insert newsObj;
        }
        return newsObj;
    }

   /**
     *  Method Name:    createUser
     *  Description:    Method to create Test User
     *  Param: None
     *  Return: User
     */
    
    public static User createUser() {
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        User objUser = new User(Alias = 'newUser', Email='newuser@testorg.com',
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        return objUser;
    }
    
    /**
     *  Method Name:    createBusinessAccount
     *  Description:    Method to create Test Business Account
     *  Param: None
     *  Return: Account
     */
    
    public static Account createBusinessAccount() {
        List<RecordType> lstRecordType = new List<RecordType>();
        lstRecordType = [Select id, name from RecordType where DeveloperName = 'Business_Account' limit 1];
        Account objAccount = new Account();
        objAccount.Name = 'Vanderbilt';
        objAccount.Website='www.test.com';
        objAccount.Type='Vanderbilt';
        return objAccount;
    }
    
     /**
     *  Method Name:    createBusinessAccount2
     *  Description:    Method to create Test Business Account
     *  Param: None
     *  Return: Account
     */
    
    public static Account createBusinessAccount2() {
        List<RecordType> lstRecordType = new List<RecordType>();
        lstRecordType = [Select id, name from RecordType where DeveloperName = 'Business_Account' limit 1];
        Account objAccount = new Account();
        objAccount.Name = 'VanderbiltNew';
        objAccount.Website='www.tests.com';
        return objAccount;
    }
    
     /**
     *  Method Name:    createPersonAccount
     *  Description:    Method to create Test Person Account
     *  Param: None
     *  Return: Account
     */
    
    public static Account createPersonAccountnew() {
       String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;

       Account Accnt = new Account(PersonMailingStreet='test@yahoo.com',
       LastName='Test',
       RecordTypeId=RecTypeId,
       PersonMailingPostalCode='12345',
       PersonMailingCity='SFO',
       PersonEmail='test@yahoo.com',
       PersonHomePhone='1234567',
       PersonMobilePhone='12345678',
       PersonOtherPhone='123242432',
       
       Salutation='Mr.',
       
       BillingStreet='Liverpool',
       BillingState='London',
       BillingCity='hashinh'
       );
  
       return Accnt;
    }
    
    public static Account createPersonAccount() {
        List<RecordType> lstRecordType = new List<RecordType>();
        lstRecordType = [Select id, name from RecordType where DeveloperName = 'PersonAccount' limit 1];
        Account objAccount = new Account();
        objAccount.LastName = 'Account';
        objAccount.FirstName='newtest';
        return objAccount;
    }
    
     /**
     *  Method Name:    createContact
     *  Description:    Method to create Contact
     *  Param: Account
     *  Return: Contact
     */
    
    public static Contact createContact(Account objAccount) {
        if(objAccount.id != null){
            Contact objContact = new Contact();
            objContact.Salutation='Mr.';
            objContact.AccountId = objAccount.id;
            objContact.LastName = 'Test'+Math.random();
            objContact.Email = 'test@test.com'+Math.random();
            return objContact;
        }else{
            return null;
        }
    }
     
    /**
     *  Method Name:    createDevelopment
     *  Description:    Method to create Test Development
     *  Param: None
     *  Return: Development__c
     */
    
    public static Development__c createDevelopment(Contact objcont) {
        Development__c devel=new Development__c();
        devel.Budget__c=5000;
        devel.Budget_Spend_to_Date__c=2000;
        devel.City__c='Bangalore';
        devel.Country__c='India';
        devel.Marketing_Name__c='testmarketing';
        devel.Post_Code__c='560017';
        //devel.Site_Manager__c=objcont.id;
        devel.State_Province__c='Karnataka';
        devel.Street__c='Rustam Bagh';
        return devel;
    }
    
    /**
     *  Method Name:    createDevSpecification
     *  Description:    Method to create Test Development Specification
     *  Param: Development__c
     *  Return: Development_Specification__c
     */
     
    public static Development_Specification__c createDevSpecification(Development__c dev) {
        Development_Specification__c devspec=new Development_Specification__c();
        devspec.Development__c=dev.id;
        devspec.File_Name__c='dQuotient Bangalore';
        //devspec.Specification__c='www.vanderbilt.co.uk';
        
        return devspec;
    }
    
    /**
     *  Method Name:   createPlot
     *  Description:    Method to create Test Plot
     *  Param: Account,Development__c
     *  Return: Plot__c
     */
    
    public static Plot__c createPlot(Account objacc,Development__c dev) {
        
        Plot__c objPlot = new Plot__c();
        objPlot.Budget_Spend_to_Date__c =2000;
        objPlot.House_Type__c = 'House';
        objPlot.City__c = 'Bangalore';
        objPlot.Completion_Date__c =system.today();
        objPlot.Development__c = dev.id;
        objPlot.Incumbent__c =objacc.id;
       
        objPlot.Plot_Number__c = '1234';
        objPlot.Post_Code__c = '560017';
        objPlot.Sold_To__c = objacc.id;
        objPlot.State_Province__c = 'karantaka';
        objPlot.Street__c = 'Rustam Bagh';
        objPlot.Warranty_End__c = system.today();
        objPlot.Warranty_Start__c = system.today().adddays(-100);
        objPlot.Warranty_Tasks_Created__c = true;
        objPlot.Number_of_Beds__c = 12;
        objPlot.Open_Market_vs_Affordable__c = 'Open Market';
        objPlot.Size_Sq_Foot__c = 120;
        return objPlot;
    }
     
      /**
     *  Method Name:   createSnag
     *  Description:    Method to create Test Snag
     *  Param: Plot__c
     *  Return: Snag__c
     */
    
       public static Snag__c createSnag(Plot__c objplot){
        
        Snag__c objSnag = new Snag__c();
        objSnag.Plot_Property__c = objplot.id;
        objSnag.Location__c = 'Bedroom 1';
        objSnag.Category__c = 'Air Conditioning';
        objSnag.Description__c = 'Description';
        objSnag.Status__c = 'Raised';
        return objSnag;
    }
     
     /**
     *  Method Name:   createPlotNoDev
     *  Description:    Method to create Test Plot
     *  Param: Account
     *  Return: Plot__c
     */
    
    public static Plot__c createPlotNoDev(Account objacc,Account acc) {
        
        Plot__c objPlot = new Plot__c();
        objPlot.Completion_Date__c =system.today();
        objPlot.Incumbent__c =objacc.id;
        objPlot.Sold_To__c = acc.id;
        objPlot.Number_of_Beds__c = 12;
        objPlot.Open_Market_vs_Affordable__c = 'Open Market';
        objPlot.Size_Sq_Foot__c = 120;
        
        return objPlot;
    }
    
    /**
     *  Method Name:    createCase
     *  Description:    Method to create Test Case
     *  Param: Contact, Plot__c
     *  Return: Case
     */
    
    public static Case createCase(Plot__c objPlot,Contact objContact) {
        
        Case objCase = new Case();
        objCase.Contact =objContact;
        objCase.Case_Categories__c = 'Air Conditioning';
        objCase.Closed_Reviewed__c = false;
        objCase.Contractors_List__c = '';
        objCase.Couple__c = true;
        objCase.Due_Date__c=system.today();
        objCase.Initial_Response_Email__c = true;
        objCase.Plot__c =objPlot.id;
        objCase.Special_Dispensation__c =false;
        
        return objCase;
    }
    /**
     *  Method Name:    createCase
     *  Description:    Method to create Test Case
     *  Param: Contact, Plot__c
     *  Return: Case
     */
    
    public static Case createCaseNoPlot(Account objAccount,Contact objContact) {
        
        Case objCase = new Case();
        objCase.AccountId=objAccount.id;
        objCase.Case_Categories__c = 'Air Conditioning';
        objCase.Contact=objContact;
        objCase.Origin='Email';
     
        
        return objCase;
    }
    
    /**
     *  Method Name:    createIssues
     *  Description:    Method to create Test Issues under Case
     *  Param: Account,Contact,Case
     *  Return: Issue__c
     */
    
    public static Issue__c createIssues(Account objAccount,Case objCase,Contact objContact) {
        
        Issue__c objIssue = new Issue__c();
        objIssue.Budget_Spend_to_Date__c = 2000;
        objIssue.Case__c = objCase.id;
        objIssue.Category__c ='Air Conditioning';
        objIssue.Contractor__c =objAccount.id;
       
        objIssue.Description__c = 'Test';
        objIssue.Handyman__c = objContact.id;
        objIssue.Status__c = 'Raised';
        return objIssue;
    }
    
      /**
     *  Method Name:    createJobSheet
     *  Description:    Method to create Test Job Sheet
     *  Param: Case,Contact,Account
     *  Return: Job_Sheet__c
     */
    
      public static Job_Sheet__c createJobSheet(Account objAcc, Case objCase,Contact objContact) {
        
        Job_Sheet__c objJobSheet = new Job_Sheet__c();
        objJobSheet.Case__c = objCase.id;
        objJobSheet.Contractor_Name__c =objAcc.id;
        objJobSheet.Materials_Required__c = 'Test Materials Required';
        objJobSheet.Operative_Notes__c = 'Test Operative Notes';
        objJobSheet.Special_Instructions__c ='Test Special Instructions';
        objJobSheet.Handyman__c = objContact.id;
        return objJobSheet;
    }
    
     /**
     *  Method Name:    createPlotSpecification
     *  Description:    Method to create Test Plot_Specification__c
     *  Param: Plot__c
     *  Return: Plot_Specification__c
     */
  
     public static Plot_Specification__c createPlotSpecification(Plot__c  objPlot) {
        Plot_Specification__c objPlotSpec = new Plot_Specification__c();
        objPlotSpec.Plot__c=objPlot.Id;
        objPlotSpec.Specification__c='https://cs89.salesforce.com/sfc/#version?selectedDocumentId=0690E000000CaS';
        objPlotSpec.File_Name__c='new Test';
        return objPlotSpec;
    }
    
     /**
     *  Method Name:    createTask
     *  Description:    Method to create Test Task
     *  Param: Account, Contact
     *  Return: Task
     */
  
     public static Task createTask(Case objCase,Contact objContact) {
        Task objTask = new Task();
        objTask.WhatId=objCase.Id;
        objTask.WhoId=objContact.id;
        objTask.Subject='Has Been Allocated to One of Our Agents';
        return objTask;
    }
    
 }