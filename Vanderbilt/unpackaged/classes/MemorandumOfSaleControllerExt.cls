/**
 *  @Class Name:    ReservationagreementControllerExt 
 *  @Description:   This is a controller for ReservationagreementPage
 *  @Company: dQuotient
 *  CreatedDate: 23/11/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Pankaj Nayak        23/11/2016                  Orginal Version
 *
 */
public with sharing class MemorandumOfSaleControllerExt{
    
    private Opportunity objOpportunity;
    public String strCaseid;
    public String MemorandumOfSaleid;
    public Opportunity objMemorandumOfSale{get;set;}
    public boolean isDisabled {get; set;}
    public List<Opportunity> MemorandumOfSaleList{get;set;}
    public String pageMessage{get;set;}
    public boolean isSuccess {get; set;}
    public boolean isError {get; set;}
    public List<Issue__c>lstOfIssues{get;set;}
    public List<Case>lstOfCases{get;set;}
    
    
    /**
     *  Method Name: JOBSheetControllerExt 
     *  Description: This is the constructor for JOBSheetControllerExt class
     *  Param: ApexPages.StandardController
     *  Return: None
    */
    public MemorandumOfSaleControllerExt(ApexPages.StandardController stdController){
        
        lstOfIssues=new List<Issue__c>();
        lstOfCases=new List<Case>();
        MemorandumOfSaleid=Apexpages.currentPage().getParameters().get('id');
      //  system.debug('jobsheetid+++'+jobsheetid);
        MemorandumOfSaleid=String.escapeSingleQuotes(MemorandumOfSaleid);
        if(stdController != null){
            
            objMemorandumOfSale= (Opportunity)stdController.getRecord();
            MemorandumOfSaleList=new List<Opportunity>();
            MemorandumOfSaleList=[select id,Development_Marketing_Name__c,Plot_Number__c,Primary_Purchaser_Title_Surname__c,Primary_Purchaser_First_Name__c from opportunity where id=:MemorandumOfSaleid];
           // if(reservationLetterList!=null &&reservationLetterList.size()>0){                
             //   strCaseid=jobSheetList[0].Case__c;
              //  system.debug('strCaseid+++'+strCaseid);
           // }
        }   
    
    
    
} 
    public Pagereference attachPdf()
    {
        string sheetid;
         Attachment att = new Attachment();
         att.name='';
         att.name+='MoS-';
         if(MemorandumOfSaleList[0].Development_Marketing_Name__c!=null){
             att.name+=MemorandumOfSaleList[0].Development_Marketing_Name__c;
              att.name+='-';
         } 
         if(MemorandumOfSaleList[0].Plot_Number__c!=null){
             att.name+=MemorandumOfSaleList[0].Plot_Number__c;
             att.name+='-';
         }
         if(MemorandumOfSaleList[0].Primary_Purchaser_Title_Surname__c !=null){
             att.name+=MemorandumOfSaleList[0].Primary_Purchaser_Title_Surname__c ;
             att.name+='-';
         }
         if(MemorandumOfSaleList[0].Primary_Purchaser_First_Name__c !=null){
             att.name+=MemorandumOfSaleList[0].Primary_Purchaser_First_Name__c;
             att.name+='-';
         }
             att.name+=system.now().day();
             att.name+='-';
             att.name+=system.now().month();
             att.name+='-';
             att.name+=system.now().year();
             att.name+=' ';
             att.name+=system.now().hour();
             att.name+=':';
             att.name+=system.now().minute();
             
              att.name+='.pdf';
         PageReference invoicePage = Page.Memorandum_of_Sale;
         invoicePage .getParameters().put('id',MemorandumOfSaleid);
       
         if(Test.isRunningTest()){
            att.body=Blob.valueOf('UNIT.TEST');
         }
         else
            att.body = invoicePage.getContent();
         // Associate with project's record Id
         att.parentid = MemorandumOfSaleid;
         insert att;
         Pagereference pg=new Pagereference('/'+MemorandumOfSaleid) ;
        return pg;
    }
   
}