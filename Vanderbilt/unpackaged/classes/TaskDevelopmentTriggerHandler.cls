/**
 *  Class Name: TaskDevelopmentTriggerHandler 
 *  Description: This is a helper class for TaskDevelopmentTrigger on Task
 *  CreatedDate: 08/12/2016
 */
public with sharing class TaskDevelopmentTriggerHandler
{
     /**
     *  Method Name:    updatetaskFields 
     *  Description:    This is a method for populating Development__c field on Task from the related Lead
     *  Param:  list<Task> taskList
     *  Return: Nil
     */
    public static void updatetaskFields(list<task> taskList)
    {
        list<id> leadIdList = new list<id>();
        for(task e : taskList)
        {
            if(e.whoid != NULL)
                leadIdList.add(e.whoId);
        }
        if(leadIdList!=NULL && leadIdList.size()>0)
        {
            map<id,Lead> leadMap = new map<id,Lead>([Select id,Primary_Development_Interest__c from Lead where id IN :leadIdList LIMIT 999]);
            if(leadMap!=NULL && leadMap.values().size()>0)
            {
                for(task e:taskList)
                {
                    if(e.whoid!=NULL)
                    {
                        if(leadMap.containskey(e.whoId))
                            e.Development__c=leadMap.get(e.whoId).Primary_Development_Interest__c;
                    }
                }
            }
        }
    }
}