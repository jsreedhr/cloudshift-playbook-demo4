@isTest
private class Test_SiteStartTriggerFunctions {
    
static testMethod void siteStart(){
    
    UserRole r1 = new UserRole();
    r1.DeveloperName = 'MyCustomRole';
    r1.Name = 'My Role';

    insert r1;

    UserRole r2 = new UserRole();
    r2.DeveloperName = 'MyCustomRole1';
    r2.Name = 'My Role';

    insert r2; 
    
     User u1 = new User();
 Profile profileId = [SELECT Id FROM Profile WHERE Name = 'Standard User' LIMIT 1];
     u1.ProfileId = profileId.id;
     u1.LastName = 'last';
     u1.Email = 'puser000@amamama.com';
     u1.Username = 'puser000@amamama.com' + System.currentTimeMillis();
     u1.CompanyName = 'TEST';
     u1.Title = 'title';
     u1.Alias = 'alias';
     u1.TimeZoneSidKey = 'America/Los_Angeles';
     u1.EmailEncodingKey = 'UTF-8';
     u1.LanguageLocaleKey = 'en_US';
     u1.LocaleSidKey = 'en_US';
     u1.UserRoleId = r1.id;

        insert u1;
     system.debug('start my-------->' +u1);
     
     
      System.RunAs(u1)
     {
        Test.startTest();
         testfunc(u1.id);
         Test.stopTest();
     }
    
   
}
    @future
    public static void testfunc(Id userid)
    {
    Development__c dev = new Development__c ();
    dev.Marketing_Name__c = 'Court Road';
    dev.Active__c = TRUE;
    dev.Name = 'Abingdon';
    dev.COINS_Development_Code__c = '0009';
    dev.Street__c = '87 North Court Road';
    dev.City__c = 'Abingdon';
    dev.Country__c = 'United Kingdom';
    dev.Post_Code__c = 'OX14 1NN';
    dev.Number_of_Plots__c = 1;
    dev.Contracts_Manager__c = userid;
    dev.Site_Manager__c = userid;
    dev.Sales_Manager__c = userid;
     
    insert dev;
    
    Plot__c plot = new Plot__c();
    plot.Name = '1 Court Road';
    plot.Plot_Number__c = '1';
    plot.Development__c = dev.id;
    plot.Number_of_Beds__c = 3;
    plot.Open_Market_vs_Affordable__c = 'Affordable';
    plot.Size_Sq_Foot__c = 57;
    plot.House_Type__c = 'House';
    
    insert plot;    
    
    Development__c sc1 = [SELECT id, Contracts_Manager__c, Site_Manager__c FROM  Development__c where id=:dev.id ];
        system.debug('start my-------->' +sc1);
      
    Plot__c lst_plots = new Plot__c ();
    lst_plots.Name = '1 Aldridge Place';
    lst_plots.Plot_Number__c = '2';
    lst_plots.Development__c = dev.id;
    lst_plots.Sales_Status__c = 'Not Released';
    lst_plots.Build_Status__c = 'Foundations';
    lst_plots.Communal_Plot__c = TRUE;
    lst_plots.Customer_Demo_Date__c = System.today();
    lst_plots.Forecast_Completion_Quality_Audit_Date__c = System.today();
    lst_plots.Forecast_Completion_Date__c = System.today();
    lst_plots.Forecast_First_Fix_Completion_Date__c = System.today();
    lst_plots.Forecast_Pre_Decoration_Completion_Date__c = System.today();
    lst_plots.Forecast_Quality_Manager_Inspection_Date__c =System.today();
    lst_plots.Forecast_Sub_Structure_Completion_Date__c = system.today();
    lst_plots.Forecast_Super_Structure_Completion_Date__c = system.today();
    lst_plots.Construction_Comments__c = 'for test';
    lst_plots.Number_of_Beds__c = 3;
    lst_plots.Open_Market_vs_Affordable__c = 'Affordable';
    lst_plots.Size_Sq_Foot__c = 57;
    lst_plots.House_Type__c = 'House';
    //lst_plots.Contracts_Manager_ID__c=String.valueof(usr1);
    
    Insert lst_plots;
        
    Site_Start__c ssc = new Site_Start__c ();
    ssc.Name = 'Site Start: Battersea';
    ssc.Development__c = dev.id;
    ssc.Status__c = 'Completed';
    system.debug('propIdset');
    ssc.Sub_Structure_Created__c = true;
    ssc.Initial_Notice_accepted__c = TRUE;
    ssc.Site_Start_Authorisation_meeting_held__c = TRUE;
    ssc.X4see_notified_inspection_plan_agreed__c =TRUE;
    ssc.Pre_Development_meeting_held__c =TRUE;
    ssc.Design_freeze_has_been_reached__c =TRUE;
    ssc.Technical_packages_ready_for_procurement__c =TRUE;
    ssc.Material_SC_orders_in_line_w_programme__c =TRUE;
    ssc.Site_prepared_correctly__c =TRUE;
    ssc.Site_fully_available_for_groundworker__c =TRUE;
    ssc.Pre_commencement_notifications_made__c =TRUE;
    ssc.Pre_commencement_conditions_addressed__c =TRUE;
    ssc.Pre_Development_meeting_held__c = TRUE;
    ssc.Local_authority_advised_of_start__c =TRUE;
    ssc.Neighbours_advised_of_intended_start__c =TRUE;
    ssc.Schools_Police_notified__c =TRUE;
    ssc.Construction_Management_Plan_complete__c =TRUE;
    ssc.Site_Traffic_Management_Plan_complete__c =TRUE;
    ssc.Sales_plan_completed_issued_to_site__c =TRUE;
    ssc.Sales_Strategy_set_for_the_development__c =TRUE;
    ssc.Site_fully_available_for_groundworker__c = TRUE;
    ssc.Site_prepared_correctly__c = TRUE;
    ssc.Site_Start_Authorisation_meeting_held__c = TRUE;
    ssc.Areas_of_concern_highlighted_to_team__c =TRUE;
    ssc.Site_Traffic_Management_Plan_complete__c = TRUE;
    ssc.F10_expiry_date__c = System.today();
    ssc.Sub_Structure_Created__c = False;
    //ssc.Contracts_Manager_ID__c= u1.id;
    //ssc.Site_Manager_ID__c = u2.id;
        
    insert ssc;    
  
        Inspection__c WI = new Inspection__c();
                	
                    WI.Name = 'X';
                    WI.Status__c = 'Not Started';
                    WI.RecordTypeID = System.Label.Inspection_Weekly_Site_Manager_RT_Id;
                    WI.Development__c = dev.id;
                	WI.Contracts_Manager__c = dev.Contracts_Manager__c;
					WI.Site_Manager__c = dev.Site_Manager__c;
    Update ssc;
   
    
    }
}