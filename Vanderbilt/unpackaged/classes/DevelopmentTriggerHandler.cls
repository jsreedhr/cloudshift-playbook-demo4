/**
 *  Name: DevelopmentTriggerHandler
 *  Copyright © 2017 Salesforce
 *  Purpose: In this class all the methods used by the trigger will be called
 *
 *  History:
 *  Version         Author              Date               Detail
 *  1.0             Jeevraj Rao       14/11/2017           creation
 *  1.1             Jeevraj Rao       14/11/2017           Added CreateConsultants
 */
public class DevelopmentTriggerHandler extends TriggerHandler{

    /* context overrides */
    
    protected override void afterInsert() {
        DeveloperTriggerFunctions.CreateConsultants(Trigger.new);
        CustomLogging.save();
    }

    protected override void afterUpdate() {
        DeveloperTriggerFunctions.createTechnicalApprovals(Trigger.new, (Map<ID,Development__c>)Trigger.oldMap);
        CustomLogging.save();
    }
}