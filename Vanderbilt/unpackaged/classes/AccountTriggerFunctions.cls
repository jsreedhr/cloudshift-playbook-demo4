public without sharing class AccountTriggerFunctions {
    
    public static void createMaterialsInsert(List <Account> lst_accounts){
        
        Map <ID, String> map_AcctsMats = new Map <ID, String>();
        List <String> lst_materials = new List <String>();
        List <String> lst_materialids = new List <String>();
        List <Trade_Material__c> lst_trades = new List <Trade_Material__c>();
               
        for(Account a: lst_accounts){
            IF(a.Materials__c != null){
                lst_materials = a.Materials__c.split(';');
                for(String s: lst_materials){
                     lst_materialids.add(s + a.id);               
                }
            }
        }
        
        for(String s: lst_materialids){
            Trade_Material__c t = new Trade_Material__c();
            t.Account__c = s.right(18).substring(0, 18);
            t.Name = s.left(s.length() - 18);
            t.Type__c = 'Material';
            lst_trades.add(t);
        	}
        	insert lst_trades;   
    }
    
    public static void createMaterialsUpdate(List <Account> lst_accounts){
        
        //Declare the variables
        Set <Id> set_Accids = new Set <ID>();
        Map <ID, String> map_AcctsMats = new Map <ID, String>();
        List <String> lst_materials = new List <String>();
        List <String> lst_materialids = new List <String>();
        List <Trade_Material__c> lst_trades = new List <Trade_Material__c>();
        
        //loop over the accounts to get their ids
        for(Account a: lst_accounts){
            set_Accids.add(a.id);
        }
        
        //Now we have the ids, we can query the existing records to delete them before we create the new ones
        List <Trade_Material__c> lst_existingmats = new List <Trade_Material__c>([SELECT ID, Account__c FROM Trade_Material__c WHERE Account__c in: set_Accids]);
        
        IF(!lst_existingmats.isEmpty()){
            delete lst_existingmats;
        }
        
        for(Account a: lst_accounts){
            IF(a.Materials__c != null){
                lst_materials = a.Materials__c.split(';');
                for(String s: lst_materials){
                     lst_materialids.add(s + a.id);               
                }
            }
        }
              
        for(String s: lst_materialids){
            Trade_Material__c t = new Trade_Material__c();
            t.Account__c = s.right(18).substring(0, 18);
            t.Name = s.left(s.length() - 18);
            t.Type__c = 'Material';
            lst_trades.add(t);
        	}
		    insert lst_trades;     
    }
}