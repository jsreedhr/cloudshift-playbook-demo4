/**
    * @Name:        CustomLogging 
    * @Description: Class to be used to persist custom logs (via the Custom_Log__c object) in Salesforce
    *                   Example of an implementation :
    *                   CustomLogging.logMessage('className', 'methodName', message, CustomLogging.ERROR);
    *
    *                   Example of logging an error :
    *                   try {
    *                       // do something
    *                   }
    *                   catch (System.exception ex) {
    *                       CustomLogging.logMessage('className', 'methodName', message, CustomLogging.ERROR);
    *                   }
    *					
    *					When all events have been logged, always call save prior to execution. Logs
    *					will only exist for the extent of the session in which they are initiated
	*					
	*					CustomLogging.save();
    *
    * @author:      Jeevraj Rao
    * @version:     1.0
    * Change Log 
    *
    * Date          author          Change Description
    * -----------------------------------------------------------------------------------
    * 14/11/2017    Jeevraj Rao     Created Class
*/

public class CustomLogging {
    
    public static String INFO = 'INFO';
    public static String DEBUG = 'DEBUG';
    public static String WARNING = 'WARNING';
    public static String ERROR = 'ERROR';
    public static String FATAL = 'FATAL';
    
    public static List<Custom_Log__c> customLogCache = new list<Custom_Log__c>();
    
    public static void logMessage(String className, String methodName, String message, String Priority){
        Custom_Log__c newLogMessage = new Custom_Log__c(
            Class__c = className,
            Method__c = methodName,
            Message__c = message,
            Priority__c = Priority);
        try{
            Database.insert(newLogMessage);
        }catch(Exception ex){
            System.debug(
                'Failed to INSERT the [Apex Debug Log] ADL record. ' +
                'Error: ' + ex.getMessage()
            );
        }
    }
    
    // Persists logs for current transaction
    public static void save() {
        if (customLogCache != null && customLogCache.size() > 0) {
            try {
                upsert customLogCache;
            }
            catch(System.exception ex) {
                // to be decided
            }
        }
    }
    
}