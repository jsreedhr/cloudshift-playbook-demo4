/**
 *  @Class Name:    Task_Trigger_Utility 
 *  @Description:   This is a utility class for Task Trigger
 *  @Company: dQuotient
 *  CreatedDate: 30/09/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran				30/09/2016                  Orginal Version 
 * */
 
public with sharing class Lead_Trigger_Utility{

 
    /**
     *  Method Name:    SendInitialCaseResponse 
     *  Description:    Method to Modify Initial Response Field On Case Related to the task
     *  Param:  Trigger.oldMap, Trigger.newMap
     *  Return: None
     */
    public static void SendInitialCaseResponse( map<id,Task> mapNewTask){
		Set<Id> CaseIdSet=new Set<Id>();
		List<Case> CaseList=new List<Case>();
        for(Task objTaskNew: mapNewTask.values()){
           if(String.valueOf(objTaskNew.WhoId).substring(0,3)=='500' && objTaskNew.subject.contains('Has Been Allocated to One of Our Agents')){
			   CaseIdSet.add(objTaskNew.WhoId);
		   }
        }
		CaseList=[select id,Initial_Response_Email__c  from Case where Initial_Response_Email__c=false AND id in:CaseIdSet];
    		if(CaseList!=null && CaseList.size()>0){
    		    for(Case C :CaseList){
    			    C.Initial_Response_Email__c=true;
    		    }
            }
    }

}