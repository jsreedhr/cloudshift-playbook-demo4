public without sharing class ContractManagerHandler {
    public static void ToCompareContractManagerValues(Map<Id,Development__c> OldMap, Map<Id,Development__c> NewMap){
        
       Map<Id,Development__c> dev = new Map<Id,Development__c>([select id,name,Contracts_Manager__c, (SELECT id,name FROM Plots_Properties__r) from Development__c]);
      
     Map<Id,Plot__c> MapPlots= new Map<Id,Plot__c>([select id,Name,(select id,Contracts_Manager__c from Sub_Structures__r),(select id,Contracts_Manager__c  from Super_Structures__r ),(select id,Contracts_Manager__c  from First_Fixes__r ),(select id,Contracts_Manager__c from Pre_Decoration__r ),(select id,Contracts_Manager__c  from Completion_Quality_Audits__r ),(select id,Contracts_Manager__c from Quality_Manager_Inspections__r ) from Plot__c where Development__c IN :dev.keyset()]);
       system.debug('MapPlots---->'+MapPlots);
        List<Sub_Structure__c> ssb = new List<Sub_Structure__c> ();
        List<Super_Structure__c> sup = new List<Super_Structure__c> ();
        List<First_Fix__C> ffx = new List<First_Fix__C> ();
        List<Pre_Decoration__c> pdc = new List<Pre_Decoration__c> ();
        List<Completion_Quality_Audit__c> cqa= new List<Completion_Quality_Audit__c> ();
        List<Quality_Manager_Inspection__C> qmi = new List<Quality_Manager_Inspection__C> ();        
        
        for(id  devid : NewMap.keyset())
  {
   system.debug('dev--->'+dev);
    if((Oldmap.get(devid).Contracts_Manager__c!=Newmap.get(devid).Contracts_Manager__c) && Newmap.get(devid).Contracts_Manager__c != Null)

     {
                system.debug('devCurrentNow--->'+devid);
          system.debug('devCurrent--->'+dev.get(devid));

       Development__c getobj = dev.get(devid);
         
         
         if(getobj.Plots_Properties__r.size()>0) {
             for (Plot__c pc : getobj.Plots_Properties__r) {
               system.debug('Plot__c pc---->'+pc);
                 plot__c getpc = MapPlots.get(pc.id);
               system.debug('getpc---->'+getpc);
                 
                 if(getpc.Sub_Structures__r.size()>0) {
                     for(Sub_Structure__c ss : getpc.Sub_Structures__r ){
                         system.debug('getpc ss--->'+getpc);
                         
                         system.debug('Sub_Structure__c ss--->'+ss);
                         ss.Contracts_Manager__c = getobj.Contracts_Manager__c;
                         system.debug('ss.Contracts_Manager__c----->'+ss.Contracts_Manager__c );
                        ssb.add(ss); 
                     }
                 }
                 
                  if(getpc.Super_Structures__r.size()>0) {
                     for(Super_Structure__c su : getpc.Super_Structures__r ){
                         su.Contracts_Manager__c = getobj.Contracts_Manager__c;
                        sup.add(su); 
                     }
                 }
                 
             
              if(getpc.First_Fixes__r.size()>0) {
                 for(First_Fix__C ff : getpc.First_Fixes__r ){
                     ff.Contracts_Manager__c = getobj.Contracts_Manager__c;
                    ffx.add(ff);
                 }
              }
             
               if(getpc.Pre_Decoration__r.size()>0) {
                 for(Pre_Decoration__c pd : getpc.Pre_Decoration__r ){
                     pd.Contracts_Manager__c = getobj.Contracts_Manager__c;
                     pdc.add(pd);
                 }
              }  
             
             if(getpc.Completion_Quality_Audits__r.size()>0) {
                 for(Completion_Quality_Audit__c ca : getpc.Completion_Quality_Audits__r ){
                     ca.Contracts_Manager__c = getobj.Contracts_Manager__c;
                      cqa.add(ca);
                 }
              }  
             
             if(getpc.Quality_Manager_Inspections__r.size()>0) {
                 for(Quality_Manager_Inspection__C qm : getpc.Quality_Manager_Inspections__r ){
                     qm.Contracts_Manager__c = getobj.Contracts_Manager__c;
                      qmi.add(qm);
                 }
              } 
             
         }
      
      }
     }
      
        
}
        update ssb;
        update sup;
        update ffx;
        update pdc;
        update cqa;
        update qmi;
         
       
        
}
        public static void ToCompareContractManagerValuesBefore(Map<Id,Development__c> OldMap, Map<Id,Development__c> NewMap){
       
         list<Development__c> lstdev = [select Contracts_Manager_Old__c from Development__c where id in : NewMap.keyset()];
        
        for(Development__c devOldMap : lstdev){
        {
            
            devOldMap.Contracts_Manager_Old__c = oldmap.get(devOldMap.id).Contracts_Manager__c;
         }
        try{
              update lstdev;
                
            }catch(DmlException e){
            
                for(Development__c displayError : lstdev){
             string error = e.getMessage();
                
                displayError.addError(error);
                }
            }
           
            
        }
       
        }
    
}