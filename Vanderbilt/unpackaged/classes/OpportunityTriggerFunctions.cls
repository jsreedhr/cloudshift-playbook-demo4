public with sharing class OpportunityTriggerFunctions {
    
    public void updateStatus(list<opportunity> opp, Map<id,Opportunity> MapIdOpp){
                
        list<Id> lstPlotIds = new list<Id>();
        list<plot__c> lstPlots = new list<plot__c>();
        list<plot__c> UpdatedPlotRecords = new list<plot__C>();
        
        
        map<string,date>    mapForReservationDate = new map<string,date>();
        map<string,decimal> mapForReservationPrice = new map<string,decimal>();
        map<string,date>    mapForactualExchangeDate = new map<string,date>();
        map<string,date>    mapForactualcompletionDate = new map<string,date>();
        map<string,string>  mapForsalesStatus = new map<string,string>();
        map<string,date>    mapForForecastExchangeDate = new map<string,date>();
        map<string,date>    mapForForecastCompletionDate = new map<string,date>();
        map<string,string>  mapForBuildStatus = new map<string,string>();
        map<string,ID>      mapForPrimaryPurchaser = new map<string,string>();
        map<string,ID>      mapForSecondaryPurchaser = new map<string,string>();
        map<string,decimal> mapForReservationDeposit = new map<string,decimal>();
        map<string,decimal> mapForExchangeDeposit = new map<string,decimal>();
        
        List <Opportunity> lst_oppstoprogress = new List <Opportunity>();
        
		for(Opportunity newvalue: opp){
            for(Opportunity oldvalue: MapIdOpp.values()){
                IF(newvalue.id == oldvalue.id){
                    IF((oldvalue.StageName != 'Cancelled' && newvalue.StageName != 'Cancelled') || (oldvalue.StageName != newvalue.StageName)){
                        lst_oppstoprogress.add(newvalue);
                    }
                }
            }
        }
        
        List<Opportunity> NewOpp = [Select id,stageName,Plot_Property__c,Anticipated_Exchange_Date__c,CloseDate,amount,Actual_Exchange_Date__c,Actual_Completion_Date__c,Primary_Purchaser__r.accountId,Secondary_Purchaser__r.account.Id,Secondary_Purchaser__c, Reservation_Fee__c, Exchange_Deposit__c, Reservation_Date__c from opportunity where id in:lst_oppstoprogress];
        List<Incentives__c> lst_incentives = [Select id, Sales_Progression__c from Incentives__c where Sales_Progression__r.id in:lst_oppstoprogress];
        List<Incentives__c> lst_incentivestodelete = new List <Incentives__c>();
        //List<Extra__c> lst_extrastodelete = new List <Extra__c>();
        
        for(opportunity op: NewOpp){        
            
            lstPlotIds.add(op.Plot_Property__c);
            
            if(op.stageName == 'Reserved - Approved'){
                mapForsalesStatus.put(op.Plot_Property__c,'Reserved');
                mapForReservationDate.put(op.Plot_Property__c, op.Reservation_Date__c);
                mapForForecastExchangeDate.put(op.Plot_Property__c,op.Anticipated_Exchange_Date__c);
                mapForForecastCompletionDate.put(op.Plot_Property__c,op.CloseDate);
                mapForReservationPrice.put(op.Plot_Property__c,op.amount);
                mapForReservationDeposit.put(op.Plot_Property__c,op.Reservation_Fee__c);
                mapForExchangeDeposit.put(op.Plot_Property__c,op.Exchange_Deposit__c);
            }
            
            else if(op.stageName == 'Exchanged'){
                mapForsalesStatus.put(op.Plot_Property__c,'Exchanged');
                mapForReservationDate.put(op.Plot_Property__c, op.Reservation_Date__c);
                mapForReservationPrice.put(op.Plot_Property__c,op.amount);
                mapForReservationDeposit.put(op.Plot_Property__c,op.Reservation_Fee__c);
                mapForForecastExchangeDate.put(op.Plot_Property__c,op.Anticipated_Exchange_Date__c);
                mapForForecastCompletionDate.put(op.Plot_Property__c,op.CloseDate);
                mapForactualExchangeDate.put(op.Plot_Property__c,op.Actual_Exchange_Date__c); 
                mapForExchangeDeposit.put(op.Plot_Property__c,op.Exchange_Deposit__c);
            }
            
            else if(op.stageName == 'Completed'){
                mapForsalesStatus.put(op.Plot_Property__c,'Completed');
                mapForReservationDate.put(op.Plot_Property__c, op.Reservation_Date__c);
                mapForReservationPrice.put(op.Plot_Property__c,op.amount);
                mapForForecastExchangeDate.put(op.Plot_Property__c,op.Anticipated_Exchange_Date__c);
                mapForForecastCompletionDate.put(op.Plot_Property__c,op.CloseDate);
                mapForactualExchangeDate.put(op.Plot_Property__c,op.Actual_Exchange_Date__c);
                mapForactualcompletionDate.put(op.Plot_Property__c,op.Actual_Completion_Date__c);
                mapForReservationDeposit.put(op.Plot_Property__c,op.Reservation_Fee__c);
                mapForBuildStatus.put(op.Plot_Property__c,'Sales Ownership');
                mapForPrimaryPurchaser.put(op.Plot_Property__c, op.Primary_Purchaser__r.accountId);
                mapForSecondaryPurchaser.put(op.Plot_Property__c, op.Secondary_Purchaser__c);
                mapForExchangeDeposit.put(op.Plot_Property__c,op.Exchange_Deposit__c);
            }
            
            else if(op.stageName == 'Cancelled'){
                mapForsalesStatus.put(op.Plot_Property__c,'Available to Reserve');
                mapForReservationPrice.put(op.Plot_Property__c,null);                
                mapForactualExchangeDate.put(op.Plot_Property__c,null); 
                mapForactualcompletionDate.put(op.Plot_Property__c,null); 
                mapForForecastExchangeDate.put(op.Plot_Property__c,null); 
                mapForForecastCompletionDate.put(op.Plot_Property__c,null);
                mapForReservationDeposit.put(op.Plot_Property__c, null);
                mapForPrimaryPurchaser.put(op.Plot_Property__c, null);
                mapForSecondaryPurchaser.put(op.Plot_Property__c, null);
                mapForExchangeDeposit.put(op.Plot_Property__c, null);
                mapForReservationDate.put(op.Plot_Property__c, null);
                
                for(Incentives__c i: lst_Incentives){
                    IF(i.Sales_Progression__c == op.Id){
                        lst_incentivestodelete.add(i); 
                    }
                }
            }
            
        }
        
        
        
        system.debug('aaaaaaaaaaaaaaaaa'+lstPlotIds);
        system.debug('bbbbbbbbbbbbbbbbb'+mapForReservationPrice);
        system.debug('ccccccccccccccccc'+mapForsalesStatus);
        
        
        
        lstPlots = [select id,name,Reservation_Price__c,Exchange_Date__c,Completion_Date__c,Sales_Status__c,Forecast_Completion_Date__c,Forecast_Exchange_Date__c, Build_Status__c, Incumbent__c, Sold_To__c, Additional_Incumbent__c, Additional_Sold_To__c, Reservation_Deposit__c from plot__c where id =:lstPlotIds];       
        system.debug('ddddddddddddddddd'+lstPlots); 
        
        if(lstPlots.size()>0){
            
            //List <Extra__c> lst_extras = [SELECT ID, Plot_Property__c FROM Extra__c WHERE Plot_Property__r.id IN: lstPlots];
            
            for(plot__c p: lstPlots){
                if(mapForReservationPrice.containskey(p.id)){
                    p.Reservation_Price__c = mapForReservationPrice.get(p.id);
                }
                
                if(mapForactualExchangeDate.containskey(p.id)){
                    p.Exchange_Date__c = mapForactualExchangeDate.get(p.id);
                }
                
                if(mapForactualcompletionDate.containskey(p.id)){
                    p.Completion_Date__c = mapForactualcompletionDate.get(p.id);
                }
                
                if(mapForsalesStatus.containskey(p.id)){
                    p.Sales_Status__c = mapForsalesStatus.get(p.id);
                }
                
                if(mapForReservationDate.containskey(p.id)){
                    p.Reservation_Date__c = mapForReservationDate.get(p.id);
                }
                
                if(mapForForecastExchangeDate.containskey(p.id)){
                    p.Forecast_Exchange_Date__c = mapForForecastExchangeDate.get(p.id);
                }
                
                if(mapForForecastCompletionDate.containskey(p.id)){
                    p.Forecast_Completion_Date__c = mapForForecastCompletionDate.get(p.id);
                }
                
                if(mapForBuildStatus.containskey(p.id)){
                    p.Build_Status__c = mapForBuildStatus.get(p.id);
                }
                
                if(mapForPrimaryPurchaser.containskey(p.id)){
                    p.Sold_To__c= mapForPrimaryPurchaser.get(p.id);
                    p.Incumbent__c= mapForPrimaryPurchaser.get(p.id);
                    
                }
                
                if(mapForReservationDeposit.containskey(p.id)){
                    p.Reservation_Deposit__c = mapForReservationDeposit.get(p.id);
                    
                }
                
                if(mapForExchangeDeposit.containskey(p.id)){
                    p.Exchange_Deposit__c = mapForExchangeDeposit.get(p.id);
                    
                }
                
                if(mapForSecondaryPurchaser.containskey(p.id)){
                    p.Additional_Sold_To__c= mapForSecondaryPurchaser.get(p.id);
                    p.Additional_Incumbent__c= mapForSecondaryPurchaser.get(p.id);
                }                                           
                
                UpdatedPlotRecords.add(p); 
                
            }
            
            if(UpdatedPlotRecords.size()>0){
                
                try{
                    update UpdatedPlotRecords; 
                }
                
                catch (DMLException e) {
                    for (opportunity opr : opp) {
                        opr.addError(e.getDmlMessage(0));
                    }
                }
                
            }
            
            //If the opportunity is cancelled and there are incentives recorded against the opportunity, they should be deleted
            if(lst_incentivestodelete.size()>0){
                
                delete lst_incentivestodelete; 
                
            }
            
            //If the opportunity is cancelled and there are extras recorded against the plot, they should be deleted
            /*IF(!lst_extras.isEmpty()){
                for(Opportunity opr: opp){
                    for(Plot__c p : lstPlots){
                        for(Extra__c ext: lst_extras){
                            IF(opr.StageName == 'Cancelled')
                                IF(ext.Plot_Property__c == p.id)
                                lst_extrastodelete.add(ext);
                        }
                    }  
                }
                delete lst_extrastodelete; 
            }
            */
        }
    }
    
    public void MarketingOptOut (list <opportunity> lst_opps){
        
        Set <ID> set_business_contacts = new Set <ID>();
        Set <ID> set_person_contacts = new Set <ID>();
        
        //Check whether the primary purchaser is blank and if it is a person account
        for(Opportunity o: lst_opps){
            IF(o.Marketing_Opt_Out__c == TRUE && o.Primary_Purchaser__c != null && o.Primary_Purchaser__r.Ispersonaccount == true){
                set_person_contacts.add(o.Primary_Purchaser__c);
            }
        }
        
        //Check whether the secondary purchaser is blank and if it is a person account
        for(Opportunity o: lst_opps){
            IF(o.Marketing_Opt_Out__c == TRUE && o.Secondary_Purchaser__c != null && o.Secondary_Purchaser__r.Ispersonaccount == true){
                set_person_contacts.add(o.Secondary_Purchaser__c);
            }
        }
        
        //Check whether the primary purchaser is blank and if it is a business contact 
        for(Opportunity o: lst_opps){
            IF(o.Marketing_Opt_Out__c == TRUE && o.Primary_Purchaser__c != null && o.Primary_Purchaser__r.Ispersonaccount == false){
                set_business_contacts.add(o.Primary_Purchaser__c);
            }
        }
        
        //Check whether the secondary purchaser is blank and if it is a business contact
        for(Opportunity o: lst_opps){
            IF(o.Secondary_Purchaser__c != null && o.Secondary_Purchaser__r.Ispersonaccount == false){
                set_business_contacts.add(o.Secondary_Purchaser__c);
            }
        }
        
        IF(!set_business_contacts.isEmpty()){
            //For the business contacts, set the opt out to true
            List <Contact> lst_business_contacts = [SELECT ID, HasOptedOutOfEmail FROM Contact WHERE ID IN: set_business_contacts];
            for (Contact c: lst_business_contacts){
                c.HasOptedOutOfEmail = TRUE;            
            }
            
            try{
                    update lst_business_contacts; 
                }
                
                catch (DMLException e) {
                    for (opportunity opr : lst_opps) {
                        opr.addError(e.getDmlMessage(0));
                    }
                }
        }
        
        IF(!set_person_contacts.isEmpty()){
            
            List <Contact> lst_personcontacts = [SELECT ID, AccountID FROM Contact WHERE ID IN: set_person_contacts];
            
            //For the person accounts, set the opt out to true
            Set<ID> set_personaccountids = new Set <ID>();
            for (Contact c: lst_personcontacts){
                set_personaccountids.add(c.accountID);
            }
            
            List <Account> lst_personaccts = [SELECT ID, PersonHasOptedOutOfEmail FROM Account WHERE ID IN: set_personaccountids];
            
            for(Account a: lst_personaccts){
                a.PersonHasOptedOutOfEmail = TRUE;
            }
            
            try{
                    Update lst_personaccts;
                }
                
                catch (DMLException e) {
                    for (opportunity opr : lst_opps) {
                        opr.addError(e.getDmlMessage(0));
                    }
                }
        }
    }
}