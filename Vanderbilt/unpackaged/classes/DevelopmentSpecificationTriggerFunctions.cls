public class DevelopmentSpecificationTriggerFunctions {
    
    public static void getspecificationname(List <Development_Specification__c> lst_devspec){
        
        Set <ID> set_specids = new Set <ID>();
        string thestring;
        
        for(Development_Specification__c obj_devspec: lst_devspec){
            IF(obj_devspec.Specification__c != Null){
                thestring = obj_devspec.Specification__c;
                set_specids.add(thestring.right(15));                                                 
            }            
        }
        
        IF(!set_specids.isEmpty()){
            List <ContentDocument> lst_contentdocs = new List <ContentDocument>([SELECT ID, Title FROM ContentDocument WHERE ID in: set_specids]); 
            IF(!lst_contentdocs.isEmpty()){
                for(Development_Specification__c obj_devspec: lst_devspec){
                    for(ID obj_spec: set_specids){
                        for(ContentDocument obj_contentdoc: lst_contentdocs){
                            IF(obj_spec == obj_contentdoc.id){
                                obj_devspec.File_Name__c = obj_contentdoc.Title;                  
                            }
                        }  
                    }                
                }        
            }                
        }
    }
}