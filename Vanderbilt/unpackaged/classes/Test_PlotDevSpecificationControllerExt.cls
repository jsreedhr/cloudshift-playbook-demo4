@isTest
/**
 *  @Class Name:    Test_PlotDevSpecificationControllerExt 
 *  @Description:   This is a test class for PlotDevSpecificationControllerExt
 *  @Company: dQuotient
 *  CreatedDate: 19/10/2016
 *
 *  Modification Log
 *  -----------------------------------------------------------
 *  Developer           Modification Date           Comments
 *  -----------------------------------------------------------
 *  Kiran George              19/10/2016                  Orginal Version
 *  Nitha T S                 01/08/2017                  Last Modified
 */
private class Test_PlotDevSpecificationControllerExt {

	/**
     *  Method Name:    positiveTestCase 
     *  Description:    Method to Test PlotDevSpecificationControllerExt 
     *  Param:          None
     *  Return:         None
     */
	private static testMethod void positiveTestCase() {
	    //createBusinessAccount
	    Account objAccount=TestData.createBusinessAccount();
	    insert objAccount;
	    Account objAccount2=TestData.createBusinessAccount();
	    objAccount2.Name='new';
	    insert objAccount2;
	    //createContactUnderAccount
	    Contact objContact=TestData.createContact(objAccount);
	    insert objContact;
	    //createDevelopment
	    Development__c objDev=TestData.createDevelopment(objContact);
	    insert objDev;
	    Development_Specification__c devspec=TestData.createDevSpecification(objDev);
	    insert devspec;
	    //Create Plot
        Plot__c objPlot=TestData.createPlot(objAccount,objDev);
        insert objPlot;
        ApexPages.currentPage().getParameters().put('id',objPlot.id);
        
        PlotDevSpecificationControllerExt ctrlr=new PlotDevSpecificationControllerExt();
        ctrlr.processSelected();
        system.assert(objPlot.id!=null);
	}

}