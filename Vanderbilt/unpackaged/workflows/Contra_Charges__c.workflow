<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contra_Charge_Creation_Alert</fullName>
        <description>Contra Charge - Creation Alert</description>
        <protected>false</protected>
        <recipients>
            <field>Surveyor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Commercial_Team/Contra_Charge_Creation_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Contra_Charge_Populate_Surveyor_Email</fullName>
        <field>Surveyor_Email__c</field>
        <formula>Development__r.Surveyor__r.Email</formula>
        <name>Contra Charge - Populate Surveyor Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Contra Charge - Email notification</fullName>
        <actions>
            <name>Contra_Charge_Creation_Alert</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>An email alert should be sent to the Development Surveyor Email Alert to Development Surveyor when created</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Contra Charge - Populate Surveyor Email</fullName>
        <actions>
            <name>Contra_Charge_Populate_Surveyor_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contra_Charges__c.Surveyor_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
