<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Lead_New_Website_Lead_Notification_to_Queue_Members</fullName>
        <description>Lead - New Website Lead Notification to Queue Members</description>
        <protected>false</protected>
        <recipients>
            <recipient>james.geake@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.slingerland@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Lead_Templates/Lead_New_Website_Lead_Notification_to_Queue_Members</template>
    </alerts>
    <alerts>
        <fullName>Lead_Owner_Change</fullName>
        <description>Lead Owner Change</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Lead_Templates/Lead_Lead_Assignment_Notification_to_Sales_Rep</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_Alert_To_Lear_Record_Owner</fullName>
        <ccEmails>kristina@cloudshiftgroup.com</ccEmails>
        <description>Send Email Alert To Lear Record Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>maryann.blackmore@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.slingerland@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sales@vanderbilthomes.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Progression_Templates/Hot_Lead_Notification_for_Sales_Consultant</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_Alert_To_Notify_about_Completed_Survey</fullName>
        <description>Send Email Alert To Notify about Completed Survey</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>david@cloudshiftgroup.com.vb.live</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>maryann.blackmore@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sarah.slingerland@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderAddress>sales@vanderbilthomes.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Progression_Templates/Survey_Completed_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Lead_Help_to_Buy_No</fullName>
        <field>Help_to_Buy__c</field>
        <literalValue>0</literalValue>
        <name>Lead - Help to Buy &apos;No&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Help_to_Buy_Yes</fullName>
        <field>Help_to_Buy__c</field>
        <literalValue>1</literalValue>
        <name>Lead - Help to Buy &apos;Yes&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Mortgage_Required_No</fullName>
        <field>Mortgage_Required__c</field>
        <literalValue>0</literalValue>
        <name>Lead - Mortgage Required &apos;No&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Mortgage_Required_Yes</fullName>
        <field>Mortgage_Required__c</field>
        <literalValue>1</literalValue>
        <name>Lead - Mortgage Required &apos;Yes&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Updated_Hot_Lead_Alert_Sent_Date</fullName>
        <description>Updated &quot;Hot Lead Alert Sent Date&quot; with the date when email is sent to a Sales Consultant.</description>
        <field>Hot_Lead_Alert_Last_Sent_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Updated &quot;Hot Lead Alert Sent Date&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>hidden_seg_populate</fullName>
        <description>If Segment is populated on the lead this field must also be populated to allow the lead field to be mapped to both contact and opportunity.</description>
        <field>Hidden_Segment__c</field>
        <formula>TEXT(Segment__c)</formula>
        <name>hidden seg populate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Hot Lead Notification for Sales Consultant</fullName>
        <actions>
            <name>Send_Email_Alert_To_Lear_Record_Owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Updated_Hot_Lead_Alert_Sent_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Hot_Lead_Alert_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Hot_Lead_Alert_Last_Sent_Date__c</field>
            <operation>notEqual</operation>
            <value>LAST 7 DAYS</value>
        </criteriaItems>
        <description>This is powered by Pardot.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Help to Buy %27No%27</fullName>
        <actions>
            <name>Lead_Help_to_Buy_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Help_to_Buy_Picklist__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Help to Buy %27Yes%27</fullName>
        <actions>
            <name>Lead_Help_to_Buy_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Help_to_Buy_Picklist__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Mortgage Required %27No%27</fullName>
        <actions>
            <name>Lead_Mortgage_Required_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Mortgage_Required_Picklist__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Mortgage Required %27Yes%27</fullName>
        <actions>
            <name>Lead_Mortgage_Required_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Mortgage_Required_Picklist__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Lead - Website Leads Queue Notification</fullName>
        <actions>
            <name>Lead_New_Website_Lead_Notification_to_Queue_Members</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Website - Vanderbilt</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Owner Notification Email</fullName>
        <actions>
            <name>Lead_Owner_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notification to the new Lead Owner every time the field is changed.</description>
        <formula>ISCHANGED( OwnerId )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Survey Completed Notification</fullName>
        <actions>
            <name>Send_Email_Alert_To_Notify_about_Completed_Survey</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Survey_Completed_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>This WF is sending out automated email to notify internal users (Record Owner, Head of Sales and Sales manager) about completed Survey.This is powered by Pardot.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>hidden seg populate</fullName>
        <actions>
            <name>hidden_seg_populate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Segment__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
