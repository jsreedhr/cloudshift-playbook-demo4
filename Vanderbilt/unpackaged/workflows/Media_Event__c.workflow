<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Media_Event_Reminder_Email_Alert</fullName>
        <description>Reminder email to be sent to Sales Team one week before copy deadline date</description>
        <protected>false</protected>
        <recipients>
            <field>Responsible_Sales_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Media_Event_Templates/Media_Event_Reminder_Email</template>
    </alerts>
</Workflow>
