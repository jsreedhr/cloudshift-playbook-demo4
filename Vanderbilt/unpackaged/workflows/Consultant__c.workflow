<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Consultant_Update_Appointment_Status</fullName>
        <field>Appointment_Status__c</field>
        <literalValue>Appointed</literalValue>
        <name>Consultant - Update Appointment Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Consultant_Update_Completed_Date_Today</fullName>
        <field>Date_Completed__c</field>
        <formula>TODAY()</formula>
        <name>Consultant - Update Completed Date Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Appointed_Date</fullName>
        <field>Appointment_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Appointed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Consultant - Update Date Completed</fullName>
        <actions>
            <name>Consultant_Update_Completed_Date_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Consultant__c.Date_Completed__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Consultant__c.Delivery_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Updates the Completed Date if blank and the Status is changed to Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Consultant - Update Status on Consultant Appointment</fullName>
        <actions>
            <name>Consultant_Update_Appointment_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Appointed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updates the Appointment Status when a Consultant Account is added to the record</description>
        <formula>NOT(ISBLANK(  Consultant_Account__c  ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
