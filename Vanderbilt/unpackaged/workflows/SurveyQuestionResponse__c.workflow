<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Response_Text</fullName>
        <field>Response_Text__c</field>
        <formula>Response__c</formula>
        <name>Response (Text)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Survey Question Response - Convert Response Long Text to Text</fullName>
        <actions>
            <name>Response_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to convert long text survey response to text from long text.</description>
        <formula>LEN(Response__c ) &lt; 256</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
