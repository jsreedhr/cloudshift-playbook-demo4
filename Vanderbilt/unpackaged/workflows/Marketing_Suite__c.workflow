<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>City_Update</fullName>
        <field>City__c</field>
        <formula>Development__r.City__c</formula>
        <name>City Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Development_Adress_Copy_Street</fullName>
        <field>Street__c</field>
        <formula>Development__r.Street__c</formula>
        <name>Development_Adress_Copy_Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>State_Province_Update</fullName>
        <field>State_Province__c</field>
        <formula>Development__r.State_Province__c</formula>
        <name>State/Province Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Street_Update</fullName>
        <field>Street__c</field>
        <formula>Development__r.Street__c</formula>
        <name>Street Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Zip_Update</fullName>
        <field>Zip_Postal_Code__c</field>
        <formula>Development__r.Post_Code__c</formula>
        <name>Zip Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Address Populate From Development</fullName>
        <actions>
            <name>City_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>State_Province_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Street_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Zip_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>On creation of a Marketing Suites/Offices the address of the development is carried through</description>
        <formula>( Development__r.Street__c &lt;&gt; &quot; &quot;)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
