<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Send_Completion_Quality_Audit_REJECTED_Notification_Email_to_Owner</fullName>
        <description>Email - Send Completion/Quality Audit REJECTED Notification Email to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Completion_Quality_Audits_Approval_Process_Rejection</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_completion_quality_audit_Approval_APPROVED_Email_to_Owner</fullName>
        <description>Email - Send completion/quality audit Approval APPROVED Email to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Completion_Quality_Audits_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_completion_quality_audit_Approval_REQUEST_Email_to_Owner</fullName>
        <description>Email_Send_completion_quality_audit_Approval_REQUEST_Email_to_Owner</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Completion_Quality_Audits_Approval_Process_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Clear_Completion_Date</fullName>
        <field>Date_Completed__c</field>
        <name>Clear Completion Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completion_Quality_Audit_Set_Naming_Co</fullName>
        <field>Name</field>
        <formula>&quot;Completion/QA:&quot; &amp; &quot; &quot; &amp; Plot_Property__r.Plot_Number__c &amp; &quot; - &quot; &amp; 
Plot_Property__r.Development__r.Name &amp; &quot; &quot; &amp; &quot;(&quot; &amp; 
Plot_Property__r.Development__r.City__c &amp; &quot;, &quot; &amp; Plot_Property__r.Development__r.State_Province__c &amp; &quot;)&quot;</formula>
        <name>Completion/Quality Audit - Set Naming Co</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Completion_Quality_QA_Completed_Date</fullName>
        <description>This update populates the Date Completed when the Status is set to Completed</description>
        <field>Date_Completed__c</field>
        <formula>today()</formula>
        <name>Completion/Qual-Aud. - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_Approval_Tick</fullName>
        <field>In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>In Approval Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_Date</fullName>
        <field>Date_Approved__c</field>
        <formula>TODAY()</formula>
        <name>Set Approved Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved_Date_to_Null</fullName>
        <field>Date_Approved__c</field>
        <name>Set Approved Date to Null</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Completed_Date</fullName>
        <field>Date_Completed__c</field>
        <formula>IF(NOT(ISBLANK(Date_Completed__c)), Date_Completed__c, TODAY())</formula>
        <name>Set Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Approved_Flag</fullName>
        <field>Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tick Approved Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_In_Approval_Flag</fullName>
        <field>In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Tick In Approval Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_Approved</fullName>
        <field>Approved__c</field>
        <literalValue>0</literalValue>
        <name>Untick Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_Approval</fullName>
        <field>In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_In_Approval_Flag</fullName>
        <field>In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Untick In Approval Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Completion%2FQuality Audit - Populate Date Completed</fullName>
        <actions>
            <name>Completion_Quality_QA_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the Date Completed field when the status field is set to &quot;Completed&quot;</description>
        <formula>ISBLANK( Date_Completed__c )   &amp;&amp;   ISPICKVAL( Status__c, &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Completion%2FQuality Audit - Set Naming Convention</fullName>
        <actions>
            <name>Completion_Quality_Audit_Set_Naming_Co</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set up a standard naming convention for the Completion/Quality Audit object.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
