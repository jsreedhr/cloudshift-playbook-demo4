<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Survey_Taken_Sales_Completion_Feedback</fullName>
        <description>On Sales Completion Feedback Survey Taken an email alert gets send out to the Sales Manager and Head of Sales</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>Head_of_Sales</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Progression_Templates/Survey_Sales_Survey_Completed</template>
    </alerts>
    <rules>
        <fullName>Survey - Sales Survey Completed</fullName>
        <actions>
            <name>Survey_Taken_Sales_Completion_Feedback</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>AND(
 Survey__r.Id = &quot;a0o0Y000000gl4A&quot;,
 NOT(ISBLANK( Sales_Manager_Email__c )))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
