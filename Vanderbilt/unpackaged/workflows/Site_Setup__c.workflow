<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Send_Site_Setup_Approval_APPROVED_Email_to_Owner</fullName>
        <description>Email - Send Site Setup Approval APPROVED Email to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Site_Setup_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Send_Site_Setup_REJECTED_Notification_to_Owner</fullName>
        <description>Send Site Setup REJECTED Notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Site_Setup_Approval_Process_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Site_Setup_Completed_Date</fullName>
        <description>This update populates the Date Completed when the Status is set to Completed</description>
        <field>Date_Completed__c</field>
        <formula>today()</formula>
        <name>Site Setup - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Setup_Set_Naming_Convention</fullName>
        <field>Name</field>
        <formula>&quot;Development Setup: &quot; &amp; Development__r.Name</formula>
        <name>Site Setup - Set Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Site Setup - Naming Convention</fullName>
        <actions>
            <name>Site_Setup_Set_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set up a standard naming convention for the development setup object.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Site Setup - Populate Date Completed</fullName>
        <actions>
            <name>Site_Setup_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the Date Completed field when the status field is set to &quot;Completed&quot;</description>
        <formula>ISBLANK( Date_Completed__c )   &amp;&amp;   ISPICKVAL( Status__c, &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
