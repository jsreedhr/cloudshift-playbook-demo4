<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Account_Sub_Contractor_H_S_Email_Reminder</fullName>
        <description>Account - Sub-Contractor H&amp;S Email Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Sub_Contractor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>healthandsafety@vanderbilthomes.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Account_Email_Templates/Account_Email_to_sub_contractor_with_insurance_health_safety_expiring_soon</template>
    </alerts>
    <alerts>
        <fullName>Account_Sub_Contractor_H_S_Email_Reminder_No_Subby_Email</fullName>
        <ccEmails>joanne.keane@vanderbilthomes.co.uk</ccEmails>
        <description>Account - Sub-Contractor H&amp;S Email Reminder (No Subby Email)</description>
        <protected>false</protected>
        <senderAddress>healthandsafety@vanderbilthomes.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Account_Email_Templates/Account_Email_reminder_to_Joane_due_to_expiring_H_S_No_Subby_Email</template>
    </alerts>
    <alerts>
        <fullName>Indemnity_Insurance_End_Date_Expires_in_1_Month</fullName>
        <description>Indemnity Insurance End Date Expires in 1 Month - To be set to Paul</description>
        <protected>false</protected>
        <recipients>
            <recipient>paul.mckeown@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Technical_Team/Account_Indemnity_Insurance_Warming_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Subby_Email_Stamp</fullName>
        <field>Sub_Contractor_Email_Reminder_Sent__c</field>
        <formula>TODAY()</formula>
        <name>Account - Subby Email Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account - Indemnity Insurance  Warming</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Indemnity_Insurance_End_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Indemnity_Insurance_End_Date_Expires_in_1_Month</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.Indemnity_Insurance_End_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account - Sub-Contractor H%26S Email Reminder</fullName>
        <active>true</active>
        <description>Automatically send an email to sub-contractors 15 days prior to their next insurance expiry date requesting they provide updated information/certificates (H&amp;S Policy/SMAS/CHAS/insurances)</description>
        <formula>AND( NOT(ISBLANK( Next_H_S_Insurance_Expiry_Date__c)), Next_H_S_Insurance_Expiry_Date__c &gt;= TODAY()+15, NOT(ISBLANK( Sub_Contractor_Email__c )) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Sub_Contractor_H_S_Email_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Account_Subby_Email_Stamp</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Next_H_S_Insurance_Expiry_Date__c</offsetFromField>
            <timeLength>-15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account - Sub-Contractor H%26S Email Reminder %28No Subby Email%29</fullName>
        <active>true</active>
        <description>Automatically send an email to joanne.keane@vanderbilthomes.co.uk to notify her to get in touch via phone (Automatically send an email to sub-contractors 15 days prior to their next insurance expiry date requesting they provide updated information)</description>
        <formula>AND( NOT(ISBLANK( Next_H_S_Insurance_Expiry_Date__c)), Next_H_S_Insurance_Expiry_Date__c &gt;= TODAY()+15, ISBLANK( Sub_Contractor_Email__c ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Sub_Contractor_H_S_Email_Reminder_No_Subby_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Account.Next_H_S_Insurance_Expiry_Date__c</offsetFromField>
            <timeLength>-15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
