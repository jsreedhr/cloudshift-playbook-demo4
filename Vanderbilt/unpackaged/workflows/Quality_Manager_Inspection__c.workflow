<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Qual_Man_Insp_Completed_Date</fullName>
        <description>This update populates the Date Completed when the Status is set to Completed</description>
        <field>Date_Completed__c</field>
        <formula>today()</formula>
        <name>Qual. Man. Insp. - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quality_Manager_Inspection_Set_Naming</fullName>
        <field>Name</field>
        <formula>&quot;Quality Mgmt Insp:&quot; &amp; &quot; &quot; &amp; Plot_Property__r.Plot_Number__c &amp; &quot; - &quot; &amp; 
Plot_Property__r.Development__r.Name &amp; &quot; &quot; &amp; &quot;(&quot; &amp; 
Plot_Property__r.Development__r.City__c &amp; &quot;, &quot; &amp; Plot_Property__r.Development__r.State_Province__c &amp; &quot;)&quot;</formula>
        <name>Quality Manager Inspection - Set Naming</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Quality Manager Inspection - Populate Date Completed</fullName>
        <actions>
            <name>Qual_Man_Insp_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the Date Completed field when the status field is set to &quot;Completed&quot;</description>
        <formula>ISBLANK( Date_Completed__c )   &amp;&amp;   ISPICKVAL( Status__c, &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quality Manager Inspection - Set Naming Convention</fullName>
        <actions>
            <name>Quality_Manager_Inspection_Set_Naming</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set up a standard naming convention for the Quality Manager Inspection object.</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
