<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Send_Completion_Quality_Audit_Completion_Email_Alert_to_HoS_Sales_Manager</fullName>
        <description>Email - Send Completion/Quality Audit Completion Email Alert to HoS &amp; Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>richard.bettridge@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Completion_of_Completion_Quality_Audit_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_First_Fix_Completion_Email_Alert_to_HoS_Sales_Manager</fullName>
        <description>Email - Send First Fix Completion Email Alert to HoS &amp; Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Completion_of_First_Fix_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Pre_Decoration_Completion_Email_Alert_to_HoS_Sales_Manager</fullName>
        <description>Email - Send Pre-Decoration Completion Email Alert to HoS &amp; Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Completion_of_Pre_Decoration_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Quality_Manager_Inspection_Completion_Email_Alert_to_HoS_Sales_Manage</fullName>
        <description>Email - Send Quality Manager Inspection Completion Email Alert to HoS &amp; Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sue@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Completion_of_Quality_Manager_Inspection_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Sub_Structure_Completion_Email_Alert_to_HoS_Sales_Manager</fullName>
        <description>Email - Send Sub-Structure Completion Email Alert to HoS &amp; Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Completion_of_Sub_Structure_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Super_Structure_Completion_Email_Alert_to_HoS_Sales_Manager</fullName>
        <description>Email - Send Super-Structure Completion Email Alert to HoS &amp; Sales Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Sales_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>lesley.benwell@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Completion_of_Super_Structure_Notification</template>
    </alerts>
    <alerts>
        <fullName>Email_to_be_sent_to_HoCC_to_inform_her_of_a_plot_completion_which_requires_a_wel</fullName>
        <description>Email to be sent to HoCC to inform her of a plot completion which requires a welcome letter.</description>
        <protected>false</protected>
        <recipients>
            <recipient>sue@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plot_Property_Email_Templates/Plot_Plot_Property_Welcome_Letter_Required</template>
    </alerts>
    <alerts>
        <fullName>Email_to_customer_from_the_Head_of_Customer_Care_on_completion_Couple_Incumbent</fullName>
        <description>Email to customer from the Head of Customer Care on completion (Couple Incumbent).</description>
        <protected>false</protected>
        <recipients>
            <field>Copy_of_Incumbent_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@vanderbilthomes.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Plot_Property_Email_Templates/Plot_Plot_Property_Customer_Care_Welcome_Email_Couple_Incumbent</template>
    </alerts>
    <alerts>
        <fullName>Email_to_customer_from_the_Head_of_Customer_Care_on_completion_Single_Incumbent</fullName>
        <description>Email to customer from the Head of Customer Care on completion (Single Incumbent).</description>
        <protected>false</protected>
        <recipients>
            <field>Copy_of_Incumbent_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>noreply@vanderbilthomes.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Plot_Property_Email_Templates/Plot_Plot_Property_Customer_Care_Welcome_Email</template>
    </alerts>
    <alerts>
        <fullName>Plot_Plot_Property_Completion_Alert</fullName>
        <description>Email to be sent to HoCC to inform her of a plot completion.</description>
        <protected>false</protected>
        <recipients>
            <recipient>sue@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Plot_Property_Email_Templates/Plot_Plot_Property_Completion_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Copy_PersonAccount_Email</fullName>
        <field>Copy_of_Incumbent_Email__c</field>
        <formula>Incumbent__r.PersonContact.Email</formula>
        <name>Copy PersonAccount Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Plot_Set_Achievable_Price</fullName>
        <field>Achievable_Price__c</field>
        <formula>Release_Price__c * 0.98</formula>
        <name>Plot - Set Achievable Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Plot_Set_Warranty_End</fullName>
        <field>Warranty_End__c</field>
        <formula>IF(Sold_To__r.IsPersonAccount = TRUE, Completion_Date__c + 730, Completion_Date__c + 365)</formula>
        <name>Plot - Set Warranty End</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Plot_Set_Warranty_Start</fullName>
        <field>Warranty_Start__c</field>
        <formula>Completion_Date__c</formula>
        <name>Plot - Set Warranty Start</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Plot_Completion_Flag_to_True</fullName>
        <description>Set Plot Completion Flag to True so that the email alert workflow will only trigger once per Plot - Built By Cloudshift</description>
        <field>Plot_Completion_Alert__c</field>
        <literalValue>1</literalValue>
        <name>Set Plot Completion Flag to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Warranty_Tasks_Flag</fullName>
        <field>Warranty_Tasks_Created__c</field>
        <literalValue>1</literalValue>
        <name>Set Warranty Tasks Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_CC_Welcome_Email_Flag</fullName>
        <description>Used to stop workflow from triggering - Build By Cloudshift</description>
        <field>Flag_CC_Welcome_Email__c</field>
        <literalValue>1</literalValue>
        <name>Tick CC Welcome Email Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_CC_Welcome_Letter_Flag</fullName>
        <field>Flag_CC_Welcome_Letter__c</field>
        <literalValue>1</literalValue>
        <name>Tick CC Welcome Letter Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tick_Communal_Checkbox</fullName>
        <field>Communal_Plot__c</field>
        <literalValue>1</literalValue>
        <name>Tick Communal Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Untick_Communal_Checkbox</fullName>
        <field>Communal_Plot__c</field>
        <literalValue>0</literalValue>
        <name>Untick Communal Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Plot_Stage</fullName>
        <field>Build_Status__c</field>
        <literalValue>CML</literalValue>
        <name>Update Plot Stage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Plot_Stage_First_Fix</fullName>
        <field>Build_Status__c</field>
        <literalValue>1st Fix</literalValue>
        <name>Update Plot Stage First Fix</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Plot_Stage_Pre_Decoration</fullName>
        <field>Build_Status__c</field>
        <literalValue>2nd Fix</literalValue>
        <name>Update Plot Stage Pre-Decoration</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Plot_Stage_Quality_Manager_Inspec</fullName>
        <field>Build_Status__c</field>
        <literalValue>Sales Ownership</literalValue>
        <name>Update Plot Stage Quality Manager Inspec</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Plot_Stage_Sub_Structure</fullName>
        <field>Build_Status__c</field>
        <literalValue>Foundations</literalValue>
        <name>Update Plot Stage Sub Structure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Plot_Stage_Super_Structure</fullName>
        <field>Build_Status__c</field>
        <literalValue>Roof</literalValue>
        <name>Update Plot Stage Super Structure</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Build QA - Send Completion%2FQuality Audit Complete Notification</fullName>
        <actions>
            <name>Email_Send_Completion_Quality_Audit_Completion_Email_Alert_to_HoS_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Plot_Stage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to send a notification to the Head of Sales (Lesley Benwell) &amp; Sales Manager when a Completion/Quality Audit has been completed.</description>
        <formula>Completed_Completion_Quality_Audits__c     &gt; 0

&amp;&amp;

Communal_Plot__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Build QA - Send First Fix Complete Notification</fullName>
        <actions>
            <name>Email_Send_First_Fix_Completion_Email_Alert_to_HoS_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Plot_Stage_First_Fix</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to send a notification to the Head of Sales (Lesley Benwell) &amp; Sales Manager when a first fix has been completed.</description>
        <formula>Completed_First_Fixes__c   &gt; 0

&amp;&amp; 

Communal_Plot__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Build QA - Send Pre-Decoration Complete Notification</fullName>
        <actions>
            <name>Email_Send_Pre_Decoration_Completion_Email_Alert_to_HoS_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Plot_Stage_Pre_Decoration</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to send a notification to the Head of Sales (Lesley Benwell) &amp; Sales Manager when a pre-decoration has been completed.</description>
        <formula>Completed_Pre_Decorations__c    &gt; 0

&amp;&amp; 

Communal_Plot__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Build QA - Send Quality Manager Inspection Complete Notification</fullName>
        <actions>
            <name>Email_Send_Quality_Manager_Inspection_Completion_Email_Alert_to_HoS_Sales_Manage</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Plot_Stage_Quality_Manager_Inspec</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to send a notification to the Head of Sales (Lesley Benwell) &amp; Sales Manager when a  quality manager inspection has been completed.</description>
        <formula>Completed_Quality_Manager_Inspections__c  &gt; 0

&amp;&amp;

Communal_Plot__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Build QA - Send Super-Structure Complete Notification</fullName>
        <actions>
            <name>Email_Send_Super_Structure_Completion_Email_Alert_to_HoS_Sales_Manager</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Plot_Stage_Super_Structure</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to send a notification to the Head of Sales (Lesley Benwell) &amp; Sales Manager when a super-structure has been completed.</description>
        <formula>Completed_Super_Structures__c  &gt; 0

&amp;&amp; 

Communal_Plot__c = FALSE</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Auto-Setting of Warranty Dates</fullName>
        <actions>
            <name>Plot_Set_Warranty_End</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Plot_Set_Warranty_Start</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Completion_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to set the warranty start and warranty end dates on a plot based on the plot completion date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Incumbent Email Copy</fullName>
        <actions>
            <name>Copy_PersonAccount_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Incumbent_First_Name_Last_Name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow to make a copy of the email, this email field will then be used in an email alert - Built By Clouldshift</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Individual - Auto-Creation of Warranty Reminders</fullName>
        <actions>
            <name>Set_Warranty_Tasks_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reminder_12_Month_Customer_Success_Check_Up</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Reminder_18_Month_Customer_Success_Check_Up</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Reminder_2_Week_Customer_Success_Call</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Reminder_6_Month_Customer_Success_Check_Up</name>
            <type>Task</type>
        </actions>
        <actions>
            <name>Reminder_Send_Customer_Offboarding_Email</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>Workflow to create 2 week, 6 month, 12 month, 18 month &amp; 24 month follow-up tasks based on warranty start date.</description>
        <formula>AND( Warranty_Tasks_Created__c = FALSE, NOT(ISBLANK(Completion_Date__c)), Sold_To__r.IsPersonAccount = TRUE)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Plot%2FProperty CC Welcome Email %28Couple Incumbent%29%2E</fullName>
        <actions>
            <name>Email_to_customer_from_the_Head_of_Customer_Care_on_completion_Couple_Incumbent</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Tick_CC_Welcome_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Flag_CC_Welcome_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Sales_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Couple_Incumbents__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Property_Type__c</field>
            <operation>notEqual</operation>
            <value>Communal area</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Copy_of_Incumbent_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email to customer from the Head of Customer Care on completion. This would be a welcome email and detail the channels to contact the team etc.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Plot%2FProperty CC Welcome Email %28Single Incumbent%29%2E</fullName>
        <actions>
            <name>Email_to_customer_from_the_Head_of_Customer_Care_on_completion_Single_Incumbent</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Tick_CC_Welcome_Email_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Flag_CC_Welcome_Email__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Sales_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Couple_Incumbents__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Property_Type__c</field>
            <operation>notEqual</operation>
            <value>Communal area</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Copy_of_Incumbent_Email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email to customer from the Head of Customer Care on completion. This would be a welcome email and detail the channels to contact the team etc.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Plot%2FProperty Completion Alert</fullName>
        <actions>
            <name>Plot_Plot_Property_Completion_Alert</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Set_Plot_Completion_Flag_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Sales_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Plot_Completion_Alert__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Property_Type__c</field>
            <operation>notEqual</operation>
            <value>Communal area</value>
        </criteriaItems>
        <description>Email to be sent to HoCC to inform her of a plot completion.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Plot%2FProperty Welcome Letter Required</fullName>
        <actions>
            <name>Email_to_be_sent_to_HoCC_to_inform_her_of_a_plot_completion_which_requires_a_wel</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Tick_CC_Welcome_Letter_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Flag_CC_Welcome_Letter__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Sales_Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Couple_Incumbents__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Property_Type__c</field>
            <operation>notEqual</operation>
            <value>Communal area</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Copy_of_Incumbent_Email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Email to be sent to HoCC to inform her of a plot completion which requires a welcome letter.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Plot - Set Achievable Price</fullName>
        <actions>
            <name>Plot_Set_Achievable_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Release_Price__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow rule to set the achievable price to 98% of the release price.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Tick Communal Checkbox if Property Type is Communal area</fullName>
        <actions>
            <name>Tick_Communal_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Property_Type__c</field>
            <operation>equals</operation>
            <value>Communal area</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Communal_Plot__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Untick Communal Checkbox if Property Type is NOT Communal area</fullName>
        <actions>
            <name>Untick_Communal_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Plot__c.Property_Type__c</field>
            <operation>notEqual</operation>
            <value>Communal area</value>
        </criteriaItems>
        <criteriaItems>
            <field>Plot__c.Communal_Plot__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Reminder_12_Month_Customer_Success_Check_Up</fullName>
        <assignedTo>sue@vanderbilthomes.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>365</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Plot__c.Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Reminder: 12 Month Customer Success Check-Up</subject>
    </tasks>
    <tasks>
        <fullName>Reminder_18_Month_Customer_Success_Check_Up</fullName>
        <assignedTo>sue@vanderbilthomes.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>545</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Plot__c.Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Reminder: 18 Month Customer Success Check-Up</subject>
    </tasks>
    <tasks>
        <fullName>Reminder_2_Week_Customer_Success_Call</fullName>
        <assignedTo>sue@vanderbilthomes.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Plot__c.Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Reminder: 2-Week Customer Success Call</subject>
    </tasks>
    <tasks>
        <fullName>Reminder_6_Month_Customer_Success_Check_Up</fullName>
        <assignedTo>sue@vanderbilthomes.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>180</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Plot__c.Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Reminder: 6 Month Customer Success Check-Up</subject>
    </tasks>
    <tasks>
        <fullName>Reminder_Send_Customer_Offboarding_Email</fullName>
        <assignedTo>sue@vanderbilthomes.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <dueDateOffset>730</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Plot__c.Completion_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Open</status>
        <subject>Reminder: Send Customer Offboarding Email</subject>
    </tasks>
</Workflow>
