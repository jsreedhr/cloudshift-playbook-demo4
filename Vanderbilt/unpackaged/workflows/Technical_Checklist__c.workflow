<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>All_Fields_are_blank_uncheck</fullName>
        <field>All_Fields_Blank__c</field>
        <literalValue>0</literalValue>
        <name>All Fields are blank uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Commercial_Handover_In_Approval_Untick</fullName>
        <field>Technical_Commercial_Handover_In_Approva__c</field>
        <literalValue>0</literalValue>
        <name>Commercial Handover In Approval Untick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Land_Tech_Handover_in_Approval_Untick</fullName>
        <field>Land_Technical_Handover_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Land Tech Handover in Approval Untick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Land_Technical_Handover_Approved</fullName>
        <field>Land_Technical_Handover_Approved_Tick__c</field>
        <literalValue>1</literalValue>
        <name>Land Technical Handover Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Land_Technical_Handover_in_Approval</fullName>
        <field>Land_Technical_Handover_In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Land Technical Handover in Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_Plan_1_Approved_Tick</fullName>
        <field>Pre_Plan_1_Approved_Tick__c</field>
        <literalValue>1</literalValue>
        <name>Pre-Plan 1 Approved Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_Plan_1_in_Approval_Tick</fullName>
        <field>Pre_Plan_1_In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Pre-Plan 1 in Approval Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_Plan_1_in_Approval_UnTick</fullName>
        <field>Pre_Plan_1_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Pre-Plan 1 in Approval UnTick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_Plan_2_Approved_Tick</fullName>
        <field>Pre_Plan_2_Approved_Tick__c</field>
        <literalValue>1</literalValue>
        <name>Pre-Plan 2 Approved Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_Plan_2_in_Approval_Tick</fullName>
        <field>Pre_Plan_2_In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Pre-Plan 2 in Approval Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Pre_Plan_2_in_Approval_UnTick</fullName>
        <field>Pre_Plan_2_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Pre-Plan 2 in Approval UnTick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tech_Commercial_Handover_Approved</fullName>
        <field>Technical_Commercial_Handover_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Tech - Commercial Handover Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tech_Commercial_Handover_In_Approval</fullName>
        <field>Technical_Commercial_Handover_In_Approva__c</field>
        <literalValue>1</literalValue>
        <name>Tech - Commercial Handover In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Tech_ConstructHandover_inApproval_untick</fullName>
        <field>Technical_Construct_Handover_In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Tech ConstructHandover inApproval untick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technical_Construct_Handover_in_Approval</fullName>
        <field>Technical_Construct_Handover_In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Technical_Construct_Handover in Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technical_Construction_Handover_Approved</fullName>
        <field>Technical_Construct_Handover_Tick__c</field>
        <literalValue>1</literalValue>
        <name>Technical_Construction_Handover Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Ready_for_Approval_Checkbox</fullName>
        <field>All_Fields_Blank__c</field>
        <literalValue>1</literalValue>
        <name>Update Ready for Approval Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Land_Technical_Handover Fields are Blank</fullName>
        <actions>
            <name>Update_Ready_for_Approval_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used with an Approval process to confirm all the fields have been completed on the record.</description>
        <formula>AND( RecordType.DeveloperName=&quot;Land_Technical_Handover&quot;,  Land_Technical_Handover_Approved_Tick__c&lt;&gt;True,  NOT(ISBLANK(F10_Notification__c)), NOT(ISBLANK(Pre_construction_Info_Pack_CDM_Handover__c)), NOT(ISBLANK(Principal_Contractor_Status__c)),  NOT(ISBLANK( Site_Waste_Management_Plan__c )), NOT(ISBLANK(Traffic_Management_Plan__c)), NOT(ISBLANK(Existing_Properties_Security__c)),  NOT(ISBLANK(Existing_Properties_Keys__c)),  NOT(ISBLANK(Existing_Properties_Service_Providers_F__c)),  NOT(ISBLANK( Existing_Properties_Water__c )), NOT(ISBLANK(Existing_Properties_Gas__c)), NOT(ISBLANK(Existing_Properties_Electric__c)), NOT(ISBLANK(Existing_Properties_BT__c)),    NOT(ISBLANK(Overlay_to_be_Completed_With_Land_Title__c)), NOT(ISBLANK(Boundary_to_be_Reviewed__c)), NOT(ISBLANK(Adjacent_Property_Survey_to_be_Completed__c)),  NOT(ISBLANK( Rights_of_way_over_or_adjacent_to_site__c )),  NOT(ISBLANK(New_Wall__c)), NOT(ISBLANK(Walls_to_Site_Entrance_Frontage__c)),   NOT(ISBLANK(Existing_site_access_road__c )), NOT(ISBLANK(Existing_Site_Access_Road_Residents_Ass__c)), NOT(ISBLANK(Existing_Site_Access_Road_Deed_of_Grant__c)), NOT(ISBLANK(Existing_Site_Access_Road_Condition_Surv__c)),   NOT(ISBLANK(Asbestos_Survey__c)),  NOT(ISBLANK(Planning_Tracker_Completed__c)),	   NOT(ISBLANK( Tree_Protection_Drawing_Drawing_Issued__c )), NOT(ISBLANK( Dates_and_Payments_Required__c )),    NOT(ISBLANK(Soil_Investigation__c)), NOT(ISBLANK(Ground_Contamination__c)), NOT(ISBLANK( Remediation_Strategy__c )), NOT(ISBLANK(Soakage_Tests__c)), NOT(ISBLANK(Foundation_Type__c)), NOT(ISBLANK(Rights_of_Light_Agreement__c)), NOT(ISBLANK(Party_Wall_Award__c)),   NOT(ISBLANK(Existing_Services_Retained_Water_TBS__c)),  NOT(ISBLANK(Existing_Services_Retained_Gas__c)),  NOT(ISBLANK(Existing_Services_Retained_Electric_TBS__c)),  NOT(ISBLANK(Existing_Services_Retained_BT__c)),    NOT(ISBLANK( Traffic_Management_Plan_Highways__c )), NOT(ISBLANK(Street_Lighting__c)), NOT(ISBLANK(Adoption_Agreements_or_Licences__c)),      NOT(ISBLANK(Highways__c)), NOT(ISBLANK(Sewerage_Application_to_Connect__c)), NOT(ISBLANK(Storm_Water_Application_and_Build_Over__c)), NOT(ISBLANK(Off_site_Connections__c)), NOT(ISBLANK(Easements__c)), NOT(ISBLANK(Pumping__c)),   NOT(ISBLANK(Stage_1_Groundworks_Package_Inc_Substru__c)), NOT(ISBLANK(Stage_1_Engineers_Drawings__c)), NOT(ISBLANK(Stage_2_Working_Drawings__c)), NOT(ISBLANK(Stage_3_Windows_and_Door_Schedules__c)), NOT(ISBLANK(Stage_3_M_E_Drawings__c)),  NOT(ISBLANK(Combined_Services_Layout_Waters__c)), NOT(ISBLANK(Combined_Services_Layout_Electrics__c)), NOT(ISBLANK(Combined_Services_Layout_Gass__c)), NOT(ISBLANK(Combined_Services_Layout_BT__c)), NOT(ISBLANK(Combined_Services_Layout_MPAN__c )),   NOT(ISBLANK(Who_and_Contact_Details__c)), NOT(ISBLANK(Initial_Notice_Submitted__c)), NOT(ISBLANK(Building_Reg_Approval__c)), NOT(ISBLANK(Conditions_Outstanding__c)),   NOT(ISBLANK(Code_Requirements__c)), NOT(ISBLANK(Construction_Specification__c)), NOT(ISBLANK(Development_Specification__c)), NOT(ISBLANK(Kitchen_Layouts__c)), NOT(ISBLANK(Bathroom_Layouts__c)), NOT(ISBLANK(Landscape_Drawing_Including_Hard_and_Sof__c)),   NOT(ISBLANK(Development_Specifications__c)),    NOT(ISBLANK(Procurement_Schedule__c)), NOT(ISBLANK(Trade_Procurement_Schedule__c)), NOT(ISBLANK(Material_Procurement_Schedule__c)), NOT(ISBLANK(Ground_Worker_Tender_List__c )), NOT(ISBLANK( Development_Specificationss__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Technical - Construction Handover Fields are Blank</fullName>
        <actions>
            <name>Update_Ready_for_Approval_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used with an Approval process to confirm all the fields have been completed on the record.</description>
        <formula>AND( 
RecordType.DeveloperName=&quot;Technical_Construction_Handover&quot;, 
Technical_Construct_Handover_Tick__c&lt;&gt;True, 
NOT(ISBLANK(Planning_compliance_check_complete_Comms__c )), 
NOT(ISBLANK(Construction_Programme__c )), 
NOT(ISBLANK(Temporary_Accommodation_Layout__c )), 
NOT(ISBLANK(Scaffold_Plan__c )), 
NOT(ISBLANK(Special_Scaffold_Requirements__c )), 
NOT(ISBLANK(Principal_Contractors_H_S_Plan__c )), 
NOT(ISBLANK(Site_Plan__c )), 
NOT(ISBLANK(Setting_Out_Plan__c )), 
NOT(ISBLANK(Substructure__c )), 
NOT(ISBLANK(Gas_Membranes__c )), 
NOT(ISBLANK(Basement__c )), 
NOT(ISBLANK(GA_s_Lower_Ground_Floor__c )), 
NOT(ISBLANK(GA_s_Ground_Floor__c )), 
NOT(ISBLANK(GA_s_First_Floor__c )), 
NOT(ISBLANK(GA_s_Second_Floor__c )), 
NOT(ISBLANK(GA_s_Third_Floor__c )), 
NOT(ISBLANK(Substructure_House_Types__c )), 
NOT(ISBLANK(GA_s_Ground_Floor_House_Types__c )), 
NOT(ISBLANK(GA_s_First_Floor_House_Types__c )), 
NOT(ISBLANK(GA_s_Second_Floor_House_Types__c )), 
NOT(ISBLANK(Sections__c )), 
NOT(ISBLANK(Elevations__c )), 
NOT(ISBLANK(Electrical_Layouts__c )), 
NOT(ISBLANK(Heating_Layouts__c )), 
NOT(ISBLANK(Sales_Plans__c )), 
NOT(ISBLANK(Floor_Joist_Designs__c )), 
NOT(ISBLANK(Concrete_Floor_Designs__c )), 
NOT(ISBLANK(Roof_Truss_Designs__c )), 
NOT(ISBLANK(Bathroom_Plans_Elevations__c )), 
NOT(ISBLANK(Kitchen_Plans_Elevations__c )), 
NOT(ISBLANK(Mechanical_Extract_Details__c )), 
NOT(ISBLANK(Any_Other_Details_to_Note_General__c )), 
NOT(ISBLANK(Foundation_Details__c )), 
NOT(ISBLANK(Threshold_Details__c )), 
NOT(ISBLANK(Lift_Pit_Detail__c )), 
NOT(ISBLANK(External_Wall_Details__c )), 
NOT(ISBLANK(Internal_Wall_Details__c )), 
NOT(ISBLANK(Separating_Wall_Details__c )), 
NOT(ISBLANK(Ground_Floor_Detail__c )), 
NOT(ISBLANK(Separating_Floor_Detail__c )), 
NOT(ISBLANK(Roof_Detail__c )), 
NOT(ISBLANK(Roof_Terrace_detail__c )), 
NOT(ISBLANK(Balcony_detail__c )), 
NOT(ISBLANK(Bay_Details__c )), 
NOT(ISBLANK(Window_Schedule__c )), 
NOT(ISBLANK(Door_Schedule__c )), 
NOT(ISBLANK(Canopy_Details__c )), 
NOT(ISBLANK(SVP_RWP_Setting_Out_Details__c )), 
NOT(ISBLANK(Fire_Stops__c )), 
NOT(ISBLANK(Foundation_Layouts_Garages__c )), 
NOT(ISBLANK(Secured_by_Design_Requirements__c )), 
NOT(ISBLANK(Temporary_Works_for_Sales__c )), 
NOT(ISBLANK(Entrance_Feature_Details__c )), 
NOT(ISBLANK(Standard_Details__c )), 
NOT(ISBLANK(Conversion_Scope_of_Works_details__c )), 
NOT(ISBLANK(Any_Other_Details_to_Note_Construction__c )), 
NOT(ISBLANK(Approved_Block_Beam_Layout__c )), 
NOT(ISBLANK(Stone_Cladding__c )), 
NOT(ISBLANK(Render_inc_Joint_Reinforcement__c )), 
NOT(ISBLANK(Timber_Cladding__c )), 
NOT(ISBLANK(Metal_Cladding__c )), 
NOT(ISBLANK(Standing_Seam__c )), 
NOT(ISBLANK(Stairs__c )), 
NOT(ISBLANK(Lift__c )), 
NOT(ISBLANK(Fire_Strategy__c )), 
NOT(ISBLANK(Smoke_Shaft__c )), 
NOT(ISBLANK(GRP_Products__c )), 
NOT(ISBLANK(Flue__c )), 
NOT(ISBLANK(Brick_Details__c )), 
NOT(ISBLANK(Any_Other_Details_to_Note_Specialist__c )), 
NOT(ISBLANK(Basement_Mechanical_Electrical_Layout__c )), 
NOT(ISBLANK(Lower_Ground_Floor__c )), 
NOT(ISBLANK(Ground_Floor__c )), 
NOT(ISBLANK(First_Floor__c )), 
NOT(ISBLANK(Second_Floor__c )), 
NOT(ISBLANK(Third_Floor__c )), 
NOT(ISBLANK(External_Materials_Schedule_Construction__c )), 
NOT(ISBLANK(Sales__c )), 
NOT(ISBLANK(Construction__c )), 
NOT(ISBLANK(Housing_Association__c )), 
NOT(ISBLANK(Temporary_Supplies_Electricity__c )), 
NOT(ISBLANK(Temporary_Supplies_Water__c )), 
NOT(ISBLANK(Scheme_Utilities_BT__c )), 
NOT(ISBLANK(Scheme_Utilities_Electricity__c )), 
NOT(ISBLANK(Scheme_Utilities_Gas__c )), 
NOT(ISBLANK(Scheme_Utilities_Water__c )), 
NOT(ISBLANK(Scheme_Utilities_Virgin_Media__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Technical - Eng Handover Fields are Blank</fullName>
        <actions>
            <name>Update_Ready_for_Approval_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used with an Approval process to confirm all the fields have been completed on the record.</description>
        <formula>AND( Technical_Commercial_Handover_Approved__c&lt;&gt;True, RecordType.DeveloperName=&quot;Technical_Engineering_Handover&quot;, NOT(ISBLANK(Planning_compliance_check_complete_Eng__c)), NOT(ISBLANK(Initial_Notice_Accepted__c )), NOT(ISBLANK(Site_Walkover_and_Conditions_Schedule__c )), NOT(ISBLANK(Topographical_Site_Survey__c )), NOT(ISBLANK(Site_Clearance_Plan__c )), NOT(ISBLANK(Tree_Protection_Plan_inc_No_dig_Locati__c )), NOT(ISBLANK(Ground_Investigation_Report__c )), NOT(ISBLANK(Remediation_Strategy__c)), NOT(ISBLANK(Temporary_Footpath_Requirements__c )), NOT(ISBLANK(Ecology_Reports__c)), NOT(ISBLANK(Archaeology_Reports__c)), NOT(ISBLANK(Existing_Boundary_condition__c )), NOT(ISBLANK(Section_278_S38_Drawings_Lighting_Sign__c )), NOT(ISBLANK(Setting_Out_Drawing_and_Schedule__c )), NOT(ISBLANK(Roads_and_Sewer_Long_Sections__c )), NOT(ISBLANK(Construction_Details__c )), NOT(ISBLANK(Sewer_Connection_and_Road_Opening_Licenc__c )), NOT(ISBLANK(Drainage_Layout_inc_Domestic__c )), NOT(ISBLANK(Manhole_Schedule__c )), NOT(ISBLANK(Drainage_Pumping_Rising_Main__c )), NOT(ISBLANK(Drainage_Details_SuDS_detailing__c )), NOT(ISBLANK(Drainage_Details_S104_application__c )), NOT(ISBLANK(Drainage_Details_Pipe_Trenching__c )), NOT(ISBLANK(Drainage_Details_Land_Drainage_Details__c )), NOT(ISBLANK(Service_Disconnections_certifications__c )), NOT(ISBLANK(Existing_Services_Plan__c )), NOT(ISBLANK(TBS_Details__c )), NOT(ISBLANK(Combined_Services_Layout_Electric__c )), NOT(ISBLANK(Combined_Services_Layout_Water__c )), NOT(ISBLANK(Combined_Services_Engineering_BT_Telecom__c )), NOT(ISBLANK(Combined_Services_Layout_Gas__c )), NOT(ISBLANK(Combined_Services_Layout_Street_Lights__c )), NOT(ISBLANK(Foundation_Layouts_Strip_Depths_trad__c )), NOT(ISBLANK(Foundation_Layouts_Settings_Out__c )), NOT(ISBLANK(Piling_Mat_Designs__c )), NOT(ISBLANK(Foundation_Layouts_Pile_Designs_Layouts__c )), NOT(ISBLANK(Foundation_Layouts_Bending_Schedules__c )), NOT(ISBLANK(Piling_Ring_Beam_Bending_Schedules__c )), NOT(ISBLANK(Foundation_Layouts_Heave_Precautions__c )), NOT(ISBLANK(Foundation_Layouts_Gas_Membranes__c )), NOT(ISBLANK(Foundation_Layouts_Sections__c )), NOT(ISBLANK(Foundation_Layouts_Garages__c )), NOT(ISBLANK(Foundation_Layouts_Lift_Pit__c )), NOT(ISBLANK(Foundation_Layouts_Bin_Cycle_Stores__c )), NOT(ISBLANK(Substations_Gas_Governors_Foundations__c )), NOT(ISBLANK(Conversion_Scope_Underpinning__c )), NOT(ISBLANK(Conversion_Scope_of_Works_DPM_Tanking__c )), NOT(ISBLANK(Conversion_Works_Floor_Construction__c )), NOT(ISBLANK(Wall_Details_RC_Wall__c )), NOT(ISBLANK(Wall_Brick_Work_Foundation_Drainage__c )), NOT(ISBLANK(Wall_Details_Log_Walls__c )), NOT(ISBLANK(Wall_Details_Gabion__c )), NOT(ISBLANK(Wall_Details_Crib_Wall__c )), NOT(ISBLANK(Wall_Details_Other__c )), NOT(ISBLANK(Underpinning__c )), NOT(ISBLANK(Adjacent_Foundation_Sections__c )), NOT(ISBLANK(Extent_of_Existing_Adopted_Highway__c )), NOT(ISBLANK(Proposed_Extent_of_Adoption__c )), NOT(ISBLANK(Road_Surface_Finishes__c )), NOT(ISBLANK(Footpath_Surface_Finishes__c )), NOT(ISBLANK(Porous_Paving_Details__c )), NOT(ISBLANK(Kerb_and_Edging_Details__c )), NOT(ISBLANK(Play_Areas_layout_fencing_surfacing__c )), NOT(ISBLANK(Bin_Cycle_Store__c )), NOT(ISBLANK(Substation_Gas_Governors__c )), NOT(ISBLANK(Patio_Paths_Steps__c )), NOT(ISBLANK(Retaining_Walls__c )), NOT(ISBLANK(Screen_Walls__c )), NOT(ISBLANK(Fencing__c )), NOT(ISBLANK(Standard_Construction_Details__c )), NOT(ISBLANK(Approved_Landscaping_Layout__c )), NOT(ISBLANK(Sales_Area_External_Works_Layout__c )), NOT(ISBLANK(Construction_Program__c )), NOT(ISBLANK(Traffic_Management_Plan_Schedules_Plans__c )), NOT(ISBLANK(Compound_Storage_Haul_Road_Plan__c )), NOT(ISBLANK(H_S_Plan_or_Pretender_H_S_Plan__c )), NOT(ISBLANK(External_Material_Schedule__c )))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
