<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Send_Site_Close_Down_Approval_APPROVED_Email_to_Owner</fullName>
        <description>Email - Send Site Close Down Approval APPROVED Email to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Site_Close_Down_Approval_Process_Approved</template>
    </alerts>
    <fieldUpdates>
        <fullName>Site_Close_Down_Mark_Completed</fullName>
        <description>This update populates the Site Close Down Date Completed when the Status is set to Completed</description>
        <field>Date_Completed__c</field>
        <formula>today()</formula>
        <name>Site Close Down - Mark Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Close_Down_Set_Naming_Convention</fullName>
        <field>Name</field>
        <formula>&quot;Development Close Down: &quot; &amp; Development__r.Name</formula>
        <name>Site Close Down - Set Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Site Close Down - Populate Date Completed</fullName>
        <actions>
            <name>Site_Close_Down_Mark_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This update populates the Site Start Date Completed when the Status is set to Completed</description>
        <formula>ISBLANK( Date_Completed__c )   &amp;&amp;   ISPICKVAL( Status__c, &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Site Close Down - Set Naming Convention</fullName>
        <actions>
            <name>Site_Close_Down_Set_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to set up a standard naming convention for the site close down object.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
