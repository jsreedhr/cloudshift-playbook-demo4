<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Notification_of_Inspection_Being_Created</fullName>
        <description>Email - Notification of Inspection Being Created</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <recipient>peter.hutson@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Site_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/H_S_Inspection_Created_Alert</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Update</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>H%26S Inspection - Change Record Types</fullName>
        <actions>
            <name>Update_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW()</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
