<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Snag_Closed_Datestamp</fullName>
        <field>Date_Completed__c</field>
        <formula>TODAY()</formula>
        <name>Snag Closed Datestamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Snag - Date Completed</fullName>
        <actions>
            <name>Snag_Closed_Datestamp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISBLANK(Date_Completed__c) &amp;&amp; ISPICKVAL( Status__c ,&apos;Completed&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
