<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Build_QA_Notify_Head_of_QA_that_last_Plot_created_on_Development</fullName>
        <description>Build QA - Notify Head of QA that last Plot created on Development</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.hutson@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Last_Plot_of_Development_Completed</template>
    </alerts>
    <alerts>
        <fullName>Land_New_Development_Created_from_Site</fullName>
        <description>Land - New Development Created from Site</description>
        <protected>false</protected>
        <recipients>
            <recipient>paul.mckeown@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Land_Templates/Land_New_Development_Created_from_Site</template>
    </alerts>
    <alerts>
        <fullName>Land_Site_Failed_at_Planning</fullName>
        <description>Email alert sent to James notifying him to delete the development as the site failed at planning.</description>
        <protected>false</protected>
        <recipients>
            <recipient>james.geake@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Land_Templates/Land_Site_Failed_at_Planning</template>
    </alerts>
    <fieldUpdates>
        <fullName>Tick_Development_Complete_Flag</fullName>
        <field>Development_Complete_Email_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Tick Development Complete Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Email Alert - Last Plot completed on Development</fullName>
        <actions>
            <name>Build_QA_Notify_Head_of_QA_that_last_Plot_created_on_Development</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Tick_Development_Complete_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>This Email Alert notifies the Head of Build QA that the last Plot on a Development has been created</description>
        <formula>AND( Number_of_Plots__c&gt;0, Plots_Left_to_Complete__c=0, Development_Complete_Email_Flag__c=false )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Notification when a Development is created by a Process</fullName>
        <actions>
            <name>Land_New_Development_Created_from_Site</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Development__c.Site_Stage__c</field>
            <operation>equals</operation>
            <value>Legally Completed</value>
        </criteriaItems>
        <description>This triggers when a site reaches the Legally Completed stage.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Land - Site Failed at Planning</fullName>
        <actions>
            <name>Land_Site_Failed_at_Planning</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Development__c.Site_Stage__c</field>
            <operation>equals</operation>
            <value>Planning not achieved / Sold</value>
        </criteriaItems>
        <description>Email alert sent to James notifying him to delete the development as the site failed at planning.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
