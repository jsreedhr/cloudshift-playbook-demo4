<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>New_reservation</fullName>
        <ccEmails>henry.dawkins@vanderbilthomes.co.uk</ccEmails>
        <ccEmails>kamal.grewal@vanderbilthomes.co.uk</ccEmails>
        <description>New reservation</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Henry_Digital_Marketing/New_sales_progression</template>
    </alerts>
    <alerts>
        <fullName>Opportunity_Sales_Progression_Completion_Survey</fullName>
        <description>Opportunity - Sales Progression Completion Survey</description>
        <protected>false</protected>
        <recipients>
            <field>Primary_Purchaser__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@vanderbilthomes.co.uk</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Sales_Progression_Templates/Sales_Progression_Send_Survey_Email</template>
    </alerts>
    <alerts>
        <fullName>Sales_Progression_Reservation_Approval_Confirmation</fullName>
        <description>Sales Progression - Reservation Approval Confirmation</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Progression_Templates/Sales_Progression_Reservation_Approval_Confirmation</template>
    </alerts>
    <alerts>
        <fullName>Sales_Progression_Reservation_Approval_Process_Rejection</fullName>
        <description>Sales Progression - Reservation Approval Process Rejection</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Sales_Progression_Templates/Sales_Progression_Reservation_Approval_Process_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Reservation_Approved_Tick</fullName>
        <field>Reservation_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Reservation Approved Tick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reservation_Approved_Untick</fullName>
        <description>Reservation Approved Untick</description>
        <field>Reservation_Approved__c</field>
        <literalValue>0</literalValue>
        <name>Reservation Approved Untick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reservation_In_Approval</fullName>
        <description>Reservation In Approval - checkbox ticked</description>
        <field>In_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Reservation In Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reservation_In_Approval_Untick</fullName>
        <field>In_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Reservation In Approval Untick</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Progress_Set_Naming_Convention</fullName>
        <field>Name</field>
        <formula>IF(Account.IsPersonAccount = TRUE, &quot;Reservation&quot; &amp;&quot; - &quot;&amp; Development_Marketing_Name__c &amp;&quot; - &quot;&amp; &quot;Plot:&quot;&amp; Plot_Property__r.Plot_Number__c &amp;&quot; - &quot;&amp; Account.LastName, &quot;Reservation&quot; &amp;&quot; - &quot;&amp; Development_Marketing_Name__c &amp;&quot; - &quot;&amp; &quot;Plot:&quot;&amp; Plot_Property__r.Plot_Number__c &amp;&quot; - &quot;&amp; Account.Name)</formula>
        <name>Sales Progress -  Set Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Reserved</fullName>
        <field>StageName</field>
        <literalValue>Reserved</literalValue>
        <name>Set Stage to Reserved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Stage_to_Reserved_Approved</fullName>
        <field>StageName</field>
        <literalValue>Reserved - Approved</literalValue>
        <name>Set Stage to &apos;Reserved - Approved&apos;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>New sales progression</fullName>
        <actions>
            <name>New_reservation</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Reservation_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Notifies Henry Dawkins when there is a new reservation for digital marketing purposes.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity - On Completion Send Survey</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Completion_Survey_Sent__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Opportunity_Sales_Progression_Completion_Survey</name>
                <type>Alert</type>
            </actions>
            <timeLength>72</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Sales Progression - Naming Convention</fullName>
        <actions>
            <name>Sales_Progress_Set_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Naming convention for Sales Progression Names.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
