<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Close_Date_to_Today</fullName>
        <field>Close_Out_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Close Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inspection_Date</fullName>
        <field>Inspection_Date__c</field>
        <formula>H_S_Inspections__r.Inspection_Date__c</formula>
        <name>Update Inspection Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>H%26S Inspection Actions - Close Date</fullName>
        <actions>
            <name>Update_Close_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Status__c,&quot;Completed&quot;)  &amp;&amp;  ISBLANK( Close_Out_Date__c )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
