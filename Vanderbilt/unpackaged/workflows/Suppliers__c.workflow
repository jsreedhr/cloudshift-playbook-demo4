<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Supplier_Update_Daily_Trigger</fullName>
        <field>Daily_Update_Trigger__c</field>
        <formula>TODAY()</formula>
        <name>Supplier - Update Daily Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Supplier_Update_RAG_Status_1_WF</fullName>
        <field>RAG_Status_1_WF__c</field>
        <formula>RAG_Status_1__c</formula>
        <name>Supplier - Update RAG Status 1 WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Supplier_Update_RAG_Status_2_WF</fullName>
        <field>RAG_Status_2_WF__c</field>
        <formula>RAG_Status_2__c</formula>
        <name>Supplier - Update RAG Status 2 WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Suppliers_Tick_If_Technical_Approval_R</fullName>
        <field>Technical_Approval_Required__c</field>
        <literalValue>1</literalValue>
        <name>Suppliers - Tick If Technical Approval R</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Check Materials if Technical Approval Required</fullName>
        <actions>
            <name>Suppliers_Tick_If_Technical_Approval_R</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>IF( OR( 
ISPICKVAL(Material__c , &quot;Brickwork Support System/Steel&quot;),  ISPICKVAL(Material__c , &quot;Stonework&quot;),  
ISPICKVAL(Material__c , &quot;Steel Lintels&quot;),  ISPICKVAL(Material__c , &quot;Structural steel - unfabricated&quot;),  ISPICKVAL(Material__c , &quot;Structural steel - fabricated&quot;),  ISPICKVAL(Material__c , &quot;AOV&apos;s&quot;),  
ISPICKVAL(Material__c , &quot;GRP Dormers &amp; Porches&quot;),  ISPICKVAL(Material__c , &quot;GRP Chimneys&quot;),  ISPICKVAL(Material__c , &quot;Flues&quot;),  
ISPICKVAL(Material__c , &quot;Roof Trusses&quot;),    ISPICKVAL(Material__c , &quot;Timber Entrance Canopies&quot;),  ISPICKVAL(Material__c , &quot;Joists &amp; Flooring&quot;),  
ISPICKVAL(Material__c , &quot;Permanent Site Signage&quot;), 
ISPICKVAL(Material__c , &quot;Timber Staircases&quot;)), TRUE, FALSE )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Supplier - Daily Update Rag status WF</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Suppliers__c.Daily_Update_Trigger__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Suppliers__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Order Finalised</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Supplier_Update_Daily_Trigger</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Supplier_Update_RAG_Status_1_WF</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Supplier_Update_RAG_Status_2_WF</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Suppliers__c.Daily_Update_Trigger__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Supplier - Update RAG Status 1 %26 2</fullName>
        <actions>
            <name>Supplier_Update_RAG_Status_1_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Supplier_Update_RAG_Status_2_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW(),  ISCHANGED(Appointed_Order_Date__c) ,  ISCHANGED(Sign_Off_Actual_Date__c) , ISCHANGED(OTE_Actual_Date__c) , ISCHANGED( Target_Appointment_Date__c ) , ISCHANGED( Sign_Off_Target_Date__c ) , ISCHANGED(OTE_Target_Date__c), ISCHANGED( Technical_Approval_Actual_Date__c ) , ISCHANGED( Technical_Approval_Target_Date__c ), ISCHANGED( Daily_Update_Trigger__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
