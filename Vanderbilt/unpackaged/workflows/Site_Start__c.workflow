<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_F10_30_days_to_Contracts_Manager</fullName>
        <description>Email - F10 30 days to Contracts Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Site_Start_F10_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Email_Remind_Contracts_Manager_about_F10</fullName>
        <description>Email - Remind Contracts Manager about F10</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Site_Start_F10_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Site_Start_Approval_APPROVED_Email_to_Owner</fullName>
        <description>Email - Send Site Start Approval APPROVED Email to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Site_Start_Approval_Process_Approved</template>
    </alerts>
    <alerts>
        <fullName>Email_Send_Site_Start_REJECTED_Notification_to_Owner</fullName>
        <description>Email - Send Site Start REJECTED Notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Site_Start_Approval_Process_Rejection</template>
    </alerts>
    <fieldUpdates>
        <fullName>Site_Start_Completed_Date</fullName>
        <description>This update populates the Site Start Date Completed when the Status is set to Completed</description>
        <field>Date_Completed__c</field>
        <formula>TODAY()</formula>
        <name>Site Start - Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Start_Set_Naming_Convention</fullName>
        <field>Name</field>
        <formula>&quot;Development Start: &quot; &amp; Development__r.Name</formula>
        <name>Site Start - Set Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Build QA - Site Start - F10 reminder</fullName>
        <active>true</active>
        <description>This rule sends an email reminder to the Contracts Manager 30 days prior to the date in the field &quot;F10 expiry date&quot;</description>
        <formula>NOT(ISBLANK( F10_expiry_date__c ))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_F10_30_days_to_Contracts_Manager</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Site_Start__c.F10_expiry_date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Site Start - Naming Convention</fullName>
        <actions>
            <name>Site_Start_Set_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Site Start - Populate Date Completed</fullName>
        <actions>
            <name>Site_Start_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the Date Completed field when the status field is set to &quot;Completed&quot;</description>
        <formula>ISBLANK( Date_Completed__c )  &amp;&amp;  ISPICKVAL( Status__c, &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
