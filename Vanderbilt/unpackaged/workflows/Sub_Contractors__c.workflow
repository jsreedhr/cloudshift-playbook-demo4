<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Sub_Contractor_Update_Daily_Trigger</fullName>
        <field>Daily_Update_Trigger__c</field>
        <formula>TODAY()</formula>
        <name>Sub-Contractor - Update Daily Trigger</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Contractor_Update_RAG_Status_1_WF</fullName>
        <field>RAG_Status_1_WF__c</field>
        <formula>RAG_Status_1__c</formula>
        <name>Sub-Contractor - Update RAG Status 1 WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sub_Contractor_Update_RAG_Status_2_WF</fullName>
        <field>RAG_Status_2_WF__c</field>
        <formula>RAG_Status_2__c</formula>
        <name>Sub-Contractor - Update RAG Status 2 WF</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Sub-Contractor - Daily Update Rag status WF</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Sub_Contractors__c.Daily_Update_Trigger__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Sub_Contractors__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Appointed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Sub_Contractor_Update_Daily_Trigger</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Sub_Contractor_Update_RAG_Status_1_WF</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Sub_Contractor_Update_RAG_Status_2_WF</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Sub_Contractors__c.Daily_Update_Trigger__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Sub-Contractor - Update RAG Status 1 %26 2</fullName>
        <actions>
            <name>Sub_Contractor_Update_RAG_Status_1_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Sub_Contractor_Update_RAG_Status_2_WF</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR( ISNEW(),  ISCHANGED(Appointed_Order_Date__c) ,  ISCHANGED(Sign_Off_Actual_Date__c) , ISCHANGED(OTE_Actual_Date__c) , ISCHANGED( Target_Appointment_Date__c ) , ISCHANGED( Sign_Off_Target_Date__c ) , ISCHANGED(OTE_Target_Date__c), ISCHANGED( Daily_Update_Trigger__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
