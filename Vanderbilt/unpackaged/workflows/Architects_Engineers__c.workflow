<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Architech_Update_Appointment_Status</fullName>
        <field>Appointment_Status__c</field>
        <literalValue>Appointed</literalValue>
        <name>Architech - Update Appointment Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Appointment_Date</fullName>
        <field>Appointment_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Appointment Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Architect %2F Engineer - Update Status on Consultant Appointment</fullName>
        <actions>
            <name>Architech_Update_Appointment_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Appointment_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOt(ISBLANK(   Architect_Engineer_Account__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
