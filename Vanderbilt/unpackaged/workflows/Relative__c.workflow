<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Land_Relative_Birthday_Reminder</fullName>
        <description>Land - Relative Birthday Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Land_Templates/Land_Relative_Birthday_Reminder</template>
    </alerts>
    <rules>
        <fullName>Land - Relative Birthday Reminder</fullName>
        <actions>
            <name>Land_Relative_Birthday_Reminder</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Next_Birthday_Reminder__c = TODAY()</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
