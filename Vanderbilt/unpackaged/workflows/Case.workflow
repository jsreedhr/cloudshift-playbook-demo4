<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Case_Closure_Request_Email</fullName>
        <description>Email - Case Closure Request Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>sue@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Email_Templates/Case_Case_Closure_Request</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Set_Closed_Reviewed_Flag</fullName>
        <field>Closed_Reviewed__c</field>
        <literalValue>1</literalValue>
        <name>Case - Set Closed - Reviewed Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Set_Due_Date</fullName>
        <field>Due_Date__c</field>
        <formula>IF(ISPICKVAL(Priority, &quot;Critical&quot;), TODAY() + 2,
IF(ISPICKVAL(Priority, &quot;High&quot;), TODAY() + 5,
IF(ISPICKVAL(Priority, &quot;Medium&quot;), TODAY() + 10,
IF(ISPICKVAL(Priority, &quot;Low&quot;), TODAY() + 20, TODAY()))))</formula>
        <name>Case - Set Due-Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Thread_ID_Population</fullName>
        <field>thread_id_store__c</field>
        <formula>&quot;ref:_&quot;&amp;LEFT($Organization.Id,5)&amp;SUBSTITUTE(RIGHT($Organization.Id,10),&quot;0&quot;,&quot;&quot;) 
&amp;&quot;._&quot;&amp;LEFT(Id,5)&amp;SUBSTITUTE(LEFT(RIGHT(Id,10),5),&quot;0&quot;,&quot;&quot;)&amp;RIGHT(Id,5)&amp;&quot;:ref&quot;</formula>
        <name>Case Thread ID Population</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Case - Closed Reviewed - Set Flag</fullName>
        <actions>
            <name>Case_Set_Closed_Reviewed_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed - Reviewed</value>
        </criteriaItems>
        <description>Workflow to select the &apos;Closed - Reviewed&apos; flag the first time the case is set to &apos;Closed - Reviewed&apos;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case - Email - Set First Time Due-Date</fullName>
        <actions>
            <name>Case_Set_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to set the due-date of the case for email cases the first time based on priority. Built by CloudShift.</description>
        <formula>AND(  ISPICKVAL(Origin, &quot;Email&quot;),  ISBLANK(Due_Date__c), NOT(ISNEW()))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Case - Non-Email - Set First Time Due-Date</fullName>
        <actions>
            <name>Case_Set_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Origin</field>
            <operation>notEqual</operation>
            <value>Email</value>
        </criteriaItems>
        <description>Workflow to set the due-date of the case for non-email cases the first time based on priority. Built by CloudShift.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Thread Email Population</fullName>
        <actions>
            <name>Case_Thread_ID_Population</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow rule to populate the case thread into a field referenced in email templates. This has been built in every email template to remedy the issue caused when sending a job sheet and the case thread not being identified.</description>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Email - Automated Email to HoCC Requesting Case Closure</fullName>
        <actions>
            <name>Email_Case_Closure_Request_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Workflow email to be sent to the Head of Customer Care to inform that from the agent&apos;s perspective the case is closed.</description>
        <formula>AND(OR(  ISPICKVAL(PRIORVALUE(Status), &quot;Action Required&quot;), ISPICKVAL(PRIORVALUE(Status), &quot;Booked&quot;), ISPICKVAL(PRIORVALUE(Status), &quot;On Hold&quot;)), ISPICKVAL(Status, &quot;Closed&quot;), $User.Username != &quot;sue@vanderbilthomes.co.uk.test&quot;, $User.Username != &quot;sue@vanderbilthomes.co.uk&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
