<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Build_QA_Site_Inspection_Contracts_Manager_Monthly_Not_Done_14_Days</fullName>
        <description>Build QA - Site Inspection, Contracts Manager Monthly - Not Done +14 Days</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.hutson@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Site_Inspection_Contracts_Manager_Monthly_Not_Done_14_Days</template>
    </alerts>
    <alerts>
        <fullName>Build_QA_Site_Inspection_Contracts_Manager_Monthly_Not_Done_7_Days</fullName>
        <description>Build QA - Site Inspection, Contracts Manager Monthly - Not Done +7 Days</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Site_Inspection_Contracts_Manager_Monthly_Not_Done_7_Days</template>
    </alerts>
    <alerts>
        <fullName>Build_QA_Site_Inspection_Site_Manager_Weekly_Not_Done_by_Tuesday</fullName>
        <description>Build QA - Site Inspection, Site Manager Weekly - Not Done by Tuesday</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Site_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Site_Inspection_Site_Manager_Weekly_Not_done_by_Tuesday</template>
    </alerts>
    <alerts>
        <fullName>Build_QA_Site_Inspection_Site_Manager_Weekly_Not_Done_by_Wednesday</fullName>
        <description>Build QA - Site Inspection, Site Manager Weekly - Not Done by Wednesday</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.hutson@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Site_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Site_Inspection_Site_Manager_Weekly_Not_done_by_Wednesday</template>
    </alerts>
    <alerts>
        <fullName>Email_Creation_of_Inspection_Contracts_Manager_Monthly</fullName>
        <description>Email - Creation of Inspection (Contracts Manager Monthly)</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Creation_of_Inspection_Contracts_Manager_Monthly</template>
    </alerts>
    <alerts>
        <fullName>Email_Creation_of_Inspection_Site_Manager_Weekly</fullName>
        <description>Email - Creation of Inspection (Site Manager Weekly)</description>
        <protected>false</protected>
        <recipients>
            <field>Site_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Build_QA/Build_QA_Creation_of_Inspection_Site_Manager_weekly</template>
    </alerts>
    <fieldUpdates>
        <fullName>Inspection_Naming_Convention</fullName>
        <description>Updates inspections at create or edit with the naming convention</description>
        <field>Name</field>
        <formula>Development__r.Name &amp; &quot; - &quot; &amp; RecordType.Name &amp; &quot;: &quot; &amp; TEXT(DAY(Report_Date__c)) &amp; &quot;/&quot; &amp; TEXT(MONTH(Report_Date__c)) &amp; &quot;/&quot; &amp; TEXT(YEAR(Report_Date__c))</formula>
        <name>Inspection Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Completed_Date</fullName>
        <field>Date_Completed__c</field>
        <formula>today()</formula>
        <name>Update Completed Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Build QA - Inspection - Completed Date</fullName>
        <actions>
            <name>Update_Completed_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Inspection__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>This rule populates the Completed Date when its status is changed to Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Build QA - Inspection - Naming Convention</fullName>
        <actions>
            <name>Inspection_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Build QA - Site Inspection%2C Contracts Manager Monthly - Email Reminder</fullName>
        <active>true</active>
        <formula>RecordType.DeveloperName = &quot;Contracts_Manager_Monthly_Inspection&quot;  &amp;&amp;  NOT(ISPICKVAL(Status__c , &quot;Completed&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Build_QA_Site_Inspection_Contracts_Manager_Monthly_Not_Done_14_Days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Inspection__c.Report_Date__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Creation_of_Inspection_Contracts_Manager_Monthly</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Inspection__c.Report_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Build_QA_Site_Inspection_Contracts_Manager_Monthly_Not_Done_7_Days</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Inspection__c.Report_Date__c</offsetFromField>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Build QA - Site Inspection%2C Site Manager Weekly - Email Reminder</fullName>
        <active>true</active>
        <formula>RecordType.DeveloperName = &quot;Site_Manager_Weekly_Inspection&quot;  &amp;&amp;  Development__r.Number_of_Site_Inspections__c &gt;=2  &amp;&amp;    NOT(ISPICKVAL(Status__c , &quot;Completed&quot;))</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Creation_of_Inspection_Site_Manager_Weekly</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Inspection__c.Report_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Build_QA_Site_Inspection_Site_Manager_Weekly_Not_Done_by_Tuesday</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Inspection__c.Report_Date__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Build_QA_Site_Inspection_Site_Manager_Weekly_Not_Done_by_Wednesday</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Inspection__c.Report_Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
