<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Technical_Approval_Naming_Convention</fullName>
        <field>Name</field>
        <formula>Development__r.Name  &amp; &quot; - &quot; &amp; House_Type__c &amp; &quot; - &quot; &amp; Component__c</formula>
        <name>Technical Approval - Naming Convention</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Technical_Approval_Set_completed_date</fullName>
        <field>Date_Completed__c</field>
        <formula>TODAY()</formula>
        <name>Technical Approval - Set completed date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Technical Approval - If approved by tech team then set completed date to today</fullName>
        <actions>
            <name>Technical_Approval_Set_completed_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(  ISCHANGED( Technical_Team_Approval_Status__c ), TEXT(Technical_Team_Approval_Status__c ) = &quot;Approved&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Technical Approval - Naming Convention</fullName>
        <actions>
            <name>Technical_Approval_Naming_Convention</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
