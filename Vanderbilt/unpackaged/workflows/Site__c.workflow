<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Send_New_Site_Alert_to_HoL</fullName>
        <description>Email - Send New Site Alert to HoL</description>
        <protected>false</protected>
        <recipients>
            <recipient>duncan.k@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>steve.brownlie@vanderbilthomes.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>Land_Templates/Land_New_Site_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Site_Set_Notification_Flag</fullName>
        <field>New_Site_Notification_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Site - Set Notification Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Site_Set_Planning_Manager_As_Nathan</fullName>
        <field>Planning_Manager__c</field>
        <lookupValue>nathan.craker@vanderbilthomes.co.uk</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Site - Set Planning Manager As Nathan</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Stage_Change_Date</fullName>
        <field>Stage_Change_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Stage Change Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Site - Inform HoL of new Site</fullName>
        <actions>
            <name>Email_Send_New_Site_Alert_to_HoL</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Site_Set_Notification_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Site__c.Stage__c</field>
            <operation>notEqual</operation>
            <value>Registered,Archive</value>
        </criteriaItems>
        <criteriaItems>
            <field>Site__c.New_Site_Notification_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Workflow to be sent to the Head of Land when a new site has come online.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Site - Set Planning Manager As Nathan</fullName>
        <actions>
            <name>Site_Set_Planning_Manager_As_Nathan</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Site__c.CreatedDate</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Site - Stage Change Tracking</fullName>
        <actions>
            <name>Update_Stage_Change_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED( Stage__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
