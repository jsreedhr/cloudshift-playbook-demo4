trigger Lead on Lead (Before Insert, After Insert, After Update){
    IF(Trigger.isBefore){
    	LeadTriggerFunctions.SetPrimaryDevLookupFromWebtoLead(Trigger.new);
    }
    IF(Trigger.isAfter){
        IF(Trigger.isInsert){
			LeadTriggerFunctions.AssociateWebLeadtoCampaign(Trigger.new);
    	}
        IF(Trigger.isUpdate){
			LeadTriggerFunctions.ConvertLeadEnhancements(Trigger.new);
    	}
    }
}