trigger Task_Trigger on Task (before insert, before update, after update,after insert) {
    if(Trigger.IsAfter){
        if(Trigger.IsInsert){
            Task_Trigger_Utility.SendInitialCaseResponse(trigger.newMap);
        }
        if(Trigger.IsUpdate){
            Task_Trigger_Utility.SendInitialCaseResponseUpdate(trigger.oldMap,trigger.newMap);
        }
    }
}