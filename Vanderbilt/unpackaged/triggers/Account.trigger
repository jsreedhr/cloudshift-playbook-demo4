trigger Account on Account (after insert, before update) {
    IF(Trigger.isInsert){
	AccountTriggerFunctions.createMaterialsInsert(Trigger.new);
    }
    IF(Trigger.isUpdate){
	AccountTriggerFunctions.createMaterialsUpdate(Trigger.new);
    }
}