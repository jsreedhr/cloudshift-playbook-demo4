trigger changeCurrencyInWord on Opportunity (before insert, before update) {

    for (Opportunity c : Trigger.new) {
        if (c.Amount != null && c.Amount >= 0) {
           
            Long n = c.Amount.longValue();
            string amo = NumberToWord.english_number(n);
            string amo1 = amo.remove(',');
            c.Amount_in_Words__c = amo1;
        } else {
            c.Amount_in_Words__c = null;
        }
        
        if (c.Reservation_Fee__c != null && c.Reservation_Fee__c>= 0) {
           
            Long n = c.Reservation_Fee__c.longValue();
            string amor = NumberToWord.english_number(n);
            string amor1 = amor.remove(',');
            c.Reservation_Deposit_in_Words__c= amor1;
        } else {
            c.Reservation_Deposit_in_Words__c= null;
        }
    }
}