trigger Issue_Trigger on Issue__c (after insert, after update, after delete,after undelete){
    
    
    List<Profile> lstProfile = new List<Profile>();
    
    
    
    // After Trigger
    if(Trigger.isAfter){
        
        // Update Trigger
        if(Trigger.isUpdate||Trigger.isInsert ||Trigger.isUnDelete||Trigger.IsDelete){
            
            Issue_Trigger_Utility.ContractorsListPopulateInsert(Trigger.IsDelete?Trigger.oldMap:Trigger.newMap);
            
        }
       
        
        
    }
    
    
    }