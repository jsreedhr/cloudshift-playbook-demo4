trigger Opportunity on opportunity (before insert, before update, after update){
    
    if(Trigger.isBefore){
        OpportunityTriggerFunctions  uss = new OpportunityTriggerFunctions();
        uss.MarketingOptOut(trigger.new);
        
    }
    
    if(Trigger.isAfter){
        OpportunityTriggerFunctions  uss = new OpportunityTriggerFunctions();
        uss.updateStatus(trigger.new,Trigger.oldmap);
        
    }        
}