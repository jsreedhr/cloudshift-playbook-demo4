trigger plotCoinsTrigger on Plot__c 
(after update) 
{
    if(trigger.isAfter && trigger.isUpdate)
        PlotCoinsTriggerHandler.coinsCallout(trigger.new,trigger.oldMap);
}