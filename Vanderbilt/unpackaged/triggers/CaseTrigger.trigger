trigger CaseTrigger on Case (after insert) {
    if(Trigger.isAfter && Trigger.isInsert){
        Case_Email_Trigger_Utility.LinkPlotToCase(Trigger.new);
    }
}