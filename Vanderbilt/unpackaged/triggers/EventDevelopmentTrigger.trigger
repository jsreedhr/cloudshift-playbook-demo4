/**
 *  Trigger Name: EventDevelopmentTrigger 
 *  Description: This is a trigger for Events
 *  CreatedDate: 08/12/2016
 */
trigger EventDevelopmentTrigger on Event 
(
    before insert,
    before update
) 
{
   EventDevelopmentTriggerHandler.updateEventFields(Trigger.new);
}