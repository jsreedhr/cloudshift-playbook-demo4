/**
 *  Name: DevelopmentTrigger
 *  Copyright © 2017 Salesforce
 *  Purpose: In this trigger DevelopmentTriggerHandler class will be called
 *
 *  History:
 *  Version         Author              Date               Detail
 *  1.0             Jeevraj Rao       14/11/2017           Creation
 */

trigger DevelopmentTrigger on Development__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	system.debug('START: DevelopmentTrigger');
	new DevelopmentTriggerHandler().run(); //GLOBALLY
}