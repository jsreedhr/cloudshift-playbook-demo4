/**
 *  Trigger Name: TaskDevelopmentTrigger 
 *  Description: This is a trigger for Tasks
 *  CreatedDate: 08/12/2016
 */
trigger TaskDevelopmentTrigger on Task 
(
    before insert,
    before update
) 
{
   TaskDevelopmentTriggerHandler.updateTaskFields(Trigger.new);
}