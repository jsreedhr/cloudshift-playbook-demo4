<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>ComposeGmail</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>LogCall</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>MailMerge</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>RequestUpdate</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SendEmail</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>ViewAll</actionName>
        <type>Default</type>
    </actionOverrides>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Development_Marketing_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Development__r.Marketing_Name__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Development Marketing Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Development_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Development__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Development Name</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Development__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>A lookup from Activity to the development of the related lead.</description>
        <externalId>false</externalId>
        <label>Development</label>
        <referenceTo>Development__c</referenceTo>
        <relationshipLabel>Activities</relationshipLabel>
        <relationshipName>Activities</relationshipName>
        <required>false</required>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Event_Summary__c</fullName>
        <description>Formula field concatenating the date and event subject. Built by CloudShift.</description>
        <externalId>false</externalId>
        <formula>IF(IsTask=FALSE, TEXT(ActivityDate) &amp; &quot; - &quot; &amp;  Subject, NULL)</formula>
        <inlineHelpText>Formula field concatenating the date and event subject.</inlineHelpText>
        <label>Event Summary</label>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <listViews>
        <fullName>All_Open_Tasks</fullName>
        <columns>TASK.WHAT_NAME</columns>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>EVENT.LOCATION</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.SUBJECT</field>
            <operation>notContain</operation>
            <value>Email:</value>
        </filters>
        <filters>
            <field>TASK.STATUS</field>
            <operation>equals</operation>
            <value>Open</value>
        </filters>
        <label>All Open Tasks</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All_Tasks</fullName>
        <columns>TASK.WHAT_NAME</columns>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>EVENT.LOCATION</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.SUBJECT</field>
            <operation>notContain</operation>
            <value>Email:</value>
        </filters>
        <label>All Tasks</label>
    </listViews>
    <listViews>
        <fullName>MyActivities</fullName>
        <columns>TASK.WHAT_NAME</columns>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>EVENT.LOCATION</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.SUBJECT</field>
            <operation>notContain</operation>
            <value>Email:</value>
        </filters>
        <label>My Tasks</label>
    </listViews>
    <listViews>
        <fullName>MyDelegatedActivities</fullName>
        <columns>TASK.WHAT_NAME</columns>
        <columns>CORE.USERS.FULL_NAME</columns>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>TASK.TYPE</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>EVENT.LOCATION</columns>
        <filterScope>Delegated</filterScope>
        <label>My Delegated Activities</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>MyTeamsActivities</fullName>
        <columns>TASK.WHAT_NAME</columns>
        <columns>CORE.USERS.FULL_NAME</columns>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>EVENT.LOCATION</columns>
        <filterScope>Team</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>My Team&apos;s Tasks</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>TodaysTasks</fullName>
        <columns>TASK.WHAT_NAME</columns>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>EVENT.LOCATION</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>TASK.CLOSED</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>lessThan</operation>
            <value>TOMORROW</value>
        </filters>
        <label>Today&apos;s Tasks</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>UpcomingEvents</fullName>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.WHAT_NAME</columns>
        <columns>EVENT.START_DATE_TIME</columns>
        <columns>EVENT.END_DATE_TIME</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>EVENT.LOCATION</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>greaterThan</operation>
            <value>YESTERDAY</value>
        </filters>
        <label>Upcoming Events</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>activities__Overdue_Tasks</fullName>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>TASK.WHAT_NAME</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>CORE.USERS.ALIAS</columns>
        <columns>TASK.LAST_UPDATE</columns>
        <columns>UPDATEDBY_USER.ALIAS</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.CLOSED</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>lessThan</operation>
            <value>TODAY</value>
        </filters>
        <label>Overdue Tasks</label>
    </listViews>
    <listViews>
        <fullName>activities__Today_Overdue_Tasks</fullName>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>TASK.WHAT_NAME</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>CORE.USERS.ALIAS</columns>
        <columns>TASK.LAST_UPDATE</columns>
        <columns>UPDATEDBY_USER.ALIAS</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.CLOSED</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>lessOrEqual</operation>
            <value>TODAY</value>
        </filters>
        <label>Today + Overdue Tasks</label>
    </listViews>
    <listViews>
        <fullName>activities__TodaysTasks</fullName>
        <filterScope>Mine</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.CLOSED</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>lessThan</operation>
            <value>TOMORROW</value>
        </filters>
        <label>Today&apos;s Tasks</label>
    </listViews>
    <listViews>
        <fullName>activities__Tomorrow_s_Tasks</fullName>
        <columns>TASK.SUBJECT</columns>
        <columns>TASK.WHO_NAME</columns>
        <columns>TASK.WHAT_NAME</columns>
        <columns>TASK.DUE_DATE</columns>
        <columns>TASK.STATUS</columns>
        <columns>TASK.PRIORITY</columns>
        <columns>CORE.USERS.ALIAS</columns>
        <columns>TASK.LAST_UPDATE</columns>
        <columns>UPDATEDBY_USER.ALIAS</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>ACTIVITY.TASK</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <filters>
            <field>TASK.CLOSED</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <filters>
            <field>TASK.DUE_DATE</field>
            <operation>equals</operation>
            <value>TOMORROW</value>
        </filters>
        <label>Tomorrow&apos;s Tasks</label>
    </listViews>
    <searchLayouts>
        <listViewButtons>activities__CloseTasks</listViewButtons>
        <listViewButtons>activities__Mass_Update</listViewButtons>
        <searchResultsAdditionalFields>TASK.SUBJECT</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TASK.WHO_NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TASK.WHAT_NAME</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>TASK.DUE_DATE</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CORE.USERS.ALIAS</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
